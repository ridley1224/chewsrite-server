<?php

require_once('../../Connections/chewsrite.php');
include("functions.php");

mysql_select_db($database_chewsrite, $chewsrite);

$date = date("Y-m-d H:i:s");

//$_POST['groupname'] = "2";
//$_POST['groupdescription'] = "2";
//$_POST['userid'] = "2";
//$_POST['imagename'] = "2";

$object = new stdClass();
$object->status = "group not saved";

if(isset($_POST['groupname']) && isset($_POST['userid']) && isset($_POST['insert']))
{
	$insertSQL = sprintf("INSERT INTO groups(groupname, groupdescription, userid, groupimage, datecreated) VALUES (%s, %s, %s, %s, %s)",
				GetSQLValueString(mysql_real_escape_string($_POST['groupname']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['groupdescription']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['userid']), "int"),
				GetSQLValueString(mysql_real_escape_string($_POST['imagename']), "text"),
				GetSQLValueString(mysql_real_escape_string($date), "date"));

	mysql_select_db($database_chewsrite, $chewsrite);
	$Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());	

	$last_id = mysql_insert_id();	
	$object->groupid = $last_id;
	$object->status = "group saved";
	
	$insertSQL2 = sprintf("INSERT INTO groupmembers(userid, groupid, dateadded) VALUES (%s, %s, %s)",
				GetSQLValueString(mysql_real_escape_string($_POST['userid']), "text"),
				GetSQLValueString(mysql_real_escape_string($last_id), "text"),
				GetSQLValueString(mysql_real_escape_string($date), "date"));

	mysql_select_db($database_chewsrite, $chewsrite);
	$Result2 = mysql_query($insertSQL2, $chewsrite) or die(mysql_error());	
    
    $insertSQL2 = sprintf("INSERT INTO groupnotificationsettings(userid, groupid, selection, datecreated) VALUES (%s, %s, %s, %s)",
				GetSQLValueString(mysql_real_escape_string($_POST['userid']), "int"),
				GetSQLValueString(mysql_real_escape_string($last_id), "int"),
                GetSQLValueString(0, "int"),
				GetSQLValueString(mysql_real_escape_string($date), "date"));

	mysql_select_db($database_chewsrite, $chewsrite);
	$Result2 = mysql_query($insertSQL2, $chewsrite) or die(mysql_error());	

	$last_id = mysql_insert_id();	
}
else if(isset($_POST['groupname']) && isset($_POST['groupid']) && isset($_POST['update']))
{
    $updateSQL = sprintf("UPDATE groups SET groupname=%s, groupdescription=%s WHERE groupid = %s ",
                        GetSQLValueString(mysql_real_escape_string($_POST['groupname']), "text"),
				        GetSQLValueString(mysql_real_escape_string($_POST['groupdescription']), "text"),
                        GetSQLValueString(mysql_real_escape_string($_POST['groupid']), "int"));

    mysql_select_db($database_chewsrite, $chewsrite);
    $Result1 = mysql_query($updateSQL, $chewsrite) or die(mysql_error());	
    $object->status = "group updated";
}

//to do randall

//send invites

if(isset($_POST['invitelist']))
{
    //send push
}

echo "{\"data\":";
echo "{\"groupData\":";
echo json_encode( $object );
echo "}";
echo "}";

?>
        