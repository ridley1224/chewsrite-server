<?php

//populated from substitutionsQuery.php

if(count($replacementlist) > 0)
{
    echo "has replacement list<br>";
    
    $foundIngredient = false;

    foreach($replacementlist as $replacement)
    {
        if($replacement->ingredientid == $ingredientObject->ingredientid) //replacement list contains object ingredientid
        {
            echo "object replaced: {$object->ingredientid} for {$replacement->subid}<br>";
            $ingredientObject->replacedobjectid = $ingredientObject->ingredientid;
            $ingredientObject->ingredientid = $replacement->subid;
            $ingredientObject->ingredientname = $replacement->ingredientname;
            $ingredientObject->isSubstitute = true;
            $ingredientObject->active = $replacement->active; //check if substitution is disabled from RecipeDetails.swift or EditRecipeIngredient.swift
            
            $list[] = $ingredientObject;
            
            $foundIngredient = true;
        }
        else
        {
            $ingredientObject->isSubstitute = false;
            $list[] = $ingredientObject;
        }
    }
    
    //if didnt find ingredient to replace, add it to list
    
//    if($foundIngredient == false)
//    {
//        $replacement->quantity = $ingredientObject->quantity;
//        $replacement->unit = $ingredientObject->unit;
//        $replacement->calories = $ingredientObject->calories;
//        $replacement->imagename = $ingredientObject->imagename;
//        $replacement->recipename = $ingredientObject->recipename;
//        
//        $replacement->replacedobjectid = $replacement->ingredientid;
//        $replacement->ingredientid = $replacement->subid;
//        $replacement->groceryItemCount = $ingredientObject->groceryItemCount;
//        $replacement->recipeIngredientsCount = $ingredientObject->recipeIngredientsCount;
//        $replacement->isSubstitute = true;
//        
//        $replacement->ingredientsid = $ingredientObject->ingredientsid;
//        $list[] = $replacement;
//    }    
    
//    calories = 400;
//                groceryItemCount = 2;
//                imagename = "pistachio-crusted-chicken-and-quinoa-salad.jpg";
//                ingredientid = 364;
//                ingredientname = "Black Pepper";
//                ingredientsid = 5;
//                isSubstitute = 0;
//                quantity = 1;
//                recipeIngredientsCount = 3;
//                recipeid = 1;
//                recipename = "Pistachio-Crusted Chicken and Quinoa Salad";
//                unit = tbs;
}
else
{
    $list[] = $ingredientObject;
    $ingredientObject->isSubstitute = false;
}

?>