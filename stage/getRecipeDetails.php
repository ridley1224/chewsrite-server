<?php require_once('../../Connections/chewsrite.php'); ?>
<?php

include("functions.php");
include("en-de.php");

//$_POST['userid'] = "22";
//$_POST['recipeid'] = "1";

mysql_select_db($database_chewsrite, $chewsrite);

//$query_rsRecipes = "SELECT FROM recipes";


if(isset($_POST['recipeid']))
{		
	$query_rsRecipes = "SELECT * FROM recipes WHERE recipeid = {$_POST['recipeid']}";
	
	mysql_select_db($database_chewsrite, $chewsrite);

    //echo $query_rsRecipes;

    $rsRecipes = mysql_query($query_rsRecipes, $chewsrite) or die(mysql_error());
    $row_rsRecipes = mysql_fetch_assoc($rsRecipes);
    $totalRows_rsRecipes = mysql_num_rows($rsRecipes);


    if($totalRows_rsRecipes)
    {
        do {

            $object = new stdClass();

            $object->recipeid = blankNull($row_rsRecipes['recipeid']);
            $object->recipename = blankNull($row_rsRecipes['recipename']);
            $object->username = blankNull($row_rsRecipes['username']);
            $object->firstname = blankNull($row_rsRecipes['firstname']);
            $object->lastname = blankNull($row_rsRecipes['lastname']);
            $object->userimage = blankNull($row_rsRecipes['userimage']);
            $object->calories = blankNull($row_rsRecipes['calories']);
            $object->source = blankNull($row_rsRecipes['source']);
            $object->sourceurl = blankNull($row_rsRecipes['sourceurl']);
            $object->imagename = blankNull($row_rsRecipes['imagename']);
            $object->videoname = blankNull($row_rsRecipes['videoname']);
            $object->category = blankNull($row_rsRecipes['category']);
            $object->preptime = blankNull($row_rsRecipes['preptime']);
            $object->cooktime = blankNull($row_rsRecipes['cooktime']);
            $object->totaltime = blankNull($row_rsRecipes['totaltime']);
            $object->isfeatured = blankNull($row_rsRecipes['isFeatured']);
            $object->dateadded = blankNull($row_rsRecipes['dateadded']);
            $object->cuisinetags = (string)blankNull($row_rsRecipes['cuisinetags']);
            $object->recipedescription = blankNull(json_encode($row_rsRecipes['description']));
            $object->servings = blankNull($row_rsRecipes['servings']);
            $object->rating = blankNull($row_rsRecipes['rating']);
            
            $object->timeAdded = blankNull(humanTiming(strtotime($row_rsRecipes['dateadded']))) . " ago";
            
            if(isset($_POST['userid']))
            {
                $a = $_POST['userid'];

                $query_rsFavorite = "SELECT * FROM userfavorites WHERE userid = '" . $a . "' AND recipeid = " . $row_rsRecipes['recipeid'];

                $rsFavorite = mysql_query($query_rsFavorite, $chewsrite) or die(mysql_error());
                $row_rsFavorite = mysql_fetch_assoc($rsFavorite);
                $totalRows_rsFavorite = mysql_num_rows($rsFavorite);

                if($totalRows_rsFavorite)
                {
                    $object->isFavorite = "1";
                }
                else
                {
                    $object->isFavorite = "0";
                }
            }
            
        }  while ($row_rsRecipes = mysql_fetch_assoc($rsRecipes));
    }
    
    include('getRelatedRecipes.php');
}
	
echo "{\"data\":";
echo "{\"recipeData\":";
echo json_encode( $object );
echo ",\"relatedRecipeData\":";
echo json_encode( $relatedRecipes );
echo "}";
echo "}";

?>
