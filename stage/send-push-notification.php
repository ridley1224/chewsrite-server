<?php

	//https://blog.serverdensity.com/how-to-build-an-apple-push-notification-provider-server-tutorial/
	//https://www.raywenderlich.com/123862/push-notifications-tutorial

	//setup payload
	
	if(isset($message) && strlen($message) > 0)
	{
		// Put your device token here (without spaces):

		//$deviceToken = '1ec490c31d049cb6808cac3df7a04f4f66450700f8d5f1eae671fe0d5ae377f0';

		// Put your private key's passphrase here:

		$passphrase = '1224';

		//$message = $argv[1];
		//$url = $argv[2];

		//$message = $_POST['message'];
		$url = "http://dto.iwatch.com";

		if (!$message || !$url)
			exit('Example Usage: $php newspush.php \'Test notification\' \'https://dto.com\'' . "\n");

		////////////////////////////////////////////////////////////////////////////////

		$ctx = stream_context_create();
		stream_context_set_option($ctx, 'ssl', 'local_cert', 'chewsrite-apns-dev.pem');
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

		// Open a connection to the APNS server

		$fp = stream_socket_client(
		  'ssl://gateway.sandbox.push.apple.com:2195', $err,
		  $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

		if (!$fp)
		  exit("Failed to connect: $err $errstr" . PHP_EOL);

		//echo '<br>Connected to APNS' . PHP_EOL;

		// Create the payload body
		
		//echo date('Y-m-d', strtotime("+5 days"));

//		$body['aps'] = array(
//		  'alert' => $message,
//		  'sound' => 'default',
//		  'link_url' => $url,
//		  'category' => "WEATHER_CATEGORY",
//		  'body' => "remote body",
//          'badge' => 1,
//		  );

		// Encode the payload as JSON

		$payload = json_encode($body);

		// Build the binary notification

		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

		// Send it to the server

		$result = fwrite($fp, $msg, strlen($msg));

		if (!$result)
        {
            		  //echo '<br>Message not delivered' . PHP_EOL;

        }
		else
        {
            //echo '<br>Message successfully delivered' . PHP_EOL;
  
            date_default_timezone_set('America/Detroit');

            $date = date("m-d-Y g:i A");
            
            if(isset($_POST['isBrowser']))
            {
                echo "<p>Push notification sent at $date to device: $deviceToken</p>";
            }
            else
            {
                //echo "<p>Push notification sent at $date to device: $deviceToken</p>";
            }            
        }
		  

		// Close the connection to the server

		fclose($fp);
	}
	
?>
