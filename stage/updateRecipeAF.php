<?php

//header('Content-type: application/json');

error_reporting(E_ERROR | E_PARSE);

require_once('../../Connections/chewsrite.php');
include("functions.php");

mysql_select_db($database_chewsrite, $chewsrite);

$request = json_decode(file_get_contents('php://input'), TRUE);

//$request = json_encode("[\"ingredients\": [{ active = 0; ingredient = 0; ingredientname = Vodka; name = \"\"; quantity = 1; recipeorder = 4; unit = oz; }], \"recipeid\": \"1\", \"userid\": \"22\"]");
//
//$request1 = json_decode($request);
//
//var_dump($request1);

//$response = ["URLOfTheSecondWebsite" => $request['websites'][1]['URL']];

$string;
$date = date("Y-m-d H:i:s");

$status = "data not saved";

//$request['recipename'] = "a";

//loop through ingredients list
//check for ingredientid as null
//if null, check ingredients table for ingredientname lowercased
//if exists, match ingredientid and insert
//if not exists, create new ingredient and store lastid and then insert

//$object = new stdClass();
//$object->quantity = "1";
//$object->unit = "oz";
//$object->ingredientid = "1";
//$object->ingredientname = "Chicken";
//
//$object2 = new stdClass();
//$object2->quantity = "1";
//$object2->unit = "oz";
//$object2->ingredientname = "Vodka";
//
//
//$request['ingredients'][] = $object;
//$request['ingredients'][] = $object2;

//$newCount = 0;


if($request['ingredients'])
{    
    $insertSQL = sprintf("UPDATE recipes SET servings=%s WHERE recipeid = %s",
                        GetSQLValueString(mysql_real_escape_string($request['servings']), "text"),
                        GetSQLValueString(mysql_real_escape_string($request['recipeid']), "int"));

    mysql_select_db($database_chewsrite, $chewsrite);
    $Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());
    
    if(isset($request['currentIngredients']))
    {
        foreach ($request['currentIngredients'] as $ingredient) 
        {	
            $insertSQL = sprintf("UPDATE recipeingredients SET quantity=%s, unit=%s WHERE recipeid = %s AND ingredientid = %s",
                        GetSQLValueString(mysql_real_escape_string($ingredient['quantity']), "text"),
                        GetSQLValueString(mysql_real_escape_string($ingredient['unit']), "text"),
                        GetSQLValueString(mysql_real_escape_string($request['recipeid']), "int"),
                        GetSQLValueString(mysql_real_escape_string($ingredient['ingredientid']), "int"));

            mysql_select_db($database_chewsrite, $chewsrite);
            $Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());	

            $status = " ingredient updated {$ingredient['ingredientid']}";     
        }
    }
    
        
    foreach ($request['ingredients'] as $ingredient) 
    {	
        //to do randall
        //look into query ingredientid on insert in app

        if (isset($ingredient['ingredientid']))
        {
            //update ingredient
            
            $insertSQL = sprintf("UPDATE recipeingredients SET quantity=%s, unit=%s WHERE recipeid = %s AND ingredientid = %s",
                        GetSQLValueString(mysql_real_escape_string($ingredient['quantity']), "text"),
                        GetSQLValueString(mysql_real_escape_string($ingredient['unit']), "text"),
                        GetSQLValueString(mysql_real_escape_string($request['recipeid']), "int"),
                        GetSQLValueString(mysql_real_escape_string($ingredient['ingredientid']), "int"));

            mysql_select_db($database_chewsrite, $chewsrite);
            $Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());	
            
            $status = " ingredient updated {$ingredient['ingredientid']}";
        }
        else
        {
            //check if ingredient already exists
            
            $a = strtolower($ingredient['ingredientname']);

            //$query_rsRecipes = "SELECT ingredientid from ingredients WHERE LOWER(ingredient) LIKE '{$a}'";
            $query_rsRecipes = "SELECT ingredientid from ingredients WHERE LOWER(ingredient) = '{$a}'";

            //echo $query_rsRecipes;

            $rsRecipes = mysql_query($query_rsRecipes, $chewsrite) or die(mysql_error());
            $row_rsRecipes = mysql_fetch_assoc($rsRecipes);
            $totalRows_rsRecipes = mysql_num_rows($rsRecipes);

            if($totalRows_rsRecipes > 0)
            {
                //ingredient exists
                
                $ingredientid = $row_rsRecipes['ingredientid'];
                //$status .= " current ingredient saved {$ingedientid}";
                
                $updateCount[] = $ingredientid;
            }
            else
            {
                //insert new ingredient

                $insertSQL = sprintf("INSERT INTO ingredients (ingredient, userid, datecreated) VALUES (%s, %s, %s)",
                        GetSQLValueString(mysql_real_escape_string($ingredient['ingredientname']), "text"),
                        GetSQLValueString(mysql_real_escape_string($request['userid']), "int"),
                        GetSQLValueString($date, "date"));

                mysql_select_db($database_chewsrite, $chewsrite);
                $Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());	

                $ingredientid = mysql_insert_id();	
                $newCount[] = $ingredientid;
                
                $status = "new ingredient saved {$ingedientid}";                
            }

            $insertSetSQL = sprintf("INSERT INTO recipeingredients (ingredientname, quantity, unit, recipeorder, recipeid, ingredientid, active, datecreated) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
                GetSQLValueString(mysql_real_escape_string($ingredient['ingredientname']), "text"),
                GetSQLValueString(mysql_real_escape_string($ingredient['quantity']), "text"),
                GetSQLValueString(mysql_real_escape_string($ingredient['unit']), "text"),
                GetSQLValueString(mysql_real_escape_string($ingredient['recipeorder']), "text"),
                GetSQLValueString(mysql_real_escape_string($request['recipeid']), "text"),
                GetSQLValueString(mysql_real_escape_string($ingredientid), "int"),
                GetSQLValueString(1, "int"),
                GetSQLValueString($date, "date"));

                mysql_select_db($database_chewsrite, $chewsrite);
                $Result2 = mysql_query($insertSetSQL, $chewsrite) or die(mysql_error());
        }            
    }

    $status .= ". ingredients saved";
}

//"query" => $query_rsRecipes

$response = ["status" => $status, "newCount" => count($newCount), "updateCount" => count($updateCount)];

echo json_encode($response);

?>
