<?php

header('Content-type: application/json');

require_once('../../Connections/chewsrite.php');
include("functions.php");

mysql_select_db($database_chewsrite, $chewsrite);

$string;
$date = date("Y-m-d H:i:s");

$object = new stdClass();
$object->status = "folder not saved";

if(isset($_POST['foldername']) && isset($_POST['userid']))
{
	$insertSQL = sprintf("INSERT INTO userfolders(foldername, userid, createddate) VALUES (%s, %s, %s)",
				GetSQLValueString(mysql_real_escape_string($_POST['foldername']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['userid']), "int"),
				GetSQLValueString(mysql_real_escape_string($date), "date"));

	mysql_select_db($database_chewsrite, $chewsrite);
	$Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());	

	$last_id = mysql_insert_id();	
	$object->folderid = (string)$last_id;
	$object->status = "folder saved";
}

echo "{\"data\":";
echo "{\"folderData\":";
echo json_encode( $object );
echo "}";
echo "}";

?>
        