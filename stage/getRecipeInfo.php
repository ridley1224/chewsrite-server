<?php 


$_POST['devStatus'] = "dev";

require_once('../../Connections/chewsrite.php'); 

include("functions.php");
include("en-de.php");

//$_POST['userid'] = "52";
//
//
//$_POST['recipeid'] = "1";

//$_POST['userid'] = "22";
//$_POST['filtered'] = "mine";

//$recipeid = "23";


//$_POST['recipeid'] = $_GET['recipeid'];

//$_POST['recipeid'] = "1";

if(isset($_POST['recipeid']))
{
    $recipeid = $_POST['recipeid'];
}
else if(isset($_GET['recipeid']))
{
    $recipeid = $_GET['recipeid'];
}

if(isset($recipeid))
{    
    mysql_select_db($database_chewsrite, $chewsrite);

    //get ingredients
    
    //include("includes/substitutionsQuery.php");

    $query_rsRecipes = "SELECT * from recipeingredients WHERE recipeid = {$recipeid} AND active = 1";

    //echo $query_rsRecipes . "<br>";

    $rsRecipes = mysql_query($query_rsRecipes, $chewsrite) or die(mysql_error());
    $row_rsRecipes = mysql_fetch_assoc($rsRecipes);
    $totalRows_rsRecipes = mysql_num_rows($rsRecipes);

    if($totalRows_rsRecipes)
    {
        do {

            $ingredientObject = new stdClass();

            $ingredientObject->recipeid = blankNull($row_rsRecipes['recipeid']);
            //$ingredientObject->imagename = blankNull($row_rsRecipes['imagename']);
            //$ingredientObject->videoname = blankNull($row_rsRecipes['videoname']);
            $ingredientObject->ingredientname = (string)blankNull($row_rsRecipes['ingredientname']);
            $ingredientObject->ingredientid = (string)blankNull($row_rsRecipes['ingredientid']);
            $ingredientObject->quantity = (string)blankNull($row_rsRecipes['quantity']);
            $ingredientObject->unit = (string)blankNull($row_rsRecipes['unit']);
            $ingredientObject->isSubstitute = false;
            
            $query_rsInCart = "SELECT ingredientids,substitutionids from groceryitems WHERE FIND_IN_SET({$row_rsRecipes['ingredientid']},ingredientids) AND userid = {$_POST['userid']}";
            
            //echo $query_rsInCart . "<br>";

            $rsInCart = mysql_query($query_rsInCart, $chewsrite) or die(mysql_error());
            $row_rsInCart = mysql_fetch_assoc($rsInCart);
            $totalRows_rsInCart = mysql_num_rows($rsInCart);
            
            if($totalRows_rsInCart > 0)
            {
                $ingredientObject->active = true;
            }
            else
            {
                $ingredientObject->active = false;
            }

            $list[] = $ingredientObject;
            
            //check replacement list
            
            //include("includes/checkSubstitutionObject.php");

        }  while ($row_rsRecipes = mysql_fetch_assoc($rsRecipes));
        
        
        //substitutes
        
        $query_rsRecipeSubstitutes = "SELECT a.*, b.recipeid, b.active FROM (SELECT * FROM substitutions) as a INNER JOIN (SELECT * FROM recipesubstitutes WHERE recipeid = {$recipeid}) as b ON a.subid = b.subid";

        $rsRecipeSubstitutes = mysql_query($query_rsRecipeSubstitutes, $chewsrite) or die(mysql_error());
        $row_rsRecipeSubstitutes = mysql_fetch_assoc($rsRecipeSubstitutes);
        $totalRows_rsRecipeSubstitutes = mysql_num_rows($rsRecipeSubstitutes);

        if($totalRows_rsRecipeSubstitutes)
        {
            //echo "has substitutions<br>";

            //add substitutes from list
            //populates in checkSubstitutionObject.php
            
            $query_rsInCart2 = "SELECT substitutionids from groceryitems WHERE FIND_IN_SET({$row_rsRecipeSubstitutes['subid']},substitutionids) AND userid = {$_POST['userid']}";
            
            $rsInCart2 = mysql_query($query_rsInCart2, $chewsrite) or die(mysql_error());
            $row_rsInCart2 = mysql_fetch_assoc($rsInCart2);
            $totalRows_rsInCart2 = mysql_num_rows($rsInCart2);
            
            
            //echo $query_rsInCart2 . "<br>";
            
            
            $subIDs = explode(",",$row_rsInCart2['substitutionids']);

            do {

                $object = new stdClass();

                $object->recipeid = blankNull($row_rsRecipeSubstitutes['recipeid']);
                $object->ingredientname = (string)blankNull($row_rsRecipeSubstitutes['substitution']);
                //$object->subid = (string)blankNull($row_rsRecipeSubstitutes['subid']);
                $object->replacedobjectid = (string)blankNull($row_rsRecipeSubstitutes['ingredientid']);
                $object->ingredientid = (string)blankNull($row_rsRecipeSubstitutes['subid']);
                $object->quantity = (string)blankNull($row_rsRecipeSubstitutes['quantity']);
                $object->unit = (string)blankNull($row_rsRecipeSubstitutes['unit']);
                
                //$object->imagename = blankNull($row_rsRecipeDetails['imagename']);
                $object->isSubstitute = true;
                
                
                //$object->active = (int)$row_rsRecipeSubstitutes['active'];
                
                if(in_array($object->ingredientid,$subIDs))
                {
                    $object->active = 1;
                }
                else
                {
                    $object->active = 0;
                }

                $list[] = $object;				

            }  while ($row_rsRecipeSubstitutes = mysql_fetch_assoc($rsRecipeSubstitutes));
        }
    }

    //instructions

    include("includes/recipes/recipeDirections.php");

    //reviews

    include("includes/recipes/recipeReviews.php");

    //tips

    include("includes/recipes/recipeTips.php");

    //get recipe tags

    include("includes/recipes/recipeTags.php");

    //nutrition data

    include("includes/recipes/recipeNutritionData.php");
    
    //related recipes
    
    include('getRelatedRecipes.php');
}


echo "{\"data\":";
echo "{\"recipeData\":";
echo json_encode( $list );
echo ",\"directionsData\":";
echo json_encode( $list2 );
echo ",\"reviewsData\":";
echo json_encode( $list3 );
echo ",\"tipsData\":";
echo json_encode( $list4 );
echo ",\"tagsData\":";
echo json_encode( $list5 );
echo ",\"nutritionData\":";
echo json_encode( $nutritionObject );
echo ",\"relatedRecipeData\":";
echo json_encode( $relatedRecipes );
echo "}";
echo "}";

?>
