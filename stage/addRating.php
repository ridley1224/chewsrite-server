<?php

header('Content-type: application/json');

require_once('../../Connections/chewsrite.php');
include("functions.php");

mysql_select_db($database_chewsrite, $chewsrite);

$string;
$date = date("Y-m-d H:i:s");

$object = new stdClass();
$object->status = "bookmark not saved";

if(isset($_POST['recipeid']))
{
	$insertSQL = sprintf("INSERT INTO reviews (recipeid, rating, review, userid, reviewdate) VALUES (%s, %s, %s, %s, %s)",
				GetSQLValueString(mysql_real_escape_string($_POST['recipeid']), "text"),
                GetSQLValueString(mysql_real_escape_string($_POST['rating']), "text"),
                GetSQLValueString(mysql_real_escape_string($_POST['review']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['userid']), "int"),
				GetSQLValueString(mysql_real_escape_string($date), "date"));

	mysql_select_db($database_chewsrite, $chewsrite);
	$Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());	

	$last_id = mysql_insert_id();	
	$object->reviewid= $last_id;
	$object->status = "rating saved";
}

echo "{\"data\":";
echo "{\"reviewData\":";
echo json_encode( $object );
echo "}";
echo "}";

?>
          