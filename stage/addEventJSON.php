<?php

header('Content-type: application/json');

require_once('../../Connections/chewsrite.php');
include("functions.php");

mysql_select_db($database_chewsrite, $chewsrite);

//$response = ["URLOfTheSecondWebsite" => $request['websites'][1]['URL']];

$string;
$date = date("Y-m-d H:i:s");

$object = new stdClass();
$object->status = "data not saved";

//$_POST['eventname'] = "en";
//$_POST['city'] = "c";
//$_POST['state'] = "s";
//$_POST['zip'] = "z";
//$_POST['totalseats'] = "10";
//$_POST['totalseats'] = "10";
//$_POST['venueid'] = "1";
//$_POST['eventdate'] = "2018-03-15 08:00:00";
//$_POST['eventdescription'] = "edd";
//$_POST['eventimage'] = "ei";

if($_POST['eventname'])
{
	if(isset($_POST['updating']))
	{
		$insertSQL = sprintf("UPDATE events SET eventname = %s, city = %s, state = %s, zip = %s, totalseats = %s, eventcost = %s, eventdescription = %s, eventdate = %s WHERE eventid = %s",
					GetSQLValueString(mysql_real_escape_string($_POST['eventname']), "text"),
					GetSQLValueString(mysql_real_escape_string($_POST['city']), "text"),
					GetSQLValueString(mysql_real_escape_string($_POST['state']), "text"),
					GetSQLValueString(mysql_real_escape_string($_POST['zip']), "text"),
					GetSQLValueString(mysql_real_escape_string($_POST['totalseats']), "text"),
					GetSQLValueString(mysql_real_escape_string($_POST['eventcost']), "text"),
					GetSQLValueString(mysql_real_escape_string($_POST['eventdescription']), "text"),
							 GetSQLValueString(mysql_real_escape_string($_POST['eventdate']), "date"),
					GetSQLValueString(mysql_real_escape_string($_POST['eventid']), "int"));
			
		mysql_select_db($database_chewsrite, $chewsrite);
		$Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());
		
		if(isset($_POST['eventimage']))
		{
			$insertSQL = sprintf("UPDATE events SET eventimage = %s WHERE eventid = %s",
						GetSQLValueString(mysql_real_escape_string($_POST['eventimage']), "text"),
						GetSQLValueString(mysql_real_escape_string($_POST['eventid']), "int"));

			mysql_select_db($database_chewsrite, $chewsrite);
			$Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());
		}
		
		$object->status = "event updated";
	}
	else
	{
		
		//query venue location city state zip
		
		$insertSQL = sprintf("INSERT INTO events (eventname, city, state, zip, totalseats, availableseats, eventcost, venueid, eventdate, eventdescription, eventimage, datecreated) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
						GetSQLValueString(mysql_real_escape_string($_POST['eventname']), "text"),
						GetSQLValueString(mysql_real_escape_string($_POST['city']), "text"),
						GetSQLValueString(mysql_real_escape_string($_POST['state']), "text"),
						GetSQLValueString(mysql_real_escape_string($_POST['zip']), "text"),
						GetSQLValueString(mysql_real_escape_string($_POST['totalseats']), "int"),
						GetSQLValueString(mysql_real_escape_string($_POST['totalseats']), "int"),
						GetSQLValueString(mysql_real_escape_string($_POST['eventcost']), "double"),
						GetSQLValueString(mysql_real_escape_string($_POST['venueid']), "int"),
						GetSQLValueString(mysql_real_escape_string($_POST['eventdate']), "date"),
						GetSQLValueString(mysql_real_escape_string($_POST['eventdescription']), "text"),
						GetSQLValueString(mysql_real_escape_string($_POST['eventimage']), "text"),
						GetSQLValueString(mysql_real_escape_string($date), "date"));

		mysql_select_db($database_chewsrite, $chewsrite);
		$Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());	

		$last_id = mysql_insert_id();	

		$object->status = "event added";
		$object->eventid = $last_id;
	}
}

echo "{\"data\":";
echo "{\"eventsData\":";
echo json_encode( $object );
echo "}";
echo "}";

?>
