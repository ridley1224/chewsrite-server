<?php

header('Content-type: application/json');

require_once('../../Connections/chewsrite.php');
include("functions.php");

mysql_select_db($database_chewsrite, $chewsrite);

//$response = ["URLOfTheSecondWebsite" => $request['websites'][1]['URL']];

$string;
$date = date("Y-m-d H:i:s");


$object = new stdClass();
$object->status = "goals not saved";

//$_POST['userid']="1";
//$_POST['insert']="true";
//$_POST['wCalories']="500";
//$_POST['wCarbohydrates']="0";
//$_POST['wProtein']="0";
//$_POST['wFat']="2";
//$_POST['wSaturatedFat']="0";
//$_POST['wUnsaturatedFat']="0";
//$_POST['wTransFat']="0";
//$_POST['wCholestrol']="0";
//$_POST['wSodium']="0";
//$_POST['wPotassium']="0";
//$_POST['wFiber']="0";
//$_POST['wSugars']="0";
//$_POST['wVitaminA']="0";
//$_POST['wVitaminC']="0";
//$_POST['wCalcium']="0";
//$_POST['wIron']="0";
//$_POST['dCalories']="100";
//$_POST['dCarbohydrates']="0";
//$_POST['dProtein']="2";
//$_POST['dFat']="0";
//$_POST['dSaturatedFat']="0";
//$_POST['dUnsaturatedFat']="0";
//$_POST['dTransFat']="0";
//$_POST['dCholestrol']="0";
//$_POST['dSodium']="0";
//$_POST['dPotassium']="0";
//$_POST['dFiber']="0";
//$_POST['dSugars']="0";
//$_POST['dVitaminA']="0";
//$_POST['dVitaminC']="0";
//$_POST['dCalcium']="0";
//$_POST['dIron']="0";



if(isset($_POST['userid']) || isset($_POST['familyid']))
{
    if(isset($_POST['userid']))
    {
        $id = $_POST['userid'];
        //$columnname = "userid";
    }
    else
    {
        $id = $_POST['familyid'];
        //$columnname = "familyid";
    }
    
    $columnname = "userid";
    
//    userid=1&wCalories=200&wCarbohydrates=0.0&wProtein=0.0&wFat=0.0&wSaturatedFat=0.0&wUnsaturatedFat=0.0&wTransFat=0.0&wCholestrol=0.0&wSodium=0.0&wPotassium=0.0&wFiber=0.0&wSugars=0.0&wVitaminA=0.0&wVitaminC=0.0&wCalcium=0.0&wIron=0.0&dCalories=100&dCarbohydrates=0.0&dProtein=0.0&dFat=0.0&dSaturatedFat=0.0&dUnsaturatedFat=0.0&dTransFat=0.0&dCholestrol=0.0&dSodium=0.0&dPotassium=0.0&dFiber=0.0&dSugars=0.0&dVitaminA=0.0&dVitaminC=0.0&dCalcium=0.0&dIron=0.0
    
    if(isset($_POST['insert']))
    {
        $insertSQL = sprintf("INSERT INTO usernutritiongoals (wCalories, wCarbohydrates, wProtein, wFat, wSaturatedFat, wUnsaturatedFat, wTransFat, wCholestrol, wSodium, wPotassium, wFiber, wSugars, wVitaminA, wVitaminC, wCalcium, wIron, dCalories, dCarbohydrates, dProtein, dFat, dSaturatedFat, dUnsaturatedFat, dTransFat, dCholestrol, dSodium, dPotassium, dFiber, dSugars, dVitaminA, dVitaminC, dCalcium, dIron, {$columnname}) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
				GetSQLValueString(mysql_real_escape_string($_POST['wCalories']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['wCarbohydrates']), "int"),
				GetSQLValueString(mysql_real_escape_string($_POST['wProtein']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['wFat']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['wSaturatedFat']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['wUnsaturatedFat']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['wTransFat']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['wCholestrol']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['wSodium']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['wPotassium']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['wFiber']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['wSugars']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['wVitaminA']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['wVitaminC']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['wCalcium']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['wIron']), "text"),
                GetSQLValueString(mysql_real_escape_string($_POST['dCalories']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['dCarbohydrates']), "int"),
				GetSQLValueString(mysql_real_escape_string($_POST['dProtein']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['dFat']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['dSaturatedFat']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['dUnsaturatedFat']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['dTransFat']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['dCholestrol']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['dSodium']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['dPotassium']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['dFiber']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['dSugars']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['dVitaminA']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['dVitaminC']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['dCalcium']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['dIron']), "text"),
				GetSQLValueString(mysql_real_escape_string($id), "int"));

        mysql_select_db($database_chewsrite, $chewsrite);
        $Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());	

        $last_id = mysql_insert_id();	
        $object->goalsid = $last_id;
        $object->status = "goals info saved";
    }
	else
    {
        //update
        
        $object->status = "goals info updated";
    }
    
    $object->insertSQL = $insertSQL;	
}

echo "{\"data\":";
echo "{\"goalsData\":";
echo json_encode( $object );
echo "}";
echo "}";

?>
