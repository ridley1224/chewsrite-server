<?php 

//header('Content-type: application/json');

require_once('../../Connections/chewsrite.php'); 
include("functions.php");
include("en-de.php");

//$_POST['userid'] = "22";
//$_POST['gender'] = "0";
//$_POST['age'] = "20";


mysql_select_db($database_chewsrite, $chewsrite);


if(isset($_POST['gender']) && isset($_POST['userid']) && isset($_POST['age']))
{
    $age = (int)$_POST['age'];
    
    $query_rsPregnantStatus = "SELECT userid FROM users WHERE ispregnant = 1 AND userid = {$_POST['userid']}";

    $rsPregnantStatus = mysql_query($query_rsPregnantStatus, $chewsrite) or die(mysql_error());
    $row_rsPregnantStatus = mysql_fetch_assoc($rsPregnantStatus);
    $totalRows_rsPregnantStatus = mysql_num_rows($rsPregnantStatus);

    if($totalRows_rsPregnantStatus)
    {
         $categorylist[] = "4";
    }

    //get user dietary concern categories

    $query_rsUserConcerns = "SELECT * FROM userdietaryconcerns WHERE userid = {$_POST['userid']}";

    $rsUserConcerns = mysql_query($query_rsUserConcerns, $chewsrite) or die(mysql_error());
    $row_rsUserConcerns = mysql_fetch_assoc($rsUserConcerns);
    $totalRows_rsUserConcerns = mysql_num_rows($rsUserConcerns);
    
    //echo "<p>concern query: {$query_rsUserConcerns}</p>";

    if($totalRows_rsUserConcerns)
    {
        do {

            $dietaryconcerns = explode(',',blankNull($row_rsUserConcerns['selections']));

        }  while ($row_rsUserConcerns = mysql_fetch_assoc($rsUserConcerns));
    }
    
    
    if($age < 18)
    {
        $categorylist[] = "0";
    }
    else
    {
        $categorylist[] = "1";
    }
    

    foreach ($dietaryconcerns as $concern)
    {
        //echo "concernid: {$concern}<br>";
        
        if($concern == "1")
        {
            //diabetic

            $categorylist[] = "2";
        }
        else if($concern == "8")
        {
            //pregant/lactating

            $categorylist[] = "3";
        }
        else if($concern == "23" || $concern == "24")
        {
            //vegan/vegitarian

            $categorylist[] = "5";
        }
    }
    
    //build gender/age query
    
    if($_POST['gender'] == 0)
    {
        $sex = "Male";
    }
    else
    {
        $sex = "Female";
    }
    
    if($age < 4)
    {
        $ageString = (string)$age;
    }
    else if($age > 3 && $age < 6)
    {
        $ageString = "3-6";
    }
    else if($age >= 4 && $age <= 5)
    {
        $ageString = "4-5";
    }
    else if($age >= 6 && $age <= 8)
    {
        $ageString = "6-8";
    }
    else if($age >= 9 && $age <= 10)
    {
        $ageString = "9-10";
    }
    else if($age >= 11 && $age <= 12)
    {
        $ageString = "11-12";
    }
    else if($age >= 13 && $age <= 14)
    {
        $ageString = "13-14";
    }
    else if($age >= 15 && $age <= 16)
    {
        $ageString = "15-16";
    }
    else if($age >= 17 && $age <= 18)
    {
        $ageString = "17-18";
    }
    else if($age >= 19 && $age <= 25)
    {
        $ageString = "19-25";
    }
    else if($age >= 26 && $age <= 50)
    {
        $ageString = "26-50";
    }
    else if($age >= 51 && $age <= 65)
    {
        $ageString = "51-65";
    }
    else if($age > 65)
    {
        $ageString = "65+";
    }
    
    $ageString = $sex . " Age " . $ageString;
    
    
    //check if there are multiple categories selected
    
    if (count($categorylist))
    {
        $categoryString = " AND category IN (" . implode(",",$categorylist) .")";
    }
    else
    {
        //check if adult
        
        if($age < 18)
        {
            $categoryString = " AND category = 0";
        }
        else
        {
            $categoryString = " AND category = 1";
        }
    }
    
    //SELECT * from nutritiondefaults WHERE (groupname = 'Male Age 26-50' AND category IN (1,3)) AND groupname != 'Per 1,000 Calories'
    
    $query_rsRecipes = "SELECT groupname, MIN(Calories) as 'Calories', MIN(Carbohydrates) as 'Carbohydrates', MAX(Protein) as 'Protein', MIN(Fat) as 'Fat', MIN(SaturatedFat) as 'SaturatedFat', MIN(UnsaturatedFat) as 'UnsaturatedFat', MIN(TransFat) as 'TransFat', MIN(Cholesterol) as 'Cholesterol', MIN(Sodium) as 'Sodium', MAX(Potassium) as 'Potassium', MAX(Fiber) as 'Fiber', MIN(Sugars) as 'Sugars', MIN(VitaminA) as 'VitaminA', MIN(VitaminC) as 'VitaminC', MAX(Calcium) as 'Calcium', MAX(Iron) as 'Iron' from nutritiondefaults WHERE (groupname = '{$ageString}'{$categoryString}) AND groupname != 'Per 1,000 Calories'";
    
	//$query_rsRecipes = "SELECT * from nutritiondefaults WHERE (groupname = '{$ageString}'{$categoryString}) AND groupname != 'Per 1,000 Calories'";
    
    //echo "<p>query: {$query_rsRecipes}</p>";

	$rsRecipes = mysql_query($query_rsRecipes, $chewsrite) or die(mysql_error());
	$row_rsRecipes = mysql_fetch_assoc($rsRecipes);
	$totalRows_rsRecipes = mysql_num_rows($rsRecipes);

	if($totalRows_rsRecipes)
	{
		do {

			$object = new stdClass();
            
            $object->dCalories = blankNull($row_rsRecipes['Calories']);
            $object->dCarbohydrates = blankNull($row_rsRecipes['Carbohydrates']);
            $object->dProtein = blankNull($row_rsRecipes['Protein']);
            $object->dFat = blankNull($row_rsRecipes['Fat']);
            $object->dSaturatedFat = blankNull($row_rsRecipes['SaturatedFat']);
            $object->dUnsaturatedFat = blankNull($row_rsRecipes['UnsaturatedFat']);
            $object->dTransFat = blankNull($row_rsRecipes['TransFat']);
            $object->dCholestrol = blankNull($row_rsRecipes['Cholesterol']);
            $object->dSodium = blankNull($row_rsRecipes['Sodium']);
            $object->dPotassium = blankNull($row_rsRecipes['Potassium']);
            $object->dFiber = blankNull($row_rsRecipes['Fiber']);
            $object->dSugars = blankNull($row_rsRecipes['Sugars']);
            $object->dVitaminA = blankNull($row_rsRecipes['VitaminA']);
            $object->dVitaminC = blankNull($row_rsRecipes['VitaminC']);
            $object->dCalcium = blankNull($row_rsRecipes['Calcium']);
            $object->dIron = blankNull($row_rsRecipes['Iron']);
            
            
            //UPDATE nutritiondefaults SET Calories = Replace(Calories,',','')
            
            
            
//            echo $row_rsRecipes['Calories'] . "<br>";
//            echo (int)$row_rsRecipes['Calories'] *7 . "<br>";
//            
//            
			$object->wCalories = (string)blankNull($row_rsRecipes['Calories']*7);
            $object->wCarbohydrates = (string)blankNull($row_rsRecipes['Carbohydrates']*7);
            $object->wProtein = (string)blankNull($row_rsRecipes['Protein']*7);
            $object->wFat = (string)blankNull($row_rsRecipes['Fat']*7);
            $object->wSaturatedFat = (string)blankNull($row_rsRecipes['SaturatedFat']*7);
            $object->wUnsaturatedFat = (string)blankNull($row_rsRecipes['UnsaturatedFat']*7);
            $object->wTransFat = (string)blankNull($row_rsRecipes['TransFat']*7);            
            $object->wPotassium = (string)blankNull($row_rsRecipes['Potassium']*7);
            $object->wFiber = (string)blankNull($row_rsRecipes['Fiber']*7);
            $object->wSugars = (string)blankNull($row_rsRecipes['Sugars']*7);
            $object->wVitaminA = (string)blankNull($row_rsRecipes['VitaminA']*7);
            $object->wVitaminC = (string)blankNull($row_rsRecipes['VitaminC']*7);
            $object->wCalcium = (string)blankNull($row_rsRecipes['Calcium']*7);
            $object->wIron = (string)blankNull($row_rsRecipes['Iron']*7);
            
            
            if($row_rsRecipes['Cholesterol'] != "N/A" && strpos($row_rsRecipes['Cholesterol'], '<') !== false)
            {
                $a = explode("<",$row_rsRecipes['Cholesterol']);
                $a = $a[1] * 7;
                $a = "<{$a}";
            }
            else
            {
                $a = $a * 7;
            }
            
            $object->wCholestrol = $a;
            
            if($row_rsRecipes['Sodium'] != "N/A" && strpos($row_rsRecipes['Sodium'], '<') !== false)
            {
                $a = explode("<",$row_rsRecipes['Sodium']);
                $a = $a[1] * 7;
                $a = "<{$a}";
            }
            else
            {
                $a = $a * 7;
            }
            
            $object->wSodium = $a;
            
            
            
            
            
            //$object->wCholestrol = blankNull($row_rsRecipes['Cholesterol']);
            //$object->wSodium = blankNull($row_rsRecipes['Sodium']);
            
            //$list[] = $object;
			
		}  while ($row_rsRecipes = mysql_fetch_assoc($rsRecipes));
	}
}
	
echo "{\"data\":";
echo "{\"nutritionData\":";
echo json_encode( $object );
echo "}";
echo "}";

?>
