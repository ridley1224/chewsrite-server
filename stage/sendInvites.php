<?php require_once('../../Connections/chewsrite.php'); 

include("functions.php");
include("en-de.php");

//$_POST['userid'] = "1";
//$_POST['groupid'] = "3";
//$_POST['userids'] = "22,23";
//$_POST['groupinvites'] = "22,23";

$object = new stdClass();
$date = date("Y-m-d H:i:s");


if(isset($_POST['emails']))
{	
	$emails = explode(',',$_POST['emails']);
	
	$query_rsInvites = "SELECT firstname, lastname from users WHERE userid = '" . $_POST['userid'] . "' ";

	$rsInvites = mysql_query($query_rsInvites, $chewsrite) or die(mysql_error());
	$row_rsInvites = mysql_fetch_assoc($rsInvites);
	$totalRows_rsInvites = mysql_num_rows($rsInvites);
	
//	$insertSQL = sprintf("INSERT INTO invites (emails, userid, invitedate) VALUES (%s, %s, %s)",
//					GetSQLValueString(mysql_real_escape_string($_POST['emails']), "int"),
//					GetSQLValueString(mysql_real_escape_string($_POST['userid']), "int"),
//					GetSQLValueString(mysql_real_escape_string($date), "date"));
//
//	mysql_select_db($database_chewsrite, $chewsrite);
//	$Result2 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());	
	
	foreach($emails as $email) {
        
        $insertSQL = sprintf("INSERT INTO appinvites (email, userid, invitedate) VALUES (%s, %s, %s)",
					GetSQLValueString(mysql_real_escape_string($_POST['emails']), "text"),
					GetSQLValueString(mysql_real_escape_string($_POST['userid']), "int"),
					GetSQLValueString(mysql_real_escape_string($date), "date"));

        mysql_select_db($database_chewsrite, $chewsrite);
        $Result2 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());        
        
		
		$subject = 'Chewsrite Invitation';
		$html = "New invite from " . $row_rsInvites['firstname'] . " " . $row_rsInvites['lastname'];
		$text = "New invite from " . $row_rsInvites['firstname'] . " " . $row_rsInvites['lastname'];
		$from = 'alerts@chewsrite.com';
		$to = $email;

		include("send-email.php");
	}
	
	$object->status = "invites sent";	
}
else if(isset($_POST['groupinvites']))
{		
	$query_rsInvites = "SELECT userid, deviceid from users WHERE userid IN(" . $_POST['userids'] . ")";

	$rsInvites = mysql_query($query_rsInvites, $chewsrite) or die(mysql_error());
	$row_rsInvites = mysql_fetch_assoc($rsInvites);
	$totalRows_rsInvites = mysql_num_rows($rsInvites);
	
	$message = "You have been invited to join {$_POST['groupname']}";
	$url = "http://chewsrite.com";
	//$body = $_POST['groupid'];
	
	
	if($totalRows_rsInvites)
	{
		do {			
			
			$insertSQL = sprintf("INSERT INTO groupinvites (receiverid, senderid, groupid, invitedate) VALUES (%s, %s, %s, %s)",
				GetSQLValueString(mysql_real_escape_string($row_rsInvites['userid']), "int"),
				GetSQLValueString(mysql_real_escape_string($_POST['userid']), "int"),
				GetSQLValueString(mysql_real_escape_string($_POST['groupid']), "int"),
				GetSQLValueString(mysql_real_escape_string($date), "date"));

			mysql_select_db($database_chewsrite, $chewsrite);
			$Result2 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());	

						
			$deviceToken = $row_rsInvites['deviceid'];
			$_POST['token'] = $row_rsInvites['deviceid'];

			$body['aps'] = array(
					  'alert' => $message,
					  'sound' => 'default',
					  'link_url' => $url,
					  'category' => "GROUP_INVITE",
					  'body' => $_POST['groupid'],
					  'badge' => 1,
					  );

			include("send-push-notification.php");

		}  while ($row_rsInvites = mysql_fetch_assoc($rsInvites));
	}
		
	$object->status = "invites sent to {$totalRows_rsInvites} users";
		
}

echo "{\"data\":";
echo "{\"inviteData\":";
echo json_encode( $object );
echo "}";
echo "}";

?>

<?php
//mysql_free_result($rsGetUser);
?>
