<?php

header('Content-type: application/json');

require_once('../../Connections/chewsrite.php');
include("functions.php");

mysql_select_db($database_chewsrite, $chewsrite);

//$response = ["URLOfTheSecondWebsite" => $request['websites'][1]['URL']];

$date = date("Y-m-d H:i:s");

//$_POST['recipeid'] = "1";
//$_POST['userid'] = "1";


$object = new stdClass();
$object->status = "likes not saved";


if(isset($_POST['userid']) || isset($_POST['familyid']))
{
    if(isset($_POST['userid']))
    {
        $id = $_POST['userid'];
        //$columnname = "userid";
    }
    else
    {
        $id = $_POST['familyid'];
        //$columnname = "familyid";
    }
    
    $columnname = "userid";
    
	$query_rsRecipes1 = "SELECT likeid FROM userfoodlikes WHERE {$columnname} = " . $id;
	$rsRecipes1 = mysql_query($query_rsRecipes1, $chewsrite) or die(mysql_error());
	$row_rsRecipes1 = mysql_fetch_assoc($rsRecipes1);
	$totalRows_rsRecipes1 = mysql_num_rows($rsRecipes1);

	if($totalRows_rsRecipes1)
	{	
        //to do randall
        //finish update
        
		$insertSQL = sprintf("UPDATE userfoodlikes SET likes = %s, dislikes = %s WHERE {$columnname} = %s",
						GetSQLValueString(mysql_real_escape_string($_POST['likes']), "text"),
                        GetSQLValueString(mysql_real_escape_string($_POST['dislikes']), "text"),
						GetSQLValueString(mysql_real_escape_string($id), "int"));

		mysql_select_db($database_chewsrite, $chewsrite);
		$Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());

		$object->status = "likes updated";
	}
	else
	{
		$insertSQL = sprintf("INSERT INTO userfoodlikes (likes, dislikes, {$columnname}, createddate) VALUES (%s, %s, %s, %s)",
						GetSQLValueString(mysql_real_escape_string($_POST['likes']), "text"),
                        GetSQLValueString(mysql_real_escape_string($_POST['dislikes']), "text"),
						GetSQLValueString(mysql_real_escape_string($id), "int"),
						GetSQLValueString(mysql_real_escape_string($date), "date"));

		mysql_select_db($database_chewsrite, $chewsrite);
		$Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());	

		$last_id = mysql_insert_id();	
		$object->favoriteid = $last_id;
		$object->status = "likes saved";
        $object->insertSQL = $insertSQL;
        $object->query = $query_rsRecipes1;
	}
}

echo "{\"data\":";
echo "{\"likesData\":";
echo json_encode( $object );
echo "}";
echo "}";

?>
