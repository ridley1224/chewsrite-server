<?php

header('Content-type: application/json');


//likes=1&dislikes=2&concerns=5&cuisines=1&userid=24&devStatus=dev
    
//$_POST['userid'] = "24";
//$_POST['dislikes'] = "1";
//$_POST['likes'] = "2";
//$_POST['concerns'] = "5";
//$_POST['cuisines'] = "1";
//$_POST['devStatus'] = "dev";

require_once('../../Connections/chewsrite.php');
include("functions.php");

mysql_select_db($database_chewsrite, $chewsrite);

//$response = ["URLOfTheSecondWebsite" => $request['websites'][1]['URL']];

$date = date("Y-m-d H:i:s");

//$_POST['recipeid'] = "1";
//$_POST['userid'] = "1";


$object = new stdClass();
$object->status = "prefs not saved";


if(isset($_POST['userid']))
{
    $id = $_POST['userid'];
    $columnname = "userid";
    
    $insertSQL = sprintf("UPDATE usercuisines SET selections = %s WHERE {$columnname} = %s",
						GetSQLValueString(mysql_real_escape_string($_POST['cuisines']), "text"),
						GetSQLValueString(mysql_real_escape_string($id), "int"));

    mysql_select_db($database_chewsrite, $chewsrite);
    $Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());

    $object->status = "1";
    
    
    $insertSQL2 = sprintf("UPDATE userdietaryconcerns SET selections = %s WHERE {$columnname} = %s",
						GetSQLValueString(mysql_real_escape_string($_POST['concerns']), "text"),
						GetSQLValueString(mysql_real_escape_string($id), "int"));

    mysql_select_db($database_chewsrite, $chewsrite);
    $Result1 = mysql_query($insertSQL2, $chewsrite) or die(mysql_error());

    $object->status = "2";
    
    
    $insertSQL3 = sprintf("UPDATE userfoodlikes SET likes = %s, dislikes = %s WHERE {$columnname} = %s",
						GetSQLValueString(mysql_real_escape_string($_POST['likes']), "text"),
                        GetSQLValueString(mysql_real_escape_string($_POST['dislikes']), "text"),
						GetSQLValueString(mysql_real_escape_string($id), "int"));

    mysql_select_db($database_chewsrite, $chewsrite);
    $Result1 = mysql_query($insertSQL3, $chewsrite) or die(mysql_error());

    $object->status = "3";
    
    $object->query = json_encode($insertSQL);
    $object->query2 = json_encode($insertSQL2);
    $object->query3 = json_encode($insertSQL3);
}

echo "{\"data\":";
echo "{\"prefsData\":";
echo json_encode( $object );
echo "}";
echo "}";

?>
