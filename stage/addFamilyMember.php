<?php

header('Content-type: application/json');

require_once('../../Connections/chewsrite.php');
include("functions.php");

mysql_select_db($database_chewsrite, $chewsrite);

//$response = ["URLOfTheSecondWebsite" => $request['websites'][1]['URL']];

$string;
$date = date("Y-m-d H:i:s");


$object = new stdClass();
$object->status = "family member not saved";

//user type usertypes
//0 = admin
//1 = regular user
//2 = family member

if(isset($_POST['newprofile']) && isset($_POST['userid']))
{
	$insertSQL = sprintf("INSERT INTO users (fullname, height, weight, dob, usertype, gender, userimage, datecreated) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
				GetSQLValueString(mysql_real_escape_string($_POST['fullname']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['height']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['weight']), "int"),
                GetSQLValueString(mysql_real_escape_string($_POST['dob']), "date"),
                GetSQLValueString(1, "int"),
                GetSQLValueString(mysql_real_escape_string($_POST['gender']), "int"),
                GetSQLValueString(mysql_real_escape_string($_POST['userimage']), "text"),
				GetSQLValueString(mysql_real_escape_string($date), "date"));

	mysql_select_db($database_chewsrite, $chewsrite);
	$Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());	
    $last_id = mysql_insert_id();
    
    $insertSQL = sprintf("INSERT INTO familymembers (fullname, height, weight, dob, relationshipid, gender, userimage, userid, referrerid, datecreated) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
				GetSQLValueString(mysql_real_escape_string($_POST['fullname']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['height']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['weight']), "int"),
                GetSQLValueString(mysql_real_escape_string($_POST['dob']), "date"),
                GetSQLValueString(mysql_real_escape_string($_POST['relationshipid']), "int"),
                GetSQLValueString(mysql_real_escape_string($_POST['gender']), "int"),
                GetSQLValueString(mysql_real_escape_string($_POST['userimage']), "text"),
                GetSQLValueString(mysql_real_escape_string($last_id), "int"),
                GetSQLValueString(mysql_real_escape_string($_POST['userid']), "int"),
				GetSQLValueString(mysql_real_escape_string($date), "date"));

	mysql_select_db($database_chewsrite, $chewsrite);
	$Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());	

	//$last_id = mysql_insert_id();	
	$object->newuserid = $last_id;
	$object->status = "family member saved";
}
else if(isset($_POST['updateprofile']) && isset($_POST['familyid']))
{
    $updateSQL = sprintf("UPDATE users SET fullname=%s, height=%s, weight=%s, dob=%s, relationshipid=%s, gender=%s, userimage=%s, dateupdated=%s WHERE memberid = %s ",
                        GetSQLValueString(mysql_real_escape_string($_POST['fullname']), "text"),
                        GetSQLValueString(mysql_real_escape_string($_POST['height']), "text"),
                        GetSQLValueString(mysql_real_escape_string($_POST['weight']), "int"),
                        GetSQLValueString(mysql_real_escape_string($_POST['dob']), "date"),
                        GetSQLValueString(mysql_real_escape_string($_POST['relationshipid']), "int"),
                        GetSQLValueString(mysql_real_escape_string($_POST['gender']), "int"),
                        GetSQLValueString(mysql_real_escape_string($_POST['userimage']), "text"),
                        GetSQLValueString(mysql_real_escape_string($_POST['familyid']), "int"),
                        GetSQLValueString(mysql_real_escape_string($date), "date"));

    mysql_select_db($database_chewsrite, $chewsrite);
    $Result1 = mysql_query($updateSQL, $chewsrite) or die(mysql_error());	
    
    $updateSQL = sprintf("UPDATE familymembers SET fullname=%s, height=%s, weight=%s, dob=%s, relationshipid=%s, gender=%s, userimage=%s, dateupdated=%s WHERE memberid = %s ",
                        GetSQLValueString(mysql_real_escape_string($_POST['fullname']), "text"),
                        GetSQLValueString(mysql_real_escape_string($_POST['height']), "text"),
                        GetSQLValueString(mysql_real_escape_string($_POST['weight']), "int"),
                        GetSQLValueString(mysql_real_escape_string($_POST['dob']), "date"),
                        GetSQLValueString(mysql_real_escape_string($_POST['relationshipid']), "int"),
                        GetSQLValueString(mysql_real_escape_string($_POST['gender']), "int"),
                        GetSQLValueString(mysql_real_escape_string($_POST['userimage']), "text"),
                        GetSQLValueString(mysql_real_escape_string($_POST['familyid']), "int"),
                        GetSQLValueString(mysql_real_escape_string($date), "date"));

    mysql_select_db($database_chewsrite, $chewsrite);
    $Result1 = mysql_query($updateSQL, $chewsrite) or die(mysql_error());	
    
    
    $object->status = "family member updated";
}

echo "{\"data\":";
echo "{\"userData\":";
echo json_encode( $object );
echo "}";
echo "}";

?>
