<?php

$date = date( "Y-m-d H:i:s" );

if ( ( isset( $_POST[ "MM_insert" ] ) ) && ( $_POST[ "MM_insert" ] == "form1" ) ) {

$api = "i07gYSiTl4Ad2h6E7C6Z4XbcwD7cianBwuCiytq6";

$curl = curl_init();

curl_setopt_array( $curl, array(
    CURLOPT_URL => "http://DEMO_KEY@api.nal.usda.gov/ndb/search?api_key={$api}",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_POSTFIELDS => "{\"q\":\"{$_POST['support']}\",\"offset\":\"0\",\"ds\":\"Standard Reference\", \"max\":\"1\"}",
    CURLOPT_HTTPHEADER => array(
        "Accept: */*",
        "Accept-Encoding: gzip, deflate",
        "Authorization: Basic REVNT19LRVk6",
        "Cache-Control: no-cache",
        "Connection: keep-alive",
        "Content-Length: 64",
        "Content-Type: application/json",
        "Cookie: JSESSIONID=66A56FCB3BDCA4805BC4844773347486",
        "Host: api.nal.usda.gov",
        "Postman-Token: c2ba397a-c6c1-4698-9ae6-e6377d0a72b3,32cc1def-a206-48fa-b20d-4148ad9bf1de",
        "User-Agent: PostmanRuntime/7.15.2",
        "cache-control: no-cache"
    ),
) );

$response = curl_exec( $curl );
$err = curl_error( $curl );

curl_close( $curl );

if ( $err ) {
    echo "cURL Error #:" . $err;
} else {
    //echo $response;

    $json = json_decode( $response );

    $debug = false;

//    if ( $debug ) {
//        echo '<pre>';
//        print_r( $response );
//        echo '</pre>';
//    }

    $foodID = $json->list->item[ 0 ]->ndbno;

    echo "foodID: {$foodID}<br>";

    $curl2 = curl_init();

    curl_setopt_array( $curl2, array(
        CURLOPT_URL => "http://DEMO_KEY@api.nal.usda.gov/ndb/reports?ndbno=api_key={$api}",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_POSTFIELDS => "{\"ndbno\":\"{$foodID}\",\"type\":\"b\"}",
        CURLOPT_HTTPHEADER => array(
            "Accept: */*",
            "Accept-Encoding: gzip, deflate",
            "Authorization: Basic REVNT19LRVk6",
            "Cache-Control: no-cache",
            "Connection: keep-alive",
            "Content-Length: 28",
            "Content-Type: application/json",
            "Cookie: JSESSIONID=66A56FCB3BDCA4805BC4844773347486",
            "Host: api.nal.usda.gov",
            "Postman-Token: 920edd2f-ec21-4d32-89c5-89d7eccbb315,4b75c822-33a3-4b7c-a99f-05a23744ff38",
            "User-Agent: PostmanRuntime/7.15.2",
            "cache-control: no-cache"
        ),
    ) );

    $response2 = curl_exec( $curl2 );
    $err2 = curl_error( $curl2 );

    curl_close( $curl2 );

    if ( $err2 ) {
        echo "cURL Error #:" . $err2;
    } else {
        //echo $response2;
        
        $json2 = json_decode( $response2 );
        
        $nutrients = $json2->report->food->nutrients;

//        if ( $debug ) {
//
//            echo '<pre>';
//            print_r( $response2 );
//            echo '</pre>';
//        }
        
        //var_dump($nutrients);
        
        foreach($nutrients as $nutrient)
        {
            echo "name: {$nutrient->name}<br>";
        }
    }
}
    
}

?>

<!DOCTYPE html>
<html>
<head>
    
    <title>Support - AIScribe</title>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0">
</head>
<body>
    <div id="primaryContainer" class="primaryContainer clearfix">
        <div id="headerBG" class="clearfix">
            <span style="font-size:30px;cursor:pointer"></span>
        </div>
        <?php include("includes/nav.php");?>
        <div id="titleDiv" class="clearfix">
            <div id="headerTxtBG" class="clearfix">
                <p id="headerLbl">Food Data</p>
            </div>
        </div>
        <div id="contentBG" class="clearfix">
            <form action="<?php echo $editFormAction; ?>" id="form1" name="form1" method="POST">
              <table width="100%" cellpadding="5" cellspacing="5">
                  <tbody>
                        <?php if(isset($status)) { ?>
                        <tr>
                            <td width="93%" style="color: red">
                                <?php echo $status; ?>
                            </td>
                        </tr>

                        <?php } ?>
                        <tr>
                            <td><textarea name="support" class="support" id="support"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="hidden" name="MM_insert" value="form1">
                                <input type="submit" name="submit" id="submit" value="Submit">
                            </td>
                        </tr>
                    </tbody>
                </table>
                
            </form>
        </div>
    </div>

</body>
</html>