 <?php include("includes/devStatus.php"); 

require_once('../../Connections/chewsrite.php'); 
include("functions.php");
include("auth.php");

//$_SESSION['userid'] = "24";

//var_dump($_SESSION);

date_default_timezone_set('America/Detroit');
$date = date("Y-m-d H:i:s");

$colname_rsRecipeDetails = "-1";
if (isset($_GET['recipeid'])) {
  $colname_rsRecipeDetails = de($_GET['recipeid']);
}

mysql_select_db($database_chewsrite, $chewsrite);
$query_rsRecipeDetails = sprintf("SELECT a.*, b.* FROM (SELECT * FROM recipes WHERE recipeid = %s) as a INNER JOIN (SELECT fullname,userid from users) as b on a.userid = b.userid", GetSQLValueString($colname_rsRecipeDetails, "int"));

//echo $query_rsRecipeDetails;

$rsRecipeDetails = mysql_query($query_rsRecipeDetails, $chewsrite) or die(mysql_error());
$row_rsRecipeDetails = mysql_fetch_assoc($rsRecipeDetails);
$totalRows_rsRecipeDetails = mysql_num_rows($rsRecipeDetails);

$selectedcuisines = explode(",",$row_rsRecipeDetails['cuisinetags']);
$selecteddietaryConcerns = explode(",",$row_rsRecipeDetails['dietaryconcerntags']);




mysql_select_db($database_chewsrite, $chewsrite);
$query_rsSelectedCuisines = "SELECT * FROM cuisines WHERE active = 1";
$rsSelectedCuisines = mysql_query($query_rsSelectedCuisines, $chewsrite) or die(mysql_error());
$row_rsSelectedCuisines = mysql_fetch_assoc($rsSelectedCuisines);
$totalRows_rsSelectedCuisines = mysql_num_rows($rsSelectedCuisines);

$query_rsDietaryConcerns = "SELECT * FROM dietaryconcerns WHERE active = 1";
$rsDietaryConcerns = mysql_query($query_rsDietaryConcerns, $chewsrite) or die(mysql_error());
$row_rsDietaryConcerns = mysql_fetch_assoc($rsDietaryConcerns);
$totalRows_rsDietaryConcerns = mysql_num_rows($rsDietaryConcerns);


?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Add Recipe</title>
<link href="admin.css" rel="stylesheet" type="text/css">
    <script src="../../jquery/jquery-1.11.1.min.js"></script>	
<script src="upload-s3.js"></script>
    
    
</head>

<body>
    
<?php include("includes/nav.php"); ?>
    
<form action="<?php echo $editFormAction; ?>" method="POST" enctype="multipart/form-data" name="form1" id="form1">

<p>&nbsp;</p>

<h1>Add Recipe</h1>
<p>&nbsp;</p>
    <div id="uploadStatus"></div>

<table width="100%" cellspacing="5" class="table">
  <tbody>
    <tr>
      <td width="123"><strong><img alt="" width="80" height="80"/></strong></td>
      <td width="813"><label for="fileField">Select Image:</label>
        <input type="file" name="fileField" id="fileField"></td>
    </tr>
    <tr>
      <td><strong>Recipe</strong></td>
      <td><input name="recipename" type="text" class="input" id="recipename" placeholder="Enter recipe name"></td>
    </tr>
    <tr>
      <td><strong>Description</strong></td>
      <td><textarea name="description" class="description" id="description"></textarea></td>
    </tr>
    <tr>
      <td><strong>Source</strong></td>
      <td><input name="source" type="text" class="input" id="source" placeholder="Enter recipe source"></td>
    </tr>
    <tr>
      <td><strong>Source URL</strong></td>
      <td><input name="sourceurl" type="text" class="input" id="sourceurl" placeholder="Enter source URL"></td>
    </tr>
    <tr>
      <td><strong>Servings</strong></td>
      <td><input name="servings" type="text" class="input" id="servings" placeholder="Enter serving amount"></td>
    </tr>
    <tr>
      <td><strong>Prep Time</strong></td>
      <td><input name="preptime" type="text" class="input" id="preptime" placeholder="Enter prep time">
        <select name="prepunits" class="units" id="prepunits">
          <option value="minutes">minutes</option>
          <option value="hours">hours</option>
          <option value="days">days</option>
        </select></td>
    </tr>
    <tr>
      <td><strong>Cook Time</strong></td>
      <td><input name="cooktime" type="text" class="input" id="cooktime" placeholder="Enter cook time">
        <select name="cookunits" class="units" id="cookunits">
          <option value="minutes">minutes</option>
          <option value="hours">hours</option>
          <option value="days">days</option>
        </select></td>
    </tr>
    <tr>
      <td><strong>Featured Recipe?</strong></td>
      <td><?php 
          
            if ($row_rsRecipeDetails['isFeatured'] == 1)
            {
                $checked = " checked";
            }
            else
            {
                $checked = "";
            }
          
        ?>
        <input name="isFeatured" type="checkbox" id="isFeatured" <?php echo $checked ?>></td>
    </tr>
    <tr>
      <td><strong>Cuisines</strong></td>
      <td><select name="cuisinetags[]" multiple="MULTIPLE" class="list" id="cuisinetags">
        
        <?php do { 
          
            if (in_array($row_rsSelectedCuisines['cuisineid'],$selectedcuisines))
            {
              $selection = " selected";
            }
            else
            {
                $selection = "";
            }
          
          ?>        
        
        <option value="<?php echo $row_rsSelectedCuisines['cuisineid']; ?>" <?php echo $selection ?>><?php echo $row_rsSelectedCuisines['cuisinename'];?></option>
        
        <?php } while ($row_rsSelectedCuisines = mysql_fetch_assoc($rsSelectedCuisines)); ?>
        
        </select></td>
    </tr>
    <tr>
      <td><strong>Dietary Concerns</strong></td>
      <td><select name="dietaryconcerntags[]" multiple="MULTIPLE" class="list" id="dietaryconcerntags">
          
          <?php do { 
          
            if (in_array($row_rsDietaryConcerns['concernid'],$selecteddietaryConcerns))
            {
              $selection = " selected";
            }
            else
            {
                $selection = "";
            }
          
          ?>          
          
        <option value="<?php echo $row_rsDietaryConcerns['concernid']; ?>" <?php echo $selection ?>><?php echo $row_rsDietaryConcerns['concernname'];?></option>
          
        <?php } while ($row_rsDietaryConcerns = mysql_fetch_assoc($rsDietaryConcerns)); ?>
          
      </select></td>
    </tr>
    <tr>
      <td><strong>Recipe Steps</strong></td>
      <td><textarea name="stepsTxt" class="description" id="stepsTxt"></textarea>
        <input type="button" name="stepsBtn" id="stepsBtn" value="Add"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td id="stepsCell">&nbsp;</td>
    </tr>
    <tr>
      <td><strong>Ingredients</strong></td>
      <td><input name="ingredientTxt" type="text" class="input" id="ingredientTxt" placeholder="Enter ingredient name">
        <input name="amountTxt" type="text" id="amountTxt" placeholder="Amount">
        <select name="units" class="units" id="units">
          <option value="oz" selected="selected">oz</option>
          <option value="cups">cups</option>
          <option value="bunch">bunch</option>
        </select>
<input type="button" name="ingredientBtn" id="ingredientBtn" value="Add"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td id="ingredientCell">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input type="button" name="submitBtn" id="submitBtn" value="Submit">
        <input name="userid" type="hidden" id="userid" value="<?php echo en($_SESSION['userid']); ?>"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </tbody>
</table>
<input type="hidden" name="MM_update" value="form1">
<input type="hidden" name="MM_insert" value="form1">
    
    </form>
</body>
</html>
<?php
mysql_free_result($rsRecipeDetails);

mysql_free_result($rsSelectedCuisines);
?>
  