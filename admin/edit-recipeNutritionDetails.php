 <?php
 include( "includes/devStatus.php" );

 require_once( '../../Connections/chewsrite.php' );
 include( "functions.php" );
 include( "auth.php" );

 $editFormAction = $_SERVER[ 'PHP_SELF' ];
 if ( isset( $_SERVER[ 'QUERY_STRING' ] ) ) {
     $editFormAction .= "?" . htmlentities( $_SERVER[ 'QUERY_STRING' ] );
 }

 $colname_rsNutritionDetails = "-1";
 if ( isset( $_GET[ 'recipeid' ] ) ) {
     $colname_rsNutritionDetails = de( $_GET[ 'recipeid' ] );

     $url = urldecode( $_GET[ 'recipeid' ] );

     //    echo "en id: {$_GET['recipeid']}<br>";
     //    echo "url id: {$url}<br>";
     //    echo "de: {$colname_rsNutritionDetails}<br>";
 }

 mysql_select_db( $database_chewsrite, $chewsrite );

 if ( ( isset( $_POST[ "MM_update" ] ) ) && ( $_POST[ "MM_update" ] == "form1" ) ) {

     //echo "post<br>";

     $query_rsGoals = "SELECT * FROM recipenutrition WHERE recipeid = {$colname_rsNutritionDetails}";

     //echo "{$query_rsGoals}<br>";

     $rsGoals = mysql_query( $query_rsGoals, $chewsrite )or die( mysql_error() );
     $totalRows_rsGoals = mysql_num_rows( $rsGoals );

     if ( $totalRows_rsGoals > 0 ) {
         //echo "update<br>";

         $updateSQL = sprintf( "UPDATE recipenutrition SET Calories=%s, Carbohydrates=%s, Protein=%s, Fat=%s, SaturatedFat=%s, UnsaturatedFat=%s, TransFat=%s, Cholestrol=%s, Sodium=%s, Potassium=%s, Fiber=%s, Sugars=%s, VitaminA=%s, VitaminC=%s, Calcium=%s, Iron=%s WHERE recipeid=%s",
             GetSQLValueString( $_POST[ 'Calories' ], "text" ),
             GetSQLValueString( $_POST[ 'Carbohydrates' ], "text" ),
             GetSQLValueString( $_POST[ 'Protein' ], "text" ),
             GetSQLValueString( $_POST[ 'Fat' ], "text" ),
             GetSQLValueString( $_POST[ 'Saturatedfat' ], "text" ),
             GetSQLValueString( $_POST[ 'UnsaturatedFat' ], "text" ),
             GetSQLValueString( $_POST[ 'TransFat' ], "text" ),
             GetSQLValueString( $_POST[ 'Cholestrol' ], "text" ),
             GetSQLValueString( $_POST[ 'Sodium' ], "text" ),
             GetSQLValueString( $_POST[ 'Potassium' ], "text" ),
             GetSQLValueString( $_POST[ 'Fiber' ], "text" ),
             GetSQLValueString( $_POST[ 'Sugars' ], "text" ),
             GetSQLValueString( $_POST[ 'VitaminA' ], "text" ),
             GetSQLValueString( $_POST[ 'VitaminC' ], "text" ),
             GetSQLValueString( $_POST[ 'Calcium' ], "text" ),
             GetSQLValueString( $_POST[ 'Iron' ], "text" ),
             GetSQLValueString( $colname_rsNutritionDetails, "int" ) );

         echo "{$updateSQL}<br>";

         mysql_select_db( $database_chewsrite, $chewsrite );
         $Result1 = mysql_query( $updateSQL, $chewsrite )or die( mysql_error() );

         $u = en( $colname_rsNutritionDetails );

         $updateGoTo = "edit-recipeNutritionDetails.php?recipeid={$u}";
         if ( isset( $_SERVER[ 'QUERY_STRING' ] ) ) {
             $updateGoTo .= ( strpos( $updateGoTo, '?' ) ) ? "&" : "?";
             $updateGoTo .= $_SERVER[ 'QUERY_STRING' ];
         }
         header( sprintf( "Location: %s", $updateGoTo ) );
     } else {
         //echo "insert<br>";

         $updateSQL = sprintf( "INSERT INTO recipenutrition  (Calories, Carbohydrates, Protein, Fat, SaturatedFat, UnsaturatedFat, TransFat, Cholestrol, Sodium, Potassium, Fiber, Sugars, VitaminA, VitaminC, Calcium, Iron, recipeid) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
             GetSQLValueString( $_POST[ 'Calories' ], "text" ),
             GetSQLValueString( $_POST[ 'Carbohydrates' ], "text" ),
             GetSQLValueString( $_POST[ 'Protein' ], "text" ),
             GetSQLValueString( $_POST[ 'Fat' ], "text" ),
             GetSQLValueString( $_POST[ 'Saturatedfat' ], "text" ),
             GetSQLValueString( $_POST[ 'UnsaturatedFat' ], "text" ),
             GetSQLValueString( $_POST[ 'TransFat' ], "text" ),
             GetSQLValueString( $_POST[ 'Cholestrol' ], "text" ),
             GetSQLValueString( $_POST[ 'Sodium' ], "text" ),
             GetSQLValueString( $_POST[ 'Potassium' ], "text" ),
             GetSQLValueString( $_POST[ 'Fiber' ], "text" ),
             GetSQLValueString( $_POST[ 'Sugars' ], "text" ),
             GetSQLValueString( $_POST[ 'VitaminA' ], "text" ),
             GetSQLValueString( $_POST[ 'VitaminC' ], "text" ),
             GetSQLValueString( $_POST[ 'Calcium' ], "text" ),
             GetSQLValueString( $_POST[ 'Iron' ], "text" ),
             GetSQLValueString( $colname_rsNutritionDetails, "int" ) );

         mysql_select_db( $database_chewsrite, $chewsrite );
         $Result1 = mysql_query( $updateSQL, $chewsrite )or die( mysql_error() );

         $updateGoTo = "viewRecipes.php";
         if ( isset( $_SERVER[ 'QUERY_STRING' ] ) ) {
             $updateGoTo .= ( strpos( $updateGoTo, '?' ) ) ? "&" : "?";
             $updateGoTo .= $_SERVER[ 'QUERY_STRING' ];
         }
         header( sprintf( "Location: %s", $updateGoTo ) );
     }
 }

 $query_rsRecipeDetails = sprintf( "SELECT recipename FROM recipes WHERE recipeid = %s", GetSQLValueString( $colname_rsNutritionDetails, "int" ) );
 $rsRecipeDetails = mysql_query( $query_rsRecipeDetails, $chewsrite )or die( mysql_error() );
 $row_rsRecipeDetails = mysql_fetch_assoc( $rsRecipeDetails );
 $totalRows_rsRecipeDetails = mysql_num_rows( $rsRecipeDetails );



 mysql_select_db( $database_chewsrite, $chewsrite );
 $query_rsNutritionDetails = sprintf( "SELECT * FROM recipenutrition WHERE recipeid = %s", GetSQLValueString( $colname_rsNutritionDetails, "int" ) );
 $rsNutritionDetails = mysql_query( $query_rsNutritionDetails, $chewsrite )or die( mysql_error() );
 $row_rsNutritionDetails = mysql_fetch_assoc( $rsNutritionDetails );
 $totalRows_rsNutritionDetails = mysql_num_rows( $rsNutritionDetails );
 ?>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Edit Nutrition Details</title>
    <link href="admin.css" rel="stylesheet" type="text/css">
</head>

<body>

    <?php include("includes/nav.php"); ?>

    <p>&nbsp;</p>

    <h1>Edit Nutrition Details</h1>

    <form action="<?php echo $editFormAction; ?>" id="form1" name="form1" method="POST">
        <table width="600" cellspacing="5" class="table">
            <tbody>
                <tr>
                    <td><strong>Recipe</strong>
                    </td>
                    <td>
                        <?php echo $row_rsRecipeDetails['recipename']; ?>
                    </td>
                </tr>
                <tr>
                    <td width="134"><strong>Calories</strong>
                    </td>
                    <td width="445"><input name="Calories" type="text" id="Calories" value="<?php echo $row_rsNutritionDetails['Calories']; ?>">
                    </td>
                </tr>
                <tr>
                    <td><strong>Carbohydrates</strong>
                    </td>
                    <td><input name="Carbohydrates" type="text" id="Carbohydrates" value="<?php echo $row_rsNutritionDetails['Carbohydrates']; ?>">
                    </td>
                </tr>
                <tr>
                    <td><strong>Protein</strong>
                    </td>
                    <td><input name="Protein" type="text" id="protein" value="<?php echo $row_rsNutritionDetails['Protein']; ?>">
                    </td>
                </tr>
                <tr>
                    <td><strong>Fat</strong>
                    </td>
                    <td><input name="Fat" type="text" id="Fat" value="<?php echo $row_rsNutritionDetails['Fat']; ?>">
                    </td>
                </tr>
                <tr>
                    <td><strong>SaturatedFat</strong>
                    </td>
                    <td><input name="Saturatedfat" type="text" id="Saturatedfat" value="<?php echo $row_rsNutritionDetails['SaturatedFat']; ?>">
                    </td>
                </tr>
                <tr>
                    <td><strong>UnsaturatedFat</strong>
                    </td>
                    <td><input name="UnsaturatedFat" type="text" id="UnsaturatedFat" value="<?php echo $row_rsNutritionDetails['UnsaturatedFat']; ?>">
                    </td>
                </tr>
                <tr>
                    <td><strong>TransFat</strong>
                    </td>
                    <td><input name="TransFat" type="text" id="TransFat" value="<?php echo $row_rsNutritionDetails['TransFat']; ?>">
                    </td>
                </tr>
                <tr>
                    <td><strong>Cholestrol</strong>
                    </td>
                    <td><input name="Cholestrol" type="text" id="Cholestrol" value="<?php echo $row_rsNutritionDetails['Cholestrol']; ?>">
                    </td>
                </tr>
                <tr>
                    <td><strong>Sodium</strong>
                    </td>
                    <td><input name="Sodium" type="text" id="Sodium" value="<?php echo $row_rsNutritionDetails['Sodium']; ?>">
                    </td>
                </tr>
                <tr>
                    <td><strong>Potassium</strong>
                    </td>
                    <td><input name="Potassium" type="text" id="Potassium" value="<?php echo $row_rsNutritionDetails['Potassium']; ?>">
                    </td>
                </tr>
                <tr>
                    <td><strong>Fiber</strong>
                    </td>
                    <td><input name="Fiber" type="text" id="Fiber" value="<?php echo $row_rsNutritionDetails['Fiber']; ?>">
                    </td>
                </tr>
                <tr>
                    <td><strong>Sugars</strong>
                    </td>
                    <td><input name="Sugars" type="text" id="Sugars" value="<?php echo $row_rsNutritionDetails['Sugars']; ?>">
                    </td>
                </tr>
                <tr>
                    <td><strong>VitaminA</strong>
                    </td>
                    <td><input name="VitaminA" type="text" id="VitaminA" value="<?php echo $row_rsNutritionDetails['VitaminA']; ?>">
                    </td>
                </tr>
                <tr>
                    <td><strong>VitaminC</strong>
                    </td>
                    <td><input name="VitaminC" type="text" id="VitaminC" value="<?php echo $row_rsNutritionDetails['VitaminC']; ?>">
                    </td>
                </tr>
                <tr>
                    <td><strong>Calcium</strong>
                    </td>
                    <td><input name="Calcium" type="text" id="Calcium" value="<?php echo $row_rsNutritionDetails['Calcium']; ?>">
                    </td>
                </tr>
                <tr>
                    <td><strong>Iron</strong>
                    </td>
                    <td><input name="Iron" type="text" id="Iron" value="<?php echo $row_rsNutritionDetails['Iron']; ?>">
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td><input type="submit" name="submit" id="submit" value="Submit">
                        <input name="recipeid" type="hidden" id="recipeid" value="<?php echo urlencode(en($colname_rsNutritionDetails)); ?>">
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><a href="recipeNutritionDetails.php?recipeid=<?php echo urlencode(en($colname_rsNutritionDetails)); ?>">Back</a>
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </tbody>
        </table>
        <input type="hidden" name="MM_update" value="form1">
    </form>
</body>
</html>
 <?php
 mysql_free_result( $rsNutritionDetails );
 ?>