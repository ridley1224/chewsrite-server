<?php 

include("includes/devStatus.php");

require_once('../../Connections/chewsrite.php'); 

include("functions.php");
include("auth.php");


//var_dump($_SESSION);

$colname_rsRecipeDetails = "-1";
if (isset($_GET['recipeid'])) {
  $colname_rsRecipeDetails = de($_GET['recipeid']);
}


mysql_select_db($database_chewsrite, $chewsrite);
$query_rsUserInfo = sprintf("SELECT * FROM users WHERE userid = %s", GetSQLValueString($_SESSION['userid'], "int"));
$rsUserInfo = mysql_query($query_rsUserInfo, $chewsrite) or die(mysql_error());
$row_rsUserInfo = mysql_fetch_assoc($rsUserInfo);
$totalRows_rsUserInfo = mysql_num_rows($rsUserInfo);


mysql_select_db($database_chewsrite, $chewsrite);
$query_rsRecipeDetails = sprintf("SELECT * FROM recipes WHERE recipeid = %s", GetSQLValueString($colname_rsRecipeDetails, "int"));


//echo $query_rsRecipeDetails;

$rsRecipeDetails = mysql_query($query_rsRecipeDetails, $chewsrite) or die(mysql_error());
$row_rsRecipeDetails = mysql_fetch_assoc($rsRecipeDetails);
$totalRows_rsRecipeDetails = mysql_num_rows($rsRecipeDetails);


$query_rsNutritionDetails = sprintf("SELECT Calories FROM recipenutrition WHERE recipeid = %s", GetSQLValueString($colname_rsRecipeDetails, "int"));


$rsNutritionDetails = mysql_query($query_rsNutritionDetails, $chewsrite) or die(mysql_error());
$row_rsNutritionDetails = mysql_fetch_assoc($rsNutritionDetails);
$totalRows_rsNutritionDetails = mysql_num_rows($rsNutritionDetails);



$selectedcuisines = explode(",",$row_rsRecipeDetails['cuisinetags']);
$selecteddietaryConcerns = explode(",",$row_rsRecipeDetails['dietaryconcerntags']);

//mysql_select_db($database_chewsrite, $chewsrite);
//$query_rsSelectedCuisines = "SELECT * FROM cuisines WHERE cuisineid IN ({$row_rsRecipeDetails['cuisinetags']}) ";
//$rsSelectedCuisines = mysql_query($query_rsSelectedCuisines, $chewsrite) or die(mysql_error());
//$row_rsSelectedCuisines = mysql_fetch_assoc($rsSelectedCuisines);
//$totalRows_rsSelectedCuisines = mysql_num_rows($rsSelectedCuisines);

mysql_select_db($database_chewsrite, $chewsrite);
$query_rsSelectedCuisines = "SELECT * FROM cuisines";
$rsSelectedCuisines = mysql_query($query_rsSelectedCuisines, $chewsrite) or die(mysql_error());
$row_rsSelectedCuisines = mysql_fetch_assoc($rsSelectedCuisines);
$totalRows_rsSelectedCuisines = mysql_num_rows($rsSelectedCuisines);

$query_rsDietaryConcerns = "SELECT * FROM dietaryconcerns";
$rsDietaryConcerns = mysql_query($query_rsDietaryConcerns, $chewsrite) or die(mysql_error());
$row_rsDietaryConcerns = mysql_fetch_assoc($rsDietaryConcerns);
$totalRows_rsDietaryConcerns = mysql_num_rows($rsDietaryConcerns);
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title><?php echo $row_rsRecipeDetails['recipename']; ?></title>
<link href="admin.css" rel="stylesheet" type="text/css">
</head>

<body>

<?php include("includes/nav.php"); ?>
    
<p>&nbsp;</p>

<h1>Recipe Details</h1>

<table width="600" cellspacing="5" class="table">
  <tbody>
    <tr>
      <td width="140"><strong><img src="https://s3-us-west-2.amazonaws.com/chewsrite/images/<?php echo $row_rsRecipeDetails['imagename']; ?>" alt="" width="80" height="80"/></strong></td>
      <td width="439">&nbsp;</td>
    </tr>
    <tr>
      <td><strong>Recipe</strong></td>
      <td><?php echo $row_rsRecipeDetails['recipename']; ?></td>
    </tr>
    <tr>
      <td><strong>Author</strong></td>
      <td><?php echo $row_rsUserInfo['fullname']; ?></td>
    </tr>
    <tr>
      <td><strong>Description</strong></td>
      <td><?php echo $row_rsRecipeDetails['description']; ?></td>
    </tr>
    <tr>
      <td><strong>Source</strong></td>
      <td><?php echo $row_rsRecipeDetails['source']; ?></td>
    </tr>
    <tr>
      <td><strong>Servings</strong></td>
      <td><?php echo $row_rsRecipeDetails['servings']; ?></td>
    </tr>
    <tr>
      <td><strong>Prep Time</strong></td>
      <td><?php echo $row_rsRecipeDetails['preptime']; ?></td>
    </tr>
    <tr>
      <td><strong>Cook Time</strong></td>
      <td><?php echo $row_rsRecipeDetails['cooktime']; ?></td>
    </tr>
    <tr>
      <td><strong>Calories</strong></td>
      <td><?php echo $row_rsNutritionDetails['Calories']; ?></td>
    </tr>
    <tr>
      <td><strong>Nutrition Info</strong></td>
      <td><a href="recipeNutritionDetails.php?recipeid=<?php echo urlencode(en($colname_rsRecipeDetails)); ?>">View nutrition info</a></td>
    </tr>
    <tr>
      <td><strong>Recipe  Directions</strong></td>
      <td><a href="recipeDirectionDetails.php?recipeid=<?php echo urlencode(en($colname_rsRecipeDetails)); ?>">View recipe directions</a>      
    </tr>
    <tr>
      <td><strong>Ingredients</strong></td>
      <td><?php
          
          if(strlen($row_rsRecipeDetails['ingredientsbulk']) > 0)
          {
              $a = str_getcsv($row_rsRecipeDetails['ingredientsbulk'], ",", '"');
          
              foreach ( $a as $ig ) {

                  echo $ig . "<br>";
              }
          }
          else
          {
              $query_rsIngredientInfo = sprintf("SELECT * FROM recipeingredients WHERE recipeid = %s AND active = 1 ORDER BY recipeorder", GetSQLValueString($colname_rsRecipeDetails, "int"));
              
              //echo $query_rsIngredientInfo . "<br>";
              
                 $rsIngredientInfo = mysql_query( $query_rsIngredientInfo, $chewsrite )or die( mysql_error() );
                 $row_rsIngredientInfo = mysql_fetch_assoc( $rsIngredientInfo );
                 $totalRows_rsIngredientInfo = mysql_num_rows( $rsIngredientInfo );

                 do {

                     echo $row_rsIngredientInfo[ 'quantity' ] . " " . $row_rsIngredientInfo[ 'unit' ]. " " . $row_rsIngredientInfo[ 'ingredientname' ] . "<br>";

                 } while ( $row_rsIngredientInfo = mysql_fetch_assoc( $rsIngredientInfo ) );
          }
          
          
          
          ?>      
    </tr>
    <tr>
      <td><strong>Featured Recipe?</strong></td>
      <td><?php 
          
            if ($row_rsRecipeDetails['isFeatured'] == 1)
            {
                echo "Yes";
            }
            else
            {
                echo "No";
            }
          
        ?>
    </tr>
    <tr>
      <td><strong>Has Video?</strong></td>
      <td><?php echo $row_rsRecipeDetails['videoname']; ?></td>
    </tr>
    <tr>
      <td><strong>Cuisines</strong></td>
      <td>
          
          <?php 
                    
          do { 
          
            if (in_array($row_rsSelectedCuisines['cuisineid'],$selectedcuisines))
            {
                $cuisinestrings[] = $row_rsSelectedCuisines['cuisinename'];
            }
                        
         } while ($row_rsSelectedCuisines = mysql_fetch_assoc($rsSelectedCuisines)); 
          
          
          echo implode(", ",$cuisinestrings);
          
          ?>
          
    </td>
    </tr>
    <tr>
      <td><strong>Dietary Concerns</strong></td>
      <td>
          
      <?php do { 
          
            if (in_array($row_rsDietaryConcerns['concernid'],$selecteddietaryConcerns))
            {
               $concernstrings[] = $row_rsDietaryConcerns['concernname'];
            }
          
    } while ($row_rsDietaryConcerns = mysql_fetch_assoc($rsDietaryConcerns));
          
          echo implode(", ",$concernstrings);
          
          ?></td>
    </tr>
    <tr>
      <td><strong>Rating</strong></td>
      <td><?php 
          
          
          if ($row_rsRecipeDetails['rating'] == "")
            {
                echo "No ratings";
            }
            else
            {
                 echo $row_rsRecipeDetails['rating'];
            }
          
          ?></td>
    </tr>
    <tr>
      <td><strong>Date Added</strong></td>
      <td><?php echo date("m-d-Y",strtotime($row_rsRecipeDetails['dateadded'])); ?></td>
    </tr>
    <tr>
      <td><a href="edit-recipeDetails.php?recipeid=<?php echo urlencode(en($colname_rsRecipeDetails)); ?>">Edit</a></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><strong><a href="viewRecipes.php">Back</a></strong></td>
      <td>&nbsp;</td>
    </tr>
  </tbody>
</table>
</body>
</html>
<?php
mysql_free_result($rsRecipeDetails);

mysql_free_result($rsSelectedCuisines);
?>
  