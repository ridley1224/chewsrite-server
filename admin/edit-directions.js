// JavaScript Document

var currentRows = 0;
var did;
var recipeid;

$(document).ready(function () {
    
    did = 1;
    recipeid = 2;    

    currentRows = parseInt($('#rows').val());
    
    $('#addRowBtn').on('click', function () {
        
        addTableRow()
    });
})

function addTableRow() {
    
    var dis = currentRows+1;
    var str2 = "";
    
    for (i = 0; i < currentRows+1; i++) {
        
        var ind = i+1;
        
        var s = "";
        
        if (i == currentRows)
        {
            s = "selected";
        }
        
       str2 += "<option value="+ind+" "+s+">"+ind+"</option>";
    }
    
    $('#stepsTable tr:nth-last-child(4)').after("<tr><td width='72'><strong>Step </strong></td><td width='435'><input type='text' class='input3' id='title"+currentRows+"' value='' placeholder='title'><input type='text' class='description3' id='direction"+currentRows+"' value='' placeholder='directions'></td><td width='65' align='center'><a href='edit-recipeDirectionDetails.php?d=1&did="+did+"&recipeid="+recipeid+"'></a><select name='select"+currentRows+"' id='select"+currentRows+"'>"+str2+"</select></td><td width='65' align='center'><a href='edit-recipeDirectionDetails.php?recipeid="+recipeid+"'>delete</a></td></tr>");
    
    currentRows++;
}

