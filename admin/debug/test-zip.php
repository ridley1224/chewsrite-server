<?php
$zip = new ZipArchive;

$dirname = "temp1";

if ( file_exists( $dirname ) ) {
    echo "directory exists<br>";
    $dirStatus = true;
} else {
    if ( mkdir( $dirname ) ) {
        echo "created directory<br>";
        $dirStatus = true;
    }
}

if ( $dirStatus == true ) {
    if ( $zip->open( 'temp/test.zip' ) === TRUE ) {

        $zip->extractTo( "{$dirname}/" );
        $zip->close();
        //echo 'ok';

        echo "extracting files<br>";

        $extensions = array( "jpeg", "jpg", "png", "gif" );

        if ( $handle = opendir( $dirname ) ) {

            while ( false !== ( $entry = readdir( $handle ) ) ) {
                $extension = pathinfo( $entry, PATHINFO_EXTENSION );

                if ( in_array( $extension, $extensions ) ) {

                    $entries[] = $entry;

                    echo "{$entry}<br>";

                    //include("s3-upload-file-bulk.php");
                }
            }

            closedir( $handle );
        }

    } else {
        echo 'failed';
    }
}

//rmdir($handle);

?>