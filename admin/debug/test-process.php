<?php
include("includes/devStatus.php"); 

require_once('../../Connections/chewsrite.php'); 

include( "functions.php" );


//$_SESSION['userid'] = "24";

//var_dump($_SESSION);

date_default_timezone_set( 'America/Detroit' );
$date = date( "Y-m-d H:i:s" );


//  $target_file = basename($_FILES["fileToUpload"]["name"]);
//  $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));


$imageFileType = "csv";

if ( $imageFileType == "csv" ) {
    //    header('Content-Type: text/csv');
    //    header('Content-Disposition: attachment; filename="recipes.csv"');

    $csvFile = 'bulk-recipes-template.csv';

    $utteranceList = readCSV( $csvFile );

    $user_CSV[ 0 ] = array( '', 'recipename', 'description', 'sourceurl', 'servings', 'preptime', 'prepunits', 'cooktime', 'cookunits', 'isFeatured', 'cuisinetags', 'dietaryconcerntags', 'stepsTxt', 'ingredientTxt', 'amountTxt', 'units' );

    // very simple to increment with i++ if looping through a database result

    $i = 0;
    $limit = 100;

    $debug = false;

    foreach ( $utteranceList as $rowString ) {
        if ( $i == 0 ) {
            $obj1 = new stdClass;
            $intent = $rowString;
            $obj1->recipename = $intent;

            if ( $debug == true ) {
                echo "<strong>recipename:</strong> {$rowString}<br>";
            }

            $i++;
        } else if ( $i == 1 ) {
            $utterance = $rowString;
            $obj1->description = $utterance;

            if ( $debug == true ) {
                echo "<strong>description:</strong> {$rowString}<br>";
            }

            $i++;
        } else if ( $i == 2 ) {
            $utterance = $rowString;
            $obj1->sourceurl = $utterance;

            if ( $debug == true ) {
                echo "<strong>sourceurl:</strong> {$rowString}<br>";
            }

            $i++;
        } else if ( $i == 3 ) {
            $utterance = $rowString;
            $obj1->servings = $utterance;

            if ( $debug == true ) {
                echo "<strong>servings:</strong> {$rowString}<br>";
            }

            $i++;
        } else if ( $i == 4 ) {
            $utterance = $rowString;
            $obj1->preptime = $utterance;

            if ( $debug == true ) {
                echo "<strong>preptime:</strong> {$rowString}<br>";
            }

            $i++;
        } else if ( $i == 5 ) {
            $utterance = $rowString;
            $obj1->prepunits = $utterance;

            if ( $debug == true ) {
                echo "<strong>prepunits:</strong> {$rowString}<br>";
            }

            $i++;
        } else if ( $i == 6 ) {
            $utterance = $rowString;
            $obj1->cooktime = $utterance;

            if ( $debug == true ) {
                echo "<strong>cooktime:</strong> {$rowString}<br>";
            }

            $i++;
        } else if ( $i == 7 ) {
            $utterance = $rowString;
            $obj1->cookunits = $utterance;

            if ( $debug == true ) {
                echo "<strong>cookunits:</strong> {$rowString}<br>";
            }

            $i++;
        } else if ( $i == 8 ) {
            $utterance = $rowString;
            $obj1->isFeatured = $utterance;
            if ( $debug == true ) {
                echo "<strong>isFeatured:</strong> {$rowString}<br>";
            }

            $i++;
        } else if ( $i == 9 ) {
            $utterance = $rowString;
            $obj1->cuisinetags = $utterance;

            if ( $debug == true ) {
                echo "<strong>cuisinetags:</strong> {$rowString}<br>";
            }

            $i++;
        } else if ( $i == 10 ) {
            $utterance = $rowString;
            $obj1->dietaryconcerntags = $utterance;

            if ( $debug == true ) {
                echo "<strong>dietaryconcerntags:</strong> {$rowString}<br>";
            }

            $i++;
        } else if ( $i == 11 ) {
            $utterance = $rowString;
            $obj1->stepsTxt = $utterance;

            if ( $debug == true ) {
                echo "<strong>stepsTxt:</strong> {$rowString}<br>";
            }

            $i++;
        } else if ( $i == 12 ) {
            $utterance = $rowString;
            $obj1->ingredientTxt = $utterance;

            if ( $debug == true ) {
                echo "<strong>ingredientTxt:</strong> {$rowString}<br>";
            }

            $i++;
        } else if ( $i == 13 ) {
            $utterance = $rowString;
            $obj1->amountTxt = $utterance;

            if ( $debug == true ) {
                echo "<strong>amountTxt:</strong> {$rowString}<br>";
            }

            $i++;
        }else if ( $i == 14 ) {
            $utterance = $rowString;
            $obj1->units = $utterance;

            if ( $debug == true ) {
                echo "<strong>units:</strong> {$rowString}<br>";
            }

            $i++;
        } else {
            
            $utterance = $rowString;
            $obj1->image = $utterance;
            
            
            if ( $debug == true ) {
                
                echo "<strong>image:</strong> {$rowString}<br>";
            }            

            $csvList[] = $obj1;

            $i = 0;
        }

    } //end loop

    array_shift( $csvList );

    print "<pre>";
    print_r( $csvList );
    print "</pre>";
    
    foreach ($csvList as $record) {

//  $insertSQL = sprintf("INSERT INTO recipes (recipename, userid, `description`, source, sourceurl, servings, preptime, cooktime, isFeatured, imagename, cuisinetags, dietaryconcerntags, dateadded) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
//                       GetSQLValueString($_POST['recipename'], "text"),
//                       GetSQLValueString($_POST['userid'], "int"),
//                       GetSQLValueString($_POST['description'], "text"),
//                       GetSQLValueString($_POST['source'], "text"),
//                       GetSQLValueString($_POST['sourceurl'], "text"),
//                       GetSQLValueString($_POST['servings'], "text"),
//                       GetSQLValueString($_POST['preptime'], "text"),
//                       GetSQLValueString($_POST['cooktime'], "text"),
//                       GetSQLValueString($_POST['isFeatured'], "int"),
//                       GetSQLValueString($_POST['imagename'], "text"),
//                       GetSQLValueString($_POST['cuisinetags'], "text"),
//                       GetSQLValueString($_POST['dietaryconcerntags'], "text"),
//                       GetSQLValueString($date, "date"));
//
//  mysql_select_db($database_chewsrite, $chewsrite);
//  $Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());
}
    


    //
    //    $fp = fopen('php://output', 'w');
    //
    //    foreach ($user_CSV as $line) {
    //
    //        // though CSV stands for "comma separated value"
    //        // in many countries (including France) separator is ";"
    //
    //        fputcsv($fp, $line, ',');
    //    }
    //
    //    fclose($fp);

    $uploadOk = 1;
} else {

    echo "Invalid file type. Please choose CSV file";

    $uploadOk = 0;
}


?>    