<?php
include("includes/devStatus.php"); 

require_once('../../Connections/chewsrite.php'); 

include( "functions.php" );


//$_SESSION['userid'] = "24";

//var_dump($_SESSION);


if(isset($_POST['userid']))
{
//    print "<pre>";
//        print_r( $_POST );
//        print "</pre>";
    

    $i = 0;
    
    foreach($_POST as $key => $value)
    {
        //echo "key: {$key} val: {$value}<br>";
        
        if($i == 0)
        {
            //create object
            
            $obj3 = new stdClass;
            
            $key = substr($key, 0, -1);
            
            $obj3->$key = $value;            
        }
        
        if($i == 14)
        {
            //insert record
            
            $insertObjects[] = $obj3;
            
            $obj3 = null;
            
            $i = 0;
            
            //echo "reset<br><br>";
            //echo "<br>";
        }
        
        if($i != 14)
        {
            //continue loop
            
            $key = substr($key, 0, -1);
            
            $obj3->$key =  $value;
            $i++;
        }        
    }
    
    print "<pre>";
    print_r( $insertObjects );
    print "</pre>";
    
    $date = date("Y-m-d H:i:s");
    
    foreach($insertObjects as $obj)
    {
      $insertSQL = sprintf("INSERT INTO recipes (recipename, userid, `description`, source, sourceurl, servings, preptime, cooktime, isFeatured, imagename, cuisinetags, dietaryconcerntags, dateadded) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($obj->recipename, "text"),
                       GetSQLValueString($_SESSION['userid'], "int"),
                       GetSQLValueString($obj->description, "text"),
                       GetSQLValueString($obj->source, "text"),
                       GetSQLValueString($obj->sourceurl, "text"),
                       GetSQLValueString($obj->servings, "text"),
                       GetSQLValueString($obj->preptime, "text"),
                       GetSQLValueString($obj->cooktime, "text"),
                       GetSQLValueString($obj->isFeatured, "int"),
                       GetSQLValueString($obj->image, "text"),
                       GetSQLValueString(implode(",",$obj->cuisinetags), "text"),
                       GetSQLValueString(implode(",",$obj->dietaryconcerntags), "text"),
                       GetSQLValueString($date, "date"));

        
        echo "sql: {$insertSQL}<br>";
        
      mysql_select_db($database_chewsrite, $chewsrite);
      //$Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());

    }
}
else
{
    mysql_select_db($database_chewsrite, $chewsrite);
    $query_rsSelectedCuisines = "SELECT * FROM cuisines WHERE active = 1";
    $rsSelectedCuisines = mysql_query($query_rsSelectedCuisines, $chewsrite) or die(mysql_error());
    $row_rsSelectedCuisines = mysql_fetch_assoc($rsSelectedCuisines);
    $totalRows_rsSelectedCuisines = mysql_num_rows($rsSelectedCuisines);

    do { 

                $obj1 = new stdClass;
                $obj1->cuisineid =  $row_rsSelectedCuisines['cuisineid'];
                $obj1->cuisinename =  $row_rsSelectedCuisines['cuisinename'];
                $cuisines[] = $obj1;

    } while ($row_rsSelectedCuisines = mysql_fetch_assoc($rsSelectedCuisines));




    //echo $query_rsSelectedCuisines . "<br>";

    $query_rsDietaryConcerns = "SELECT * FROM dietaryconcerns WHERE active = 1";
    $rsDietaryConcerns = mysql_query($query_rsDietaryConcerns, $chewsrite) or die(mysql_error());
    $row_rsDietaryConcerns = mysql_fetch_assoc($rsDietaryConcerns);
    $totalRows_rsDietaryConcerns = mysql_num_rows($rsDietaryConcerns);

    do { 

                $obj2 = new stdClass;
                $obj2->concernid =  $row_rsDietaryConcerns['concernid'];
                $obj2->concernname =  $row_rsDietaryConcerns['concernname'];
                $concerns[] = $obj2;

    } while ($row_rsDietaryConcerns = mysql_fetch_assoc($rsDietaryConcerns));

    //var_dump($concerns);


    date_default_timezone_set( 'America/Detroit' );
    $date = date( "Y-m-d H:i:s" );


    //  $target_file = basename($_FILES["fileToUpload"]["name"]);
    //  $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));


    $imageFileType = "csv";

    if ( $imageFileType == "csv" ) {
        //    header('Content-Type: text/csv');
        //    header('Content-Disposition: attachment; filename="recipes.csv"');

        $csvFile = 'bulk-recipes-template.csv';

        $utteranceList = readCSV( $csvFile );

        $user_CSV[ 0 ] = array( '', 'recipename', 'description', 'sourceurl', 'servings', 'preptime', 'prepunits', 'cooktime', 'cookunits', 'isFeatured', 'cuisinetags', 'dietaryconcerntags', 'stepsTxt', 'ingredientTxt', 'amountTxt', 'units' );

        // very simple to increment with i++ if looping through a database result

        $i = 0;
        $limit = 100;

        $debug = false;
        
//        foreach ( $utteranceList as $rowString ) {
//            
//            $obj1 = new stdClass;
//            $intent = $rowString;
//            
//        }

        foreach ( $utteranceList as $rowString ) {
            if ( $i == 0 ) {
                $obj1 = new stdClass;
                $intent = $rowString;
                $obj1->recipename = $intent;

                if ( $debug == true ) {
                    echo "<strong>recipename:</strong> {$rowString}<br>";
                }

                $i++;
            } else if ( $i == 1 ) {
                $utterance = $rowString;
                $obj1->description = $utterance;

                if ( $debug == true ) {
                    echo "<strong>description:</strong> {$rowString}<br>";
                }

                $i++;
            } else if ( $i == 2 ) {
                $utterance = $rowString;
                $obj1->sourceurl = $utterance;

                if ( $debug == true ) {
                    echo "<strong>sourceurl:</strong> {$rowString}<br>";
                }

                $i++;
            } else if ( $i == 3 ) {
                $utterance = $rowString;
                $obj1->servings = $utterance;

                if ( $debug == true ) {
                    echo "<strong>servings:</strong> {$rowString}<br>";
                }

                $i++;
            } else if ( $i == 4 ) {
                $utterance = $rowString;
                $obj1->preptime = $utterance;

                if ( $debug == true ) {
                    echo "<strong>preptime:</strong> {$rowString}<br>";
                }

                $i++;
            } else if ( $i == 5 ) {
                $utterance = $rowString;
                $obj1->prepunits = $utterance;

                if ( $debug == true ) {
                    echo "<strong>prepunits:</strong> {$rowString}<br>";
                }

                $i++;
            } else if ( $i == 6 ) {
                $utterance = $rowString;
                $obj1->cooktime = $utterance;

                if ( $debug == true ) {
                    echo "<strong>cooktime:</strong> {$rowString}<br>";
                }

                $i++;
            } else if ( $i == 7 ) {
                $utterance = $rowString;
                $obj1->cookunits = $utterance;

                if ( $debug == true ) {
                    echo "<strong>cookunits:</strong> {$rowString}<br>";
                }

                $i++;
            } else if ( $i == 8 ) {
                $utterance = $rowString;
                $obj1->isFeatured = $utterance;
                if ( $debug == true ) {
                    echo "<strong>isFeatured:</strong> {$rowString}<br>";
                }

                $i++;
            } else if ( $i == 9 ) {
                $utterance = $rowString;
                $obj1->cuisinetags = $utterance;

                if ( $debug == true ) {
                    echo "<strong>cuisinetags:</strong> {$rowString}<br>";
                }

                $i++;
            } else if ( $i == 10 ) {
                $utterance = $rowString;
                $obj1->dietaryconcerntags = $utterance;

                if ( $debug == true ) {
                    echo "<strong>dietaryconcerntags:</strong> {$rowString}<br>";
                }

                $i++;
            } else if ( $i == 11 ) {
                $utterance = $rowString;
                $obj1->stepsTxt = $utterance;

                if ( $debug == true ) {
                    echo "<strong>stepsTxt:</strong> {$rowString}<br>";
                }

                $i++;
            } else if ( $i == 12 ) {
                $utterance = $rowString;
                $obj1->ingredientTxt = $utterance;

                if ( $debug == true ) {
                    echo "<strong>ingredientTxt:</strong> {$rowString}<br>";
                }

                $i++;
            } else if ( $i == 13 ) {
                $utterance = $rowString;
                $obj1->amountTxt = $utterance;

                if ( $debug == true ) {
                    echo "<strong>amountTxt:</strong> {$rowString}<br>";
                }

                $i++;
            }else if ( $i == 14 ) {
                $utterance = $rowString;
                $obj1->units = $utterance;

                if ( $debug == true ) {
                    echo "<strong>units:</strong> {$rowString}<br>";
                }

                $i++;
            } else {

                $utterance = $rowString;
                $obj1->image = $utterance;


                if ( $debug == true ) {

                    echo "<strong>image:</strong> {$rowString}<br>";
                }            

                $csvList[] = $obj1;

                $i = 0;
            }

        } //end loop

        array_shift( $csvList );

        //print "<pre>";
        //print_r( $csvList );
        //print "</pre>";

    ?>

<form action="test-process2.php" method="post" name="form1" id="form1">
    <table>
    <tr><td>Recipe</td><td>Cuisine</td><td>Cuisine ID</td><td>Concerns</td><td>Concerns ID</td><td>Description</td><td>Source URL</td><td>Servings</td><td>Prep Time</td><td>Prep Units</td><td>Cook Time</td><td>Cook Units</td><td>Featured</td><td>Steps</td><td>Ingredients</td><td>Amounts</td><td>Units</td><td>Image</td></tr>
    <?php

    $i1 = 0;

    foreach ($csvList as $record) { ?>


    <tr>
    <td><input type="text" name="<?php echo "recipename" . $i1;?>" value="<?php echo $record->recipename;?>"></td>

    <td>    
        <select name="<?php echo "cuisinetags". $i1 . "[]"; ?>" multiple="MULTIPLE" class="list" id="<?php echo "cuisinetags". $i1; ?>">

              <?php  

                $selectedcuisines = explode(",",$record->cuisinetags);   

                foreach ($cuisines as $param) {

                if (in_array($param->cuisinename,$selectedcuisines))
                {
                    $selection = " selected";
                }
                else
                {
                    $selection = "";
                }

              ?>        

            <option value="<?php echo $param->cuisineid; ?>" <?php echo $selection ?>><?php echo $param->cuisinename;?></option>

            <?php } ?>

          </select>    
        </td>
        <td><?php echo $record->cuisinetags;?></td>
    <td><select name="<?php echo "dietaryconcerntags". $i1 . "[]"; ?>" multiple="MULTIPLE" class="list" id="<?php echo "dietaryconcerntags". $i1; ?>">

              <?php 

                $selectedconcenrs = explode(",",$record->dietaryconcerntags);

                foreach ($concerns as $param) {

                if (in_array($param->concernname,$selectedconcenrs))
                {
                    $selection = " selected";
                }
                else
                {
                    $selection = "";
                } ?> 

        <option value="<?php echo $param->concernid; ?>" <?php echo $selection ?>><?php echo $param->concernname;?></option> 

        <?php }?>

          </select></td>
        <td><?php echo $record->dietaryconcerntags;?></td>
    <td><textarea name="<?php echo "description" . $i1;?>"><?php echo $record->description;?></textarea></td>
    <td><input type="text" name="<?php echo "sourceurl" . $i1;?>" value="<?php echo $record->sourceurl;?>"></td>
    <td><input type="text" name="<?php echo "servings" . $i1;?>" value="<?php echo $record->servings;?>"></td>
    <td><input type="text" name="<?php echo "preptime" . $i1;?>" value="<?php echo $record->preptime;?>"></td>
    <td>
        <?php /*?><input type="text" name="<?php echo "prepunits" . $i1;?>" value="<?php echo $record->prepunits;?>"><?php */?>
        <select>
            <option value="0">Select...</option>
            <option value="1"<?php if($record->prepunits=="minute"){ echo " selected";}?>>Minute</option>
            <option value="2"<?php if($record->prepunits=="hour"){ echo " selected";}?>>Hour</option>
        </select>

        </td>
    <td><input type="text" name="<?php echo "cooktime" . $i1;?>" value="<?php echo $record->cooktime;?>"></td>
    <td><?php /*?><input type="text" name="<?php echo "cookunits" . $i1;?>" value="<?php echo $record->cookunits;?>"><?php */?>
        <select>
            <option value="0">Select...</option>
            <option value="1"<?php if($record->cookunits=="minute"){ echo " selected";}?>>Minute</option>
            <option value="2"<?php if($record->cookunits=="hour"){ echo " selected";}?>>Hour</option>
        </select>


        </td>
    <td><input type="text" name="<?php echo "isFeatured" . $i1;?>" value="<?php echo $record->isFeatured;?>"></td>
    <td><input type="text" name="<?php echo "stepsTxt" . $i1;?>" value="<?php echo $record->stepsTxt;?>"></td>
    <td><input type="text" name="<?php echo "ingredientTxt" . $i1;?>" value="<?php echo $record->ingredientTxt;?>"></td>
    <td><input type="text" name="<?php echo "amountTxt" . $i1;?>" value="<?php echo $record->amountTxt;?>"></td>
    <td><input type="text" name="<?php echo "units" . $i1;?>" value="<?php echo $record->units;?>"></td>
    <td><input type="text" name="<?php echo "image" . $i1;?>" value="<?php echo $record->image;?>"></td>


    </tr>

    <?php 

    $i1++;

    }

    

      ?>

        
    </table>
    <input type="hidden" value="1" name="userid" id="userid">
    <input type="submit" name="submit1" id="submit1">
</form>


    <?php

        $uploadOk = 1;
    } else {

        echo "Invalid file type. Please choose CSV file";

        $uploadOk = 0;
    }

}




?>                                