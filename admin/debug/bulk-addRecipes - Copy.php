 <?php //include("auth.php");

 include( "includes/devStatus.php" );

 require_once( '../../Connections/chewsrite.php' );
 include( "functions.php" );

 //var_dump($_SESSION);

 $debug = false;

 date_default_timezone_set( 'America/Detroit' );
 $date = date( "Y-m-d H:i:s" );

 mysql_select_db( $database_chewsrite, $chewsrite );
 $query_rsSelectedCuisines = "SELECT * FROM cuisines WHERE active = 1";
 $rsSelectedCuisines = mysql_query( $query_rsSelectedCuisines, $chewsrite )or die( mysql_error() );
 $row_rsSelectedCuisines = mysql_fetch_assoc( $rsSelectedCuisines );
 $totalRows_rsSelectedCuisines = mysql_num_rows( $rsSelectedCuisines );

 do {

     $obj1 = new stdClass;
     $obj1->cuisineid = $row_rsSelectedCuisines[ 'cuisineid' ];
     $obj1->cuisinename = $row_rsSelectedCuisines[ 'cuisinename' ];
     $cuisines[] = $obj1;

 } while ( $row_rsSelectedCuisines = mysql_fetch_assoc( $rsSelectedCuisines ) );


 //echo $query_rsSelectedCuisines . "<br>";

 $query_rsDietaryConcerns = "SELECT * FROM dietaryconcerns WHERE active = 1";
 $rsDietaryConcerns = mysql_query( $query_rsDietaryConcerns, $chewsrite )or die( mysql_error() );
 $row_rsDietaryConcerns = mysql_fetch_assoc( $rsDietaryConcerns );
 $totalRows_rsDietaryConcerns = mysql_num_rows( $rsDietaryConcerns );

 do {

     $obj2 = new stdClass;
     $obj2->concernid = $row_rsDietaryConcerns[ 'concernid' ];
     $obj2->concernname = $row_rsDietaryConcerns[ 'concernname' ];
     $concerns[] = $obj2;

 } while ( $row_rsDietaryConcerns = mysql_fetch_assoc( $rsDietaryConcerns ) );

 //var_dump($concerns);

 if ( isset( $_POST[ "submit" ] ) ) {

     if ( count( $_FILES[ "files" ][ "name" ] ) > 2 ) {
         $uploadStatus = "You can only upload a maximum of 2 files";
         $uploadOk = 0;
     } else {
         foreach ( $_FILES[ "files" ][ "tmp_name" ] as $key => $tmp_name ) {

             $target_file = $_FILES[ "files" ][ "name" ][ $key ];
             $file = $_FILES[ "files" ][ "tmp_name" ][ $key ];
             //$extension = pathinfo( $filename, PATHINFO_EXTENSION );

             //$target_file = basename( $_FILES[ "files" ][ "name" ] );
             $extension = strtolower( pathinfo( $target_file, PATHINFO_EXTENSION ) );

             if ( $debug ) {
                 echo "file: {$target_file}<br>";
                 echo "ext: {$extension}<br>";
             }


             //parse recipe instructions file

             if ( $extension == "csv" ) {

                 //                 echo "not steps file<br>";
                 //                 
                 //                 return;

                 //$csvFile = $_FILES[ "files" ][ "tmp_name" ]; //'utterances.csv';
                 $csvFile = $file;

                 $utteranceList = readCSV( $csvFile );

                 //    header('Content-Type: text/csv');
                 //    header('Content-Disposition: attachment; filename="recipes.csv"');

                 $user_CSV = array( 'recipename', 'description', 'source', 'sourceurl', 'servings', 'preptime', 'cooktime', 'isFeatured', 'cuisinetags', 'dietaryconcerntags', 'ingredients', 'image' );

                 // very simple to increment with i++ if looping through a database result

                 $i = 0;
                 $limit = 100;

                 foreach ( $utteranceList as $rowString ) {

                     //echo "rs: {$rowString}<br>";

                     if ( $i == 0 ) {
                         $obj1 = new stdClass;
                     }

                     $intent = $rowString;

                     $obj1->$user_CSV[ $i ] = $intent;

                     if ( $debug == true ) {
                         echo "<strong>{$user_CSV[$i]}:</strong> {$rowString}<br>";
                     }

                     $i++;

                     if ( $i == 12 ) {
                         $csvList[] = $obj1;

                         $i = 0;
                         $obj1 = null;
                     }
                 }

                 array_shift( $csvList );

                 if ( $debug ) {
                     print "<pre>";
                     print_r( $csvList );
                     print "</pre>";
                 }

                 //return;

                 $date = date( "Y-m-d H:i:s" );

                 foreach ( $csvList as $obj ) {
                     //parse cuisine ids

                     $cusines1 = explode( ",", $obj->cuisinetags );

                     //        print "<pre>";
                     //        print_r( $cusines1 );
                     //        print "</pre>";

                     foreach ( $cusines1 as $ob ) {
                         foreach ( $cuisines as $struct ) {

                             if ( $ob == $struct->cuisinename ) {
                                 //$item = $struct;

                                 $cuisineids[] = $struct->cuisineid;

                                 break;
                             }
                         }
                     }

                     $cusines2 = implode( ",", $cuisineids );

                     $cuisineids = [];

                     //parse concern ids

                     $concerns1 = explode( ",", $obj->dietaryconcerntags );

                     //        print "<pre>";
                     //        print_r( $concerns1 );
                     //        print "</pre>";

                     foreach ( $concerns1 as $ob ) {
                         foreach ( $concerns as $struct ) {

                             if ( $ob == $struct->concernname ) {
                                 //$item = $struct;

                                 $concernids[] = $struct->concernid;

                                 break;
                             }
                         }
                     }

                     $concerns2 = implode( ",", $concernids );

                     $concernids = [];

                     //format prep and cook time

                     if ( $obj->preptime > 1 ) {
                         $prep = $obj->preptime . " " . $obj->prepunits . "s";
                     } else {
                         $prep = $obj->preptime . " " . $obj->prepunits;
                     }

                     if ( $obj->cooktime > 1 ) {
                         $cook = $obj->cooktime . " " . $obj->cookunits . "s";
                     } else {
                         $cook = $obj->cooktime . " " . $obj->cookunits;
                     }

                     $cook = str_replace( " s", "", $cook );
                     $prep = str_replace( " s", "", $prep );

                     $name = strtolower( $obj->recipename );
                     $userid = de( $_SESSION[ 'userid' ] );

                     mysql_select_db( $database_chewsrite, $chewsrite );
                     $query_rsRecipes = "SELECT recipename FROM recipes WHERE userid = {$userid} AND LOWER(recipename) = '{$name}'";

                     //echo $query_rsRecipes;

                     $rsRecipes = mysql_query( $query_rsRecipes, $chewsrite )or die( mysql_error() );
                     $totalRows_rsRecipes = mysql_num_rows( $rsRecipes );


                     //if ( 0 == 0 )
                     if ( $totalRows_rsRecipes == 0 ) {
                         $insertSQL = sprintf( "INSERT INTO recipes (recipename, userid, `description`, source, sourceurl, servings, preptime, cooktime, isFeatured, imagename, cuisinetags, dietaryconcerntags, dateadded) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                             GetSQLValueString( $obj->recipename, "text" ),
                             GetSQLValueString( de( $_SESSION[ 'userid' ] ), "int" ),
                             GetSQLValueString( $obj->description, "text" ),
                             GetSQLValueString( $obj->source, "text" ),
                             GetSQLValueString( $obj->sourceurl, "text" ),
                             GetSQLValueString( $obj->servings, "text" ),
                             GetSQLValueString( $prep, "text" ),
                             GetSQLValueString( $cook, "text" ),
                             GetSQLValueString( $obj->isFeatured, "int" ),
                             GetSQLValueString( $obj->image, "text" ),
                             GetSQLValueString( $cusines2, "text" ),
                             GetSQLValueString( $concerns2, "text" ),
                             GetSQLValueString( $date, "date" ) );

                         //echo "<br>sql: {$insertSQL}<br>";

                         mysql_select_db( $database_chewsrite, $chewsrite );

                         $Result1 = mysql_query( $insertSQL, $chewsrite )or die( mysql_error() );
                         $last_id = mysql_insert_id();


                         $ingredients = str_getcsv( $obj->ingredients, ",", "^" );
                         //$ingredientAmounts = str_getcsv( $obj->ingredientAmounts, ",", "^" );

                         $servings = $obj->servings;

                         $fileJSON = "{\"title\": \"{$obj->recipename}\",\"yield\": \"{$servings}\",\"ingr\": [{$obj->ingredients}]}";


                         if ( $debug ) {
                             print "<pre>";
                             print_r( $ingredients );
                             print "</pre>";
                         }

                         //                            print "<pre>";
                         //                          print_r( $fileJSON);
                         //                          print "</pre>";

                         include( "add-bulk-nutrition.php" );


                         $insertedRecords[] = $last_id;
                     } else {
                         $duplicateRecords[] = $obj->image;
                     }
                 }

                 if ( $debug ) {
                     print "<pre>";
                     print_r( $nutrientObs );
                     print "</pre>";
                 }

                 $uploadStatus = count( $insertedRecords ) . " recipes saved";

                 if ( count( $duplicateRecords ) ) {
                     $uploadStatus .= "<br>" . count( $duplicateRecords ) . " duplicates skipped";
                 }

             } else if ( $extension == "zip" ) {

                 //add zip logic

                 $randomDirName = generateRandomString( 5 );

                 $zip = new ZipArchive;

                 $dirname = $randomDirName; //"temp1";

                 if ( file_exists( $dirname ) ) {
                     //echo "directory exists<br>";
                     $dirStatus = true;
                 } else {
                     if ( mkdir( $dirname ) ) {
                         //echo "created directory<br>";
                         $dirStatus = true;
                     }
                 }

                 $zipFile = $file;

                 if ( $dirStatus == true ) {
                     if ( $zip->open( $zipFile ) === TRUE ) {

                         $zip->extractTo( "{$dirname}/" );
                         $zip->close();
                         //echo 'ok';

                         //echo "extracting files<br>";

                         $extensions = array( "jpeg", "jpg", "png", "gif" );

                         if ( $handle = opendir( $dirname ) ) {

                             while ( false !== ( $entry = readdir( $handle ) ) ) {
                                 $extension = pathinfo( $entry, PATHINFO_EXTENSION );

                                 $file = "{$dirname}/{$entry}";

                                 if ( in_array( $extension, $extensions ) ) {

                                     //$entries[] = $entry;

                                     //check if imagename is in $duplicateRecords array. if it is, skip

                                     if ( !in_array( $entry, $duplicateRecords ) ) {

                                         $randomName = generateRandomString( 5 ) . "." . $extension;

                                         $oldname = "{$dirname}/{$entry}";
                                         $newname = "{$dirname}/{$randomName}";

                                         //echo "oldname: {$oldname}<br>";
                                         //echo "newname: {$newname}<br>";

                                         if ( rename( $oldname, $newname ) ) {
                                             //echo "rename successful<br>";

                                             $imagename = $randomName;
                                             $file = $newname;

                                             $updateSQL = sprintf( "UPDATE recipes SET imagename = %s WHERE imagename = %s AND userid = %s",
                                                 GetSQLValueString( mysql_real_escape_string( $randomName ), "text" ),
                                                 GetSQLValueString( mysql_real_escape_string( $entry ), "text" ),
                                                 GetSQLValueString( mysql_real_escape_string( de( $_SESSION[ 'userid' ] ) ), "int" ) );

                                             mysql_select_db( $database_chewsrite, $chewsrite );
                                             $Result2 = mysql_query( $updateSQL, $chewsrite )or die( mysql_error() );

                                             include( "s3-upload-file2.php" );
                                         }

                                         //echo "{$entry}<br>";

                                     } else {
                                         //echo "{$entry} skipped <br>";
                                     }
                                 }
                             }

                             closedir( $handle );
                         }

                     } else {
                         //echo 'extract file failed<br>';
                     }
                 }

                 deleteDir2( $dirname );

             } else {
                 $uploadStatus = "Invalid file type. Please choose .csv and .zip files";

                 $uploadOk = 0;
             }
         }
     }
 }

 ?>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Add Recipe</title>
    <link href="admin.css" rel="stylesheet" type="text/css">
</head>

<body>

    <?php include("includes/nav.php"); ?>

    <form action="<?php echo $editFormAction; ?>" method="POST" enctype="multipart/form-data" name="form1" id="form1">

        <p>&nbsp;</p>

        <h1>Add Recipes</h1>
        <p>&nbsp;</p>
        <div id="uploadStatus"></div>

        <table width="100%" cellspacing="5" class="table">
            <tbody>
                <tr>
                    <td width="813"><label for="files">Select File:</label>
                        <input type="file" name="files[]" multiple/>
                        <div id="fileType">.csv .zip</div>
                        <div id="fileStatus"></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="submitStatus">
                            <?php echo $uploadStatus;?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td><input name="userid" type="hidden" id="userid" value="<?php echo $_SESSION['userid']; ?>">
                        <input type="submit" name="submit" id="submit" value="Submit">
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </tbody>
        </table>
        <input type="hidden" name="MM_update" value="form1">
        <input type="hidden" name="MM_insert" value="form1">

    </form>
</body>
</html>
 <?php
 mysql_free_result( $rsRecipeDetails );
 mysql_free_result( $rsSelectedCuisines );
 ?>