 <?php //include("auth.php");

 include( "includes/devStatus.php" );

 require_once( '../../Connections/chewsrite.php' );
 include( "functions.php" );

 //var_dump($_SESSION);

 date_default_timezone_set( 'America/Detroit' );
 $date = date( "Y-m-d H:i:s" );

 mysql_select_db( $database_chewsrite, $chewsrite );
 $query_rsSelectedCuisines = "SELECT * FROM cuisines WHERE active = 1";
 $rsSelectedCuisines = mysql_query( $query_rsSelectedCuisines, $chewsrite )or die( mysql_error() );
 $row_rsSelectedCuisines = mysql_fetch_assoc( $rsSelectedCuisines );
 $totalRows_rsSelectedCuisines = mysql_num_rows( $rsSelectedCuisines );

 do {

     $obj1 = new stdClass;
     $obj1->cuisineid = $row_rsSelectedCuisines[ 'cuisineid' ];
     $obj1->cuisinename = $row_rsSelectedCuisines[ 'cuisinename' ];
     $cuisines[] = $obj1;

 } while ( $row_rsSelectedCuisines = mysql_fetch_assoc( $rsSelectedCuisines ) );


 //echo $query_rsSelectedCuisines . "<br>";

 $query_rsDietaryConcerns = "SELECT * FROM dietaryconcerns WHERE active = 1";
 $rsDietaryConcerns = mysql_query( $query_rsDietaryConcerns, $chewsrite )or die( mysql_error() );
 $row_rsDietaryConcerns = mysql_fetch_assoc( $rsDietaryConcerns );
 $totalRows_rsDietaryConcerns = mysql_num_rows( $rsDietaryConcerns );

 do {

     $obj2 = new stdClass;
     $obj2->concernid = $row_rsDietaryConcerns[ 'concernid' ];
     $obj2->concernname = $row_rsDietaryConcerns[ 'concernname' ];
     $concerns[] = $obj2;

 } while ( $row_rsDietaryConcerns = mysql_fetch_assoc( $rsDietaryConcerns ) );

 //var_dump($concerns);

 if ( isset( $_POST[ "submit" ] ) ) {

     $target_file = basename( $_FILES[ "fileToUpload" ][ "name" ] );
     $imageFileType = strtolower( pathinfo( $target_file, PATHINFO_EXTENSION ) );

     if ( $imageFileType == "csv" ) {
         $csvFile = $_FILES[ "fileToUpload" ][ "tmp_name" ]; //'utterances.csv';

         $utteranceList = readCSV( $csvFile );

         //    header('Content-Type: text/csv');
         //    header('Content-Disposition: attachment; filename="recipes.csv"');


         $user_CSV = array( 'recipename', 'description', 'sourceurl', 'servings', 'preptime', 'cooktime', 'isFeatured', 'cuisinetags', 'dietaryconcerntags', 'stepTitles','steps', 'ingredients', 'ingredientAmounts', 'image' );

         // very simple to increment with i++ if looping through a database result

         $i = 0;
         $limit = 100;

         $debug = false;

         foreach ( $utteranceList as $rowString ) {

             //echo "rs: {$rowString}<br>";

             if ( $i == 0 ) {
                 $obj1 = new stdClass;
             }

             $intent = $rowString;

             $obj1->$user_CSV[ $i ] = $intent;

             if ( $debug == true ) {
                 echo "<strong>{$user_CSV[$i]}:</strong> {$rowString}<br>";
             }

             $i++;

             if ( $i == 14 ) {
                 $csvList[] = $obj1;

                 $i = 0;
                 $obj1 = null;
             }
         }

         array_shift( $csvList );

//                 print "<pre>";
//                 print_r( $csvList );
//                 print "</pre>";
         
         //return;


         $date = date( "Y-m-d H:i:s" );

         foreach ( $csvList as $obj ) {
             //parse cuisine ids

             $cusines1 = explode( ",", $obj->cuisinetags );

             //        print "<pre>";
             //        print_r( $cusines1 );
             //        print "</pre>";

             foreach ( $cusines1 as $ob ) {
                 foreach ( $cuisines as $struct ) {

                     if ( $ob == $struct->cuisinename ) {
                         //$item = $struct;

                         $cuisineids[] = $struct->cuisineid;

                         break;
                     }
                 }
             }

             $cusines2 = implode( ",", $cuisineids );

             $cuisineids = [];


             //parse concern ids

             $concerns1 = explode( ",", $obj->dietaryconcerntags );

             //        print "<pre>";
             //        print_r( $concerns1 );
             //        print "</pre>";

             foreach ( $concerns1 as $ob ) {
                 foreach ( $concerns as $struct ) {

                     if ( $ob == $struct->concernname ) {
                         //$item = $struct;

                         $concernids[] = $struct->concernid;

                         break;
                     }
                 }
             }

             $concerns2 = implode( ",", $concernids );

             $concernids = [];

             //format prep and cook time

             if ( $obj->preptime > 1 ) {
                 $prep = $obj->preptime . " " . $obj->prepunits . "s";
             } else {
                 $prep = $obj->preptime . " " . $obj->prepunits;
             }

             if ( $obj->cooktime > 1 ) {
                 $cook = $obj->cooktime . " " . $obj->cookunits . "s";
             } else {
                 $cook = $obj->cooktime . " " . $obj->cookunits;
             }
             
             
             $name = strtolower($obj->recipename);
             $userid = de($_SESSION['userid']);
             
             mysql_select_db( $database_chewsrite, $chewsrite );
             $query_rsRecipes = "SELECT recipename FROM recipes WHERE userid = {$userid} AND LOWER(recipename) = '{$name}'";
             
             //echo $query_rsRecipes;
             
             $rsRecipes = mysql_query( $query_rsRecipes, $chewsrite )or die( mysql_error() );
             $totalRows_rsRecipes = mysql_num_rows( $rsRecipes );
             
             
             $stepTitles = str_getcsv($obj->stepTitles, ",", "^");
                                  
//             print "<pre>";
//             print_r( $stepTitles );
//             print "</pre>";
             
             $steps = str_getcsv($obj->steps, ",", "^");
                                  
//             print "<pre>";
//             print_r( $steps );
//             print "</pre>";
             
             
             //if( 0 == 0)
             if( $totalRows_rsRecipes == 0)
             {
                 $insertSQL = sprintf( "INSERT INTO recipes (recipename, userid, `description`, source, sourceurl, servings, preptime, cooktime, isFeatured, imagename, cuisinetags, dietaryconcerntags, dateadded) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                 GetSQLValueString( $obj->recipename, "text" ),
                 GetSQLValueString( de( $_SESSION[ 'userid' ] ), "int" ),
                 GetSQLValueString( $obj->description, "text" ),
                 GetSQLValueString( $obj->source, "text" ),
                 GetSQLValueString( $obj->sourceurl, "text" ),
                 GetSQLValueString( $obj->servings, "text" ),
                 GetSQLValueString( $prep, "text" ),
                 GetSQLValueString( $cook, "text" ),
                 GetSQLValueString( $obj->isFeatured, "int" ),
                 GetSQLValueString( $obj->image, "text" ),
                 GetSQLValueString( $cusines2, "text" ),
                 GetSQLValueString( $concerns2, "text" ),
                 GetSQLValueString( $date, "date" ) );

                 //echo "<br>sql: {$insertSQL}<br>";

                 mysql_select_db( $database_chewsrite, $chewsrite );
                 $Result1 = mysql_query( $insertSQL, $chewsrite )or die( mysql_error() );
                 
                 $last_id = mysql_insert_id();
                 
                 //insert into recipesteps
                 
                 $stepTitles = str_getcsv($obj->stepTitles, ",", "^");
                 $steps = str_getcsv($obj->steps, ",", "^");
                 $ingredients = str_getcsv($obj->ingredients, ",", "^");
                 $ingredientAmounts = str_getcsv($obj->ingredientAmounts, ",", "^");
                 
                 
//                 print "<pre>";
//                 print_r( $ingredientAmounts);
//                 print "</pre>";
                 
//                 print "<pre>";
//                 print_r( $stepTitles );
//                 print "</pre>";
//                 
                 $titlesInd = 0;
                 
                 
                 foreach ( $steps as $step ) {
                     
                     $insertStepSQL = sprintf("INSERT INTO recipedirections (title, directions, directionsorder, recipeid) VALUES (%s, %s, %s, %s)",
                        GetSQLValueString(mysql_real_escape_string($stepTitles[$titlesInd]), "text"),
                        GetSQLValueString(mysql_real_escape_string($step), "text"),
                        GetSQLValueString(mysql_real_escape_string($titlesInd ), "int"),
                        GetSQLValueString(mysql_real_escape_string($last_id), "int"));
                     
                     //echo "step: {$insertStepSQL}<br>";

                        mysql_select_db($database_chewsrite, $chewsrite);
                        $Result2 = mysql_query($insertStepSQL, $chewsrite) or die(mysql_error());
                     
                     $titlesInd++;
                 }
                 
                  //insert into recipe ingredients
                 
                 $ingredientInd = 0;
                 
                 foreach ($ingredients as $ingredient) 
                 {
                     $ingredientLower = strtolower($ingredient);
            
                     $query_rsRecipes = "SELECT ingredientid from ingredients WHERE LOWER(ingredient) LIKE '{$ingredientLower}'";

                    //echo $query_rsRecipes;

                    $rsRecipes = mysql_query($query_rsRecipes, $chewsrite) or die(mysql_error());
                    $row_rsRecipes = mysql_fetch_assoc($rsRecipes);
                    $totalRows_rsRecipes = mysql_num_rows($rsRecipes);

                    if($totalRows_rsRecipes > 0)
                    {
                        //ingredient exists

                        $ingedientid = $row_rsRecipes['ingredientid'];
                    }
                     
                    $amount = $ingredientAmounts[$ingredientInd];
                     
                    //echo "amount: {$amount}<br>";
                     
                    $vals = explode(" ",$amount);
                     

                    $insertIngredientSQL = sprintf("INSERT INTO recipeingredients (ingredientname, ingredientid, quantity, unit, recipeorder, recipeid) VALUES (%s, %s, %s, %s, %s, %s)",
                        GetSQLValueString(mysql_real_escape_string($ingredient), "text"),
                        GetSQLValueString(mysql_real_escape_string(blankNull($ingedientid)), "int"),
                        GetSQLValueString(mysql_real_escape_string($vals[0]), "text"),
                        GetSQLValueString(mysql_real_escape_string($vals[1]), "text"),
                        GetSQLValueString(mysql_real_escape_string($ingredientInd), "text"),
                        GetSQLValueString(mysql_real_escape_string($last_id), "int"));
                     
                     
                     //echo "ingredient: {$insertIngredientSQL}<br>";

                    mysql_select_db($database_chewsrite, $chewsrite);
                    $Result3 = mysql_query($insertIngredientSQL, $chewsrite) or die(mysql_error());
                     
                     $ingredientInd++;
                 }                 
                 
                 $insertedRecords[] = $last_id;
             }
             else
             {
                 $duplicateRecords[] = "";
             }
         }

         $uploadStatus = count( $insertedRecords ) . " recipes saved";
         
         if(count( $duplicateRecords ))
         {
             $uploadStatus .= "<br>" . count( $duplicateRecords ) . " duplicates skipped";
         }
         
         
     } else {

         $uploadStatus = "Invalid file type. Please choose CSV file";

         $uploadOk = 0;
     }
 }

 ?>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Add Recipe</title>
    <link href="admin.css" rel="stylesheet" type="text/css">
</head>

<body>

    <?php include("includes/nav.php"); ?>

    <form action="<?php echo $editFormAction; ?>" method="POST" enctype="multipart/form-data" name="form1" id="form1">

        <p>&nbsp;</p>

        <h1>Add Recipes</h1>
        <p>&nbsp;</p>
        <div id="uploadStatus"></div>

        <table width="100%" cellspacing="5" class="table">
            <tbody>
                <tr>
                    <td width="813"><label for="fileToUpload">Select File:</label>
                        <input type="file" name="fileToUpload" id="fileToUpload">
                        <div id="fileType">.csv</div>
                        <div id="fileStatus"></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="submitStatus">
                            <?php echo $uploadStatus;?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td><input name="userid" type="hidden" id="userid" value="<?php echo $_SESSION['userid']; ?>">
                        <input type="submit" name="submit" id="submit" value="Submit">
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </tbody>
        </table>
        <input type="hidden" name="MM_update" value="form1">
        <input type="hidden" name="MM_insert" value="form1">

    </form>
</body>
</html>
 <?php
 mysql_free_result( $rsRecipeDetails );

 mysql_free_result( $rsSelectedCuisines );
 ?>