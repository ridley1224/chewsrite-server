 <?php include("includes/devStatus.php"); 

require_once('../../Connections/chewsrite.php'); 
include("functions.php");
include("auth.php");

//$colname_rsNutritionDetails = "7";
if (isset($_GET['recipeid'])) {
  $colname_rsNutritionDetails = de($_GET['recipeid']);
}

mysql_select_db($database_chewsrite, $chewsrite);

$query_rsRecipeDetails = sprintf("SELECT recipename FROM recipes WHERE recipeid = %s", GetSQLValueString($colname_rsNutritionDetails, "int"));
$rsRecipeDetails = mysql_query($query_rsRecipeDetails, $chewsrite) or die(mysql_error());
$row_rsRecipeDetails = mysql_fetch_assoc($rsRecipeDetails);
$totalRows_rsRecipeDetails = mysql_num_rows($rsRecipeDetails);

//echo $query_rsRecipeDetails;


$query_rsNutritionDetails = sprintf("SELECT * FROM recipenutrition WHERE recipeid = %s", GetSQLValueString($colname_rsNutritionDetails, "int"));
$rsNutritionDetails = mysql_query($query_rsNutritionDetails, $chewsrite) or die(mysql_error());
$row_rsNutritionDetails = mysql_fetch_assoc($rsNutritionDetails);
$totalRows_rsNutritionDetails = mysql_num_rows($rsNutritionDetails);
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Nutrition Details</title>
<link href="admin.css" rel="stylesheet" type="text/css">
</head>

<body>

    <?php include("includes/nav.php"); ?>
    
<p>&nbsp;</p>

<h1>Nutrition Details</h1>

<table width="600" cellspacing="5" class="table">
  <tbody>
    <tr>
      <td><strong>Recipe</strong></td>
      <td><?php echo $row_rsRecipeDetails['recipename']; ?></td>
    </tr>
    <tr>
      <td width="134"><strong>Calories</strong></td>
      <td width="445"><?php if(!isset($row_rsNutritionDetails['Calories'])){echo "0";} else {echo $row_rsNutritionDetails['Calories'] . " cal";} ?></td>
    </tr>
    <tr>
      <td><strong>Carbohydrates</strong></td>
      <td><?php if(!isset($row_rsNutritionDetails['Carbohydrates'])){echo "0";} else {echo $row_rsNutritionDetails['Carbohydrates'] . " g";} ?></td>
    </tr>
    <tr>
      <td><strong>Protein</strong></td>
      <td><?php if(!isset($row_rsNutritionDetails['Protein'])){echo "0";} else {echo $row_rsNutritionDetails['Protein'] . " g";} ?></td>
    </tr>
    <tr>
      <td><strong>Fat</strong></td>
      <td><?php if(!isset($row_rsNutritionDetails['Fat'])){echo "0";} else {echo $row_rsNutritionDetails['Fat'] . " g";} ?></td>
    </tr>
    <tr>
      <td><strong>SaturatedFat</strong></td>
      <td><?php if(!isset($row_rsNutritionDetails['SaturatedFat'])){echo "0";} else {echo $row_rsNutritionDetails['SaturatedFat'] . " g";} ?></td>
    </tr>
    <tr>
      <td><strong>UnsaturatedFat</strong></td>
      <td><?php if(!isset($row_rsNutritionDetails['UnsaturatedFat'])){echo "0";} else {echo $row_rsNutritionDetails['UnsaturatedFat'] . " g";} ?></td>
    </tr>
    <tr>
      <td><strong>TransFat</strong></td>
      <td><?php if(!isset($row_rsNutritionDetails['TransFat'])){echo "0";} else {echo $row_rsNutritionDetails['TransFat'] . " g";} ?></td>
    </tr>
    <tr>
      <td><strong>Cholestrol</strong></td>
      <td><?php if(!isset($row_rsNutritionDetails['Cholestrol'])){echo "0";} else {echo $row_rsNutritionDetails['Cholestrol'] . " mg";} ?></td>
    </tr>
    <tr>
      <td><strong>Sodium</strong></td>
      <td><?php if(!isset($row_rsNutritionDetails['Sodium'])){echo "0";} else {echo $row_rsNutritionDetails['Sodium'] . " mg";} ?></td>
    </tr>
    <tr>
      <td><strong>Potassium</strong></td>
      <td><?php if(!isset($row_rsNutritionDetails['Potassium'])){echo "0";} else {echo $row_rsNutritionDetails['Potassium'] . " mg";} ?></td>
    </tr>
    <tr>
      <td><strong>Fiber</strong></td>
      <td><?php if(!isset($row_rsNutritionDetails['Fiber'])){echo "0";} else {echo $row_rsNutritionDetails['Fiber'] . " g";} ?></td>
    </tr>
    <tr>
      <td><strong>Sugars</strong></td>
      <td><?php if(!isset($row_rsNutritionDetails['Sugars'])){echo "0";} else {echo $row_rsNutritionDetails['Sugars'] . " g";} ?></td>
    </tr>
    <tr>
      <td><strong>VitaminA</strong></td>
      <td><?php if(!isset($row_rsNutritionDetails['VitaminA'])){echo "0";} else {echo $row_rsNutritionDetails['VitaminA'] . " µg";} ?></td>
    </tr>
    <tr>
      <td><strong>VitaminC</strong></td>
      <td><?php if(!isset($row_rsNutritionDetails['VitaminC'])){echo "0";} else {echo $row_rsNutritionDetails['VitaminC'] . " mg";} ?></td>
    </tr>
    <tr>
      <td><strong>Calcium</strong></td>
      <td><?php if(!isset($row_rsNutritionDetails['Calcium'])){echo "0";} else {echo $row_rsNutritionDetails['Calcium'] . " mg";} ?></td>
    </tr>
    <tr>
      <td><strong>Iron</strong></td>
      <td><?php if(!isset($row_rsNutritionDetails['Iron'])){echo "0";} else {echo $row_rsNutritionDetails['Iron'] . " mg";} ?></td>
    </tr>
    <tr>
      <td><a href="edit-recipeNutritionDetails.php?recipeid=<?php echo urlencode(en($colname_rsNutritionDetails)); ?>">Edit</a></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><a href="recipeDetails.php?recipeid=<?php echo urlencode(en($colname_rsNutritionDetails)); ?>">Back</a></td>
      <td>&nbsp;</td>
    </tr>
  </tbody>
</table>
</body>
</html>
<?php
mysql_free_result($rsNutritionDetails);
?>
  