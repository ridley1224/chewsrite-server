 <?php include("includes/devStatus.php"); 

require_once('../../Connections/chewsrite.php'); 
include("functions.php");

//include("auth.php");

//$_SESSION['userid'] = "24";

//var_dump($_SESSION);

date_default_timezone_set('America/Detroit');
$date = date("Y-m-d H:i:s");

//echo "recipename: " . $_POST["recipename"];

if ((isset($_POST["recipename"]))) {
    
    if($_FILES['file'])
    {    
        $file = $_FILES['file']['tmp_name'];
        $filename = $_FILES['file']['name'];    
        $extension = ltrim(strtolower(strrchr($filename, '.')),".");

        $imagename = generateRandomString(5) . ".{$extension}";

        include("s3-upload-file.php");
    }
    else
    {
        include("includes/insertRecipe.php");
    }
}

?>