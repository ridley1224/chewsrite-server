<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>User Recipes</title>
<link href="admin.css" rel="stylesheet" type="text/css">
</head>
    
    <?php include("includes/nav.php"); ?>

<form action="upload-s3-bulk.php" method="post" enctype="multipart/form-data">
    <table width="100%">
        <tr>
            <td>Select Photo (one or multiple):</td>
            <td><input type="file" name="files[]" multiple/></td>
        </tr>
        <tr>
            <td colspan="2" align="center">Note: Supported image format: .jpeg, .jpg, .png, .gif</td>
        </tr>
        <tr>
            <td colspan="2" align="center"><input type="submit" value="Upload" id="selectedButton"/></td>
        </tr>
    </table>
</form>
    
</html>