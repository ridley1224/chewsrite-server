 <?php include("includes/devStatus.php"); 

require_once('../../Connections/chewsrite.php'); 
include("functions.php");
include("auth.php");

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_rsReviews = 20;
$pageNum_rsReviews = 0;
if (isset($_GET['pageNum_rsReviews'])) {
  $pageNum_rsReviews = $_GET['pageNum_rsReviews'];
}
$startRow_rsReviews = $pageNum_rsReviews * $maxRows_rsReviews;

mysql_select_db($database_chewsrite, $chewsrite);
//$query_rsReviews = "SELECT * FROM reviews ORDER BY reviewdate DESC";


$query_rsReviews = sprintf("SELECT a.*, b.*, c.* FROM (SELECT * FROM reviews) as a INNER JOIN (SELECT fullname,userid from users) as b on a.userid = b.userid INNER JOIN (SELECT imagename,recipename, recipeid from recipes) as c on a.recipeid = c.recipeid");


$query_limit_rsReviews = sprintf("%s LIMIT %d, %d", $query_rsReviews, $startRow_rsReviews, $maxRows_rsReviews);
$rsReviews = mysql_query($query_limit_rsReviews, $chewsrite) or die(mysql_error());
$row_rsReviews = mysql_fetch_assoc($rsReviews);

if (isset($_GET['totalRows_rsReviews'])) {
  $totalRows_rsReviews = $_GET['totalRows_rsReviews'];
} else {
  $all_rsReviews = mysql_query($query_rsReviews);
  $totalRows_rsReviews = mysql_num_rows($all_rsReviews);
}
$totalPages_rsReviews = ceil($totalRows_rsReviews/$maxRows_rsReviews)-1;

$queryString_rsReviews = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_rsReviews") == false && 
        stristr($param, "totalRows_rsReviews") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_rsReviews = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_rsReviews = sprintf("&totalRows_rsReviews=%d%s", $totalRows_rsReviews, $queryString_rsReviews);


?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>User Recipes</title>
<link href="admin.css" rel="stylesheet" type="text/css">
</head>

<body>

<?php include("includes/nav.php"); ?>
    
<p>&nbsp;</p>

<h1>Reviews</h1>
    
    <?php  if($totalRows_rsReviews > 0 ) { ?>

<table width="100%" cellspacing="5" class="table">
    <tbody>
      <tr align="left">
        <th width="9%">&nbsp;</th>
        <th width="17%">Recipe</th>
        <th width="30%">Review</th>
        <th width="12%">Date</th>
        <th width="7%">&nbsp;</th>
        <th width="7%">&nbsp;</th>
        <th width="18%">&nbsp;</th>
      </tr>
   <?php  do { ?>     
        
        
      <tr>
        <td><strong><img src="https://s3-us-west-2.amazonaws.com/chewsrite/images/<?php echo $row_rsReviews['imagename']; ?>" alt="" width="80" height="80"/></strong></td>
        <td><?php echo $row_rsReviews['recipename']; ?></td>
        <td><?php 
             
             if (strlen($row_rsReviews['review']) > 100)
             { 
                 echo substr($row_rsReviews['review'], 0, 100) . "..."; 
             } 
             else 
             { 
                 echo $row_rsReviews['review'];
             } ?></td>
        <td><?php echo date("m-d-Y",strtotime($row_rsReviews['reviewdate'])); ?></td>
        <td><a href="edit-reviewDetails.php?reviewid=<?php echo urlencode(en($row_rsReviews['reviewid'])); ?>">Edit</a></td>
        <td><a href="deleteReview.php?reviewid=<?php echo urlencode(en($row_rsReviews['reviewid'])); ?>">Delete</a></td>
        <td>
            
            <?php
             
            if (strlen($row_rsReviews['approved']) != 1)
             { 
                 ?><a href="approveReview.php?reviewid=<?php echo urlencode(en($row_rsReviews['reviewid'])); ?>">Approve</a><?php
             } 
             else
             {
                 echo "Approved";
             }
             
             ?>
          
          </td>
      </tr>
        
          <?php } while ($row_rsReviews = mysql_fetch_assoc($rsReviews)); ?>

    </tbody>
</table>
  <p>&nbsp;
    <?php if ($pageNum_rsReviews > 0) { // Show if not first page ?>
      <a href="<?php printf("%s?pageNum_rsReviews=%d%s", $currentPage, 0, $queryString_rsReviews); ?>">First</a> 
      <?php } // Show if not first page ?>
    <?php if ($pageNum_rsReviews > 0) { // Show if not first page ?>
      <a href="<?php printf("%s?pageNum_rsReviews=%d%s", $currentPage, max(0, $pageNum_rsReviews - 1), $queryString_rsReviews); ?>">Previous</a> 
      <?php } // Show if not first page ?>
    <?php if ($pageNum_rsReviews < $totalPages_rsReviews) { // Show if not last page ?>
  <a href="<?php printf("%s?pageNum_rsReviews=%d%s", $currentPage, min($totalPages_rsReviews, $pageNum_rsReviews + 1), $queryString_rsReviews); ?>">Next</a> 
  <?php } // Show if not last page ?>
    <?php if ($pageNum_rsReviews < $totalPages_rsReviews) { // Show if not last page ?>
      <a href="<?php printf("%s?pageNum_rsReviews=%d%s", $currentPage, $totalPages_rsReviews, $queryString_rsReviews); ?>">Last</a> 
      <?php } // Show if not last page ?>
</p>
    
    <?php } else {echo "No reviews";} // if reviews > 0 ?>
    
</body>
</html>
<?php
mysql_free_result($rsReviews);
?>
  