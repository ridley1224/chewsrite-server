 <?php //include("auth.php");

 include( "includes/devStatus.php" );

 require_once( '../../Connections/chewsrite.php' );
 include( "functions.php" );

 //var_dump($_SESSION);

 date_default_timezone_set( 'America/Detroit' );
 $date = date( "Y-m-d H:i:s" );

 mysql_select_db( $database_chewsrite, $chewsrite );

 //var_dump($concerns);

 if ( isset( $_POST[ "submit" ] ) ) {

     //echo "count: " . count($_FILES["files"]["name"]);

     //sort($_FILES["files"]["name"]);

     //var_dump($_FILES["files"]["name"]);

     //echo "<br>";

     if ( count( $_FILES[ "files" ][ "name" ] ) > 1 ) {
         $uploadStatus = "You can only upload a maximum of 1 files";
         $uploadOk = 0;
     } else {
         foreach ( $_FILES[ "files" ][ "tmp_name" ] as $key => $tmp_name ) {

             $target_file = $_FILES[ "files" ][ "name" ][ $key ];
             $file = $_FILES[ "files" ][ "tmp_name" ][ $key ];
             //$extension = pathinfo( $filename, PATHINFO_EXTENSION );

             //$target_file = basename( $_FILES[ "files" ][ "name" ] );
             $extension = strtolower( pathinfo( $target_file, PATHINFO_EXTENSION ) );

             //echo "file: {$target_file}";

             //return;

             //echo "ext: {$extension}<br>";

             //parse recipe instructions file

             if ( $extension == "csv" ) {

                 //return

                 $csvFile = $file;
                 $utteranceList = readCSV( $csvFile );

                 $i = 0;
                $currentRows = 0;
                $totalRows = count( $utteranceList );

                //echo "tr: ".$totalRows . "<br>";
                foreach ( $utteranceList as $rowString1 ) {

                    $currentRows++;

                    if ( $rowString1 == "Recipe Name" ) {
                        $i = 0;

                        if ( $hasObjects == true ) {
                            $objects[] = $obj2;
                        }

                        $obj2 = new stdClass;

                        $hasObjects = true;
                    }

                    if ( $i == 1 ) {
                        $recipename = $rowString1;
                        //echo "Recipe Name: {$rowString1}<br>";
                        $obj2->recipename = $recipename;
                    }

                    if ( $i > 1 ) {
                        if ( $i % 2 == 0 ) {
                            //echo "Title: {$rowString1}<br>";
                            $stepTitles[] = $rowString1;
                            $obj2->titles[] = $rowString1;
                        } else {
                            //echo "Step: {$rowString1}<br>";
                            $steps[] = $rowString1;
                            $obj2->steps[] = $rowString1;
                        }
                    }

                    $i++;

                    if ( $currentRows == $totalRows ) {
                        $objects[] = $obj2;
                    }
                }

                //var_dump($objects);

                //print "<pre>";
                //print_r( $objects );
                //print "</pre>";

                $titlesInd = 0;

                foreach ( $objects as $ob ) {

                    $stepTitles = $ob->titles;
                    $steps = $ob->steps;

                    $recipe = strtolower( $ob->recipename );

                    //echo "recipe: {$ob->recipename}<br>";

                    mysql_select_db( $database_chewsrite, $chewsrite );
                    $query_rsRecipeInfo = sprintf( "SELECT recipeid FROM recipes WHERE LCASE(recipename) = %s",
                        GetSQLValueString( mysql_real_escape_string( $recipe ), "text" ) );


                    //$query_rsRecipeInfo = "SELECT recipeid FROM recipes WHERE LCASE(recipename) = '{$recipe}'";
                    $rsRecipeInfo = mysql_query( $query_rsRecipeInfo, $chewsrite )or die( mysql_error() );
                    $row_rsRecipeInfo = mysql_fetch_assoc( $rsRecipeInfo );
                    $totalRows_rsRecipeInfo = mysql_num_rows( $rsRecipeInfo );

                    //echo "query_rsRecipeInfo: {$query_rsRecipeInfo}<br>";

                    if ( $totalRows_rsRecipeInfo > 0 ) {
                        
                        $titlesInd = 0;

                        foreach ( $steps as $step ) {

                            $insertStepSQL = sprintf( "INSERT INTO recipedirections (title, directions, directionsorder, recipeid, datecreated) VALUES (%s, %s, %s, %s, %s)",
                                GetSQLValueString( mysql_real_escape_string( $stepTitles[ $titlesInd ] ), "text" ),
                                GetSQLValueString( mysql_real_escape_string( $step ), "text" ),
                                GetSQLValueString( mysql_real_escape_string( $titlesInd ), "int" ),
                                GetSQLValueString( mysql_real_escape_string( $row_rsRecipeInfo[ 'recipeid' ] ), "int" ),
                                GetSQLValueString( $date, "date" ) );

                            mysql_select_db( $database_chewsrite, $chewsrite );
                            $Result2 = mysql_query( $insertStepSQL, $chewsrite )or die( mysql_error() );

                            //echo "insert SQL: {$insertStepSQL}<br>";
                            
                            $titlesInd++;
                        }
                        
                        $insertedRecords[] = $ob->recipename;
                    } else {
                        echo "No recipe named {$recipe}<br>";
                    }    

                    
                }
                 
                 $uploadStatus = "Directions for " . count( $insertedRecords ) . " recipes saved";

             } else {
                 $uploadStatus = "Invalid file type. Please choose .csv file";

                 $uploadOk = 0;
             }
         }
     }
 }

 ?>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Add Recipe</title>
    <link href="admin.css" rel="stylesheet" type="text/css">
</head>

<body>

    <?php include("includes/nav.php"); ?>

    <form action="<?php echo $editFormAction; ?>" method="POST" enctype="multipart/form-data" name="form1" id="form1">

        <p>&nbsp;</p>

        <h1>Add Recipes</h1>
        <p>&nbsp;</p>
        <div id="uploadStatus"></div>

        <table width="100%" cellspacing="5" class="table">
            <tbody>
                <tr>
                    <td width="813"><label for="files">Select File:</label>
                        <input type="file" name="files[]" multiple/>
                        <div id="fileType">.csv</div>
                        <div id="fileStatus"></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="submitStatus">
                            <?php echo $uploadStatus;?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td><input name="userid" type="hidden" id="userid" value="<?php echo $_SESSION['userid']; ?>">
                        <input type="submit" name="submit" id="submit" value="Submit">
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </tbody>
        </table>
        <input type="hidden" name="MM_update" value="form1">
        <input type="hidden" name="MM_insert" value="form1">

    </form>
</body>
</html>
 <?php
 mysql_free_result( $rsRecipeDetails );
 mysql_free_result( $rsSelectedCuisines );
 ?>