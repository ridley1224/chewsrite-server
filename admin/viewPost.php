<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "index.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php require_once('../../../Connections/prospectdugout.php'); ?>
<?php include ("../en-de.php");?>
<?php include ("../functions.php");?>

<?php

//echo "postid: " . $_GET['postid'] . "<br>";
//echo "de: " . de($_GET['postid']);

$colname_rsPostInfo = "-1";
if (isset($_GET['postid'])) {
  $colname_rsPostInfo = de($_GET['postid']);
}
mysql_select_db($database_prospectdugout, $prospectdugout);
$query_rsPostInfo = sprintf("SELECT postid, image, caption FROM posts WHERE postid = %s", GetSQLValueString($colname_rsPostInfo, "int"));



$rsPostInfo = mysql_query($query_rsPostInfo, $prospectdugout) or die(mysql_error());
$row_rsPostInfo = mysql_fetch_assoc($rsPostInfo);
$totalRows_rsPostInfo = mysql_num_rows($rsPostInfo);
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>View Post</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<table width="400" cellspacing="10">
  <tbody>
    <tr>
      <td><a href="home.php">Home</a></td>
      <td><a href="reportedPosts.php">Reported Posts</a></td>
      <td><a href="featuredRequests.php">Featured Post Requests</a></td>
      <td><a href="logout.php">Logout</a></td>
    </tr>
  </tbody>
</table>
<table width="600" cellspacing="10">
  <tbody>
    <tr>
      <td width="75"><a href="viewPost.php?postid=<?php echo $row_rsReportedPosts['postid']; ?>">
      <img src="../userimages/1.jpg" width="75" height="76"></a></td>
      
      <td width="234"><?php echo $row_rsPostInfo['caption']; ?></td>
      <td width="122" style="text-align: center"><a href="reportedPosts.php?deletereport=true&postid=<?php echo urlencode(en($row_rsPostInfo['postid'])); ?>">Delete Report</a></td>
      <td width="109" style="text-align: center"><a href="reportedPosts.php?remove=true&postid=<?php echo urlencode(en($row_rsPostInfo['postid']); ?>">Remove Post</a></td>
    </tr>
  </tbody>
</table>
</body>
</html>
<?php
mysql_free_result($rsPostInfo);
?>
