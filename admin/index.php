<?php 

include("includes/devStatus.php");

require_once('../../Connections/chewsrite.php'); 

include ("../en-de.php");
include("functions.php");


// *** Validate request to login to this site.
if (!isset($_SESSION)) {
  session_start();
}

$loginFormAction = $_SERVER['PHP_SELF'];
if (isset($_GET['accesscheck'])) {
  $_SESSION['PrevUrl'] = $_GET['accesscheck'];
}

if (isset($_POST['username'])) {
  $loginUsername=$_POST['username'];
  $password=$_POST['password'];
  $MM_fldUserAuthorization = "";
  $MM_redirectLoginSuccess = "viewRecipes.php";
  $MM_redirectLoginFailed = "index.php";
  $MM_redirecttoReferrer = false;
  mysql_select_db($database_chewsrite, $chewsrite);
  
  $LoginRS__query=sprintf("SELECT email, password, userid,usertype FROM users WHERE email=%s AND password=%s AND usertype = 2",
    GetSQLValueString($loginUsername, "text"), GetSQLValueString($password, "text")); 
   
  $LoginRS = mysql_query($LoginRS__query, $chewsrite) or die(mysql_error());
    $row_rsUser = mysql_fetch_assoc($LoginRS);
  $loginFoundUser = mysql_num_rows($LoginRS);
  if ($loginFoundUser) {
     $loginStrGroup = "";
    
	if (PHP_VERSION >= 5.1) {session_regenerate_id(true);} else {session_regenerate_id();}
    //declare two session variables and assign them
    $_SESSION['MM_Username'] = $loginUsername;
    $_SESSION['MM_UserGroup'] = $row_rsUser['usertype'];
      $_SESSION['userid'] = en($row_rsUser['userid']);

    if (isset($_SESSION['PrevUrl']) && false) {
      $MM_redirectLoginSuccess = $_SESSION['PrevUrl'];	
    }
    header("Location: " . $MM_redirectLoginSuccess );
  }
  else {
    header("Location: ". $MM_redirectLoginFailed );
  }
}

?>

<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Admin Login</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<h1>Login </h1>
<form ACTION="<?php echo $loginFormAction; ?>" id="form1" name="form1" method="POST">
  <table width="0" cellspacing="10">
    <tbody>
      <tr>
        <td>Username</td>
        <td><input name="username" type="text" id="username"></td>
      </tr>
      <tr>
        <td>Password</td>
        <td><input name="password" type="password" id="password"></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><input type="submit" name="submit" id="submit" value="Submit"></td>
      </tr>
    </tbody>
  </table>
</form>
<!--<p><a href="home.php">Skip Login for Testing</a></p>-->
</body>
</html>