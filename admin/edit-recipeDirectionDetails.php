 <?php
 include( "includes/devStatus.php" );

 require_once( '../../Connections/chewsrite.php' );
 include( "functions.php" );
 include( "auth.php" );

 //$colname_rsNutritionDetails = "7";

 if ( isset( $_GET[ 'recipeid' ] ) ) {
     $colname_rsNutritionDetails = de( $_GET[ 'recipeid' ] );
 }


 //$colname_rsNutritionDetails = 56;

 if ( isset( $_GET[ 'd' ] ) && $_GET[ 'd' ] == 1 && isset( $_GET[ 'did' ] ) ) {
     $deleteSQL = sprintf( "delete FROM recipedirections WHERE directionsid=%s",
         GetSQLValueString( de( $_GET[ 'did' ] ), "int" ) );

     mysql_select_db( $database_chewsrite, $chewsrite );
     $Result1 = mysql_query( $deleteSQL, $chewsrite )or die( mysql_error() );
 }

 mysql_select_db( $database_chewsrite, $chewsrite );

 $query_rsRecipeDetails = sprintf( "SELECT recipename FROM recipes WHERE recipeid = %s", GetSQLValueString( $colname_rsNutritionDetails, "int" ) );
 $rsRecipeDetails = mysql_query( $query_rsRecipeDetails, $chewsrite )or die( mysql_error() );
 $row_rsRecipeDetails = mysql_fetch_assoc( $rsRecipeDetails );
 $totalRows_rsRecipeDetails = mysql_num_rows( $rsRecipeDetails );

 //echo $query_rsRecipeDetails;

 $query_rsNutritionDetails = sprintf( "SELECT * FROM recipedirections WHERE recipeid = %s", GetSQLValueString( $colname_rsNutritionDetails, "int" ) );
 $rsNutritionDetails = mysql_query( $query_rsNutritionDetails, $chewsrite )or die( mysql_error() );
 $row_rsNutritionDetails = mysql_fetch_assoc( $rsNutritionDetails );
 $totalRows_rsNutritionDetails = mysql_num_rows( $rsNutritionDetails );

 // $i = 0;
 //
 // do {
 //
 //     echo "hi<br>";
 //
 //     $updateSQL = sprintf( "UPDATE recipedirections SET directionsorder = %s WHERE directionsid=%s",
 //         GetSQLValueString( $i, "int" ),
 //         GetSQLValueString( $row_rsNutritionDetails[ 'directionsid' ], "int" ) );
 //
 //     echo "q: {$updateSQL}<br>";
 //
 //     mysql_select_db( $database_chewsrite, $chewsrite );
 //     //$Result1 = mysql_query($updateSQL, $chewsrite) or die(mysql_error());
 //
 //     $i++;
 //
 // } while ( $row_rsNutritionDetails = mysql_fetch_assoc( $rsNutritionDetails ) );

 ?>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Nutrition Details</title>
    <link href="admin.css" rel="stylesheet" type="text/css">
    <script src="../../jquery/jquery-1.11.1.min.js"></script>
    <script src="edit-directions.js"></script>
</head>

<body>

    <?php include("includes/nav.php"); ?>

<p>&nbsp;</p>

<h1>Edit Recipe Directions</h1>
<p>
  <input type="button" name="addRowBtn" id="addRowBtn" value="Add recipe step">
</p>

    <table width="600" cellspacing="5" class="table" id="stepsTable">
        <tbody>
            

            <?php 
      
      if($totalRows_rsNutritionDetails > 0) {
      
      $i = 0; do { ?>

            <tr>
                <td width="72"><strong>Step <?php echo $i+1; ?></strong>
                </td>
                <td width="435">
                    <input type="text" class="input3" id="title<?php echo $i; ?>" value="<?php echo $row_rsNutritionDetails['title']; ?>" placeholder="title">
                    <input type="text" class="description3" id="direction<?php echo $i; ?>" value="<?php echo $row_rsNutritionDetails['directions']; ?>" placeholder="directions">
                </td>
                <td width="65" align="center"><a href="edit-recipeDirectionDetails.php?d=1&did=<?php echo urlencode(en($row_rsNutritionDetails['directionsid'])); ?>&recipeid=<?php echo urlencode(en($colname_rsNutritionDetails)); ?>"></a>
                  <select name="select<?php echo $i; ?>" id="select<?php echo $i; ?>">
                      
                      <?php for ($x = 0; $x < $totalRows_rsNutritionDetails; $x++) { ?>
                      
                      <option value="<?php echo $i+1; ?>"><?php echo $x+1; ?></option>
                      
                      <?php }?>                    
                      
                </select></td>
              <td width="65" align="center"><a href="edit-recipeDirectionDetails.php?d=1&did=<?php echo urlencode(en($row_rsNutritionDetails['directionsid'])); ?>&recipeid=<?php echo urlencode(en($colname_rsNutritionDetails)); ?>">delete</a></td>
            </tr>

            <?php                   
          if(isset($_GET['d']) && $_GET[ 'd' ] == 1 && isset($_GET['did']))
          {
              $updateSQL = sprintf("UPDATE recipedirections SET directionsorder = %s WHERE directionsid=%s",
               GetSQLValueString($i, "int"),
                GetSQLValueString($row_rsNutritionDetails['directionsid'], "int"));
              
              //echo "q: {$updateSQL}<br>";

              mysql_select_db($database_chewsrite, $chewsrite);
              $Result1 = mysql_query($updateSQL, $chewsrite) or die(mysql_error());
          }
            $i++;

            } while ($row_rsNutritionDetails = mysql_fetch_assoc($rsNutritionDetails)); } ?>

            <tr>
                <td>&nbsp;</td>
                <td>Submit</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td><a href="recipeDirectionDetails.php?recipeid=<?php echo urlencode(en($colname_rsNutritionDetails)); ?>">Back</a>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <input type="hidden" id="rows" value="<?php echo  $totalRows_rsNutritionDetails?>">
</body>
</html>
 <?php
 mysql_free_result( $rsNutritionDetails );
 ?>