<?php

$insertSQL = sprintf("INSERT INTO recipes (recipename, userid, `description`, source, sourceurl, servings, preptime, cooktime, isFeatured, imagename, videoname, cuisinetags, dietaryconcerntags, dateadded) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['recipename'], "text"),
                       GetSQLValueString(de($_POST['userid']), "int"),
                       GetSQLValueString($_POST['description'], "text"),
                       GetSQLValueString($_POST['source'], "text"),
                       GetSQLValueString($_POST['sourceurl'], "text"),
                       GetSQLValueString($_POST['servings'], "text"),
                       GetSQLValueString($_POST['preptime'], "text"),
                       GetSQLValueString($_POST['cooktime'], "text"),
                       GetSQLValueString($_POST['isFeatured'], "int"),
                       GetSQLValueString($imagename, "text"),
                        GetSQLValueString($videoname, "text"),
                       GetSQLValueString($_POST['cuisinetags'], "text"),
                       GetSQLValueString($_POST['dietaryconcerntags'], "text"),
                       GetSQLValueString($date, "date"));
    
    //echo "insertSQL: " . $insertSQL;

    mysql_select_db($database_chewsrite, $chewsrite);
    $Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());

    $recipeid = mysql_insert_id();
    
    $recipeStatus = "recipe saved successfully";

//echo $recipeStatus;


if ((isset($_POST["recipeIngredients"]))) {
    
    $ingredients = explode(",",$_POST["recipeIngredients"]);
    $amounts = explode(",",$_POST["recipeAmounts"]);
    $units = explode(",",$_POST["recipeUnits"]);
    
    
    
    $i = 0;
    
    foreach($ingredients as $ingredient) {
        
        //$ingredientString = explode(" ",$ingredient);
        
        //$ingredientName = $ingredientString[0];
        $quantity = $amounts[$i];
        $unit = $units[$i];
        
        
        $lower = strtolower($ingredient);

        //$query_rsRecipes = "SELECT ingredientid from ingredients WHERE LOWER(ingredient) LIKE '{$a}'";
        $query_rsRecipes = "SELECT ingredientid from ingredients WHERE LOWER(ingredient) = '{$lower}'";

        //echo $query_rsRecipes;

        $rsRecipes = mysql_query($query_rsRecipes, $chewsrite) or die(mysql_error());
        $row_rsRecipes = mysql_fetch_assoc($rsRecipes);
        $totalRows_rsRecipes = mysql_num_rows($rsRecipes);

        if($totalRows_rsRecipes > 0)
        {
            //ingredient exists

            $ingredientid = $row_rsRecipes['ingredientid'];
            $status .= " current ingredient saved: {$ingedientid}";

            $updateCount[] = $ingredientid;
        }
        else
        {
            //insert new ingredient

            $insertSQL = sprintf("INSERT INTO ingredients (ingredient, userid, datecreated) VALUES (%s, %s, %s)",
                    GetSQLValueString(mysql_real_escape_string($ingredient), "text"),
                    GetSQLValueString(mysql_real_escape_string($_POST['userid']), "int"),
                    GetSQLValueString($date, "date"));

            mysql_select_db($database_chewsrite, $chewsrite);
            $Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());	

            $ingredientid = mysql_insert_id();	
            $newCount[] = $ingredientid;

            $status .= " new ingredient saved: {$ingedientid}";                
        }

        $insertSetSQL = sprintf("INSERT INTO recipeingredients (ingredientname, quantity, unit, recipeorder, recipeid, ingredientid, active, datecreated) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
            GetSQLValueString(mysql_real_escape_string($ingredient), "text"),
            GetSQLValueString(mysql_real_escape_string($quantity), "text"),
            GetSQLValueString(mysql_real_escape_string($unit), "text"),
            GetSQLValueString(mysql_real_escape_string($i), "text"),
            GetSQLValueString(mysql_real_escape_string($recipeid), "text"),
            GetSQLValueString(mysql_real_escape_string($ingredientid), "int"),
            GetSQLValueString(1, "int"),
            GetSQLValueString($date, "date"));

            mysql_select_db($database_chewsrite, $chewsrite);
            $Result2 = mysql_query($insertSetSQL, $chewsrite) or die(mysql_error());
        
        $i++;
    }
    
    
    //recipeSteps
    //insert into recipedirections
    
    $i = 0;

    $recipeTitles = explode(",",$_POST["recipeTitles"]);
    $recipeSteps = explode(",",$_POST["recipeSteps"]);
    
    foreach($recipeTitles as $title) {
        
        $titleInd = $recipeTitles[$i];
        $stepInd = $recipeSteps[$i];

        $insertStepSQL = sprintf("INSERT INTO recipedirections (title, directions, recipeid, directionsorder,datecreated) VALUES (%s, %s, %s, %s, %s)",
            GetSQLValueString(mysql_real_escape_string($titleInd), "text"),
            GetSQLValueString(mysql_real_escape_string($stepInd), "text"),
            GetSQLValueString($recipeid, "int"),
            GetSQLValueString($i, "int"),
            GetSQLValueString(mysql_real_escape_string($date), "date"));

            mysql_select_db($database_chewsrite, $chewsrite);
            $Result2 = mysql_query($insertStepSQL, $chewsrite) or die(mysql_error());
        
        $i++;
    }
    
    $response = ["status" => $status, "newCount" => count($newCount), "updateCount" => count($updateCount), "insertSQL" => $insertSQL, "recipeStatus" => $recipeStatus, "recipeid" => en($recipeid) ];
    
    $json = json_encode($response);
    
    echo $json;
}


?>