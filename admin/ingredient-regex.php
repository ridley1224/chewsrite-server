<?php 

$measurements = array('tbsp', 'tsp', 'cup', 'cups', 'oz', 'ounce', 'ounces', 'pint', 'pints', 'quart', 'quarts', 'gal', 'gallon', 'mil', 'milliliter', 'lt', 'liter', 'lb', 'pount', 'lbs', 'pounds', 'large', 'lg', 'medium', 'med', 'small', 'sm', 'extra large', 'xl', 'dozen', 'doz', 'unit', 'units', 'fl ounce', 'fl oz', 'gram', 'g', 'kilogram', 'kg');


$pattern2 = '/[-]?[0-9]+[,.]?[0-9]*([\/][0-9]+[,.]?[0-9]*)*/';

$replacement = '$2';

//echo "<strong>ing:</strong> {$ingredient}<br>";
        
if (preg_match($pattern2, $ingredient)) 
{
    $replace1 = preg_replace($pattern2, $replacement, $ingredient);
    //echo "Found<br>";
    //echo "<br><strong>replace:</strong> " . strtolower($replace1) . "<br>";
    
    $replace1 = preg_replace($pattern2, $replacement, $ingredient);
  
    preg_match($pattern2, $ingredient, $matches);
    $ingredientCount = $matches[0];
    
    //echo "ingredientCount: {$ingredientCount}<br>";
    

    foreach ($measurements as $measurement) {

        //echo "  measurement val: " . $measurement . "<br>";

        if (stripos(trim($replace1), trim(strtolower($measurement))) !== FALSE) {

            //echo "<strong>measurement found</strong><br>"; 

            //echo "new: " . str_ireplace(trim($measurement),"",$replace1) . "<br>";

            $measurementVal = $measurement;
            $val = str_ireplace(trim($measurement),"",$replace1);

            break;
        }
        else
        {
            $val = $replace1;
            $measurementVal = "";
        }
    }

} else {

    //echo "Not found<br>";
    
    $measurementVal = "";
    
    if(!$ingredientCount)
    {
        $ingredientCount = 1;
    }
    
    $val = $ingredient;
}

//echo "<strong>ingredient:</strong> {$val}<br>";
//echo "count: {$ingredientCount}<br>";
//echo "measurement: {$measurementVal}<br>";
//echo "<strong>ingredient:</strong> {$val}<br>";

?>