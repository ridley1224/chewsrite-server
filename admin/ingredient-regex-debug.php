<?php 

//https://www.regextester.com/94462
//https://www.freeformatter.com/regex-tester.html

    
$str = "\"3/4 cup Farro\", \"1 Lemon\", \"1 Red Onion\", \"1 1/2 Cup Sugar Snap Peas\", \"3/4 Cup Mint\", \"2 Tbsp Red Wine Vinegar\", \"2 Tbsp Sugar\", \"1/2 Cup Kalamata Olives\", \"1/4 Cup Parmesan Cheese (grated)\", \"2 Tbsp Olive Oil\", \"1 Tsp Salt\", \"1 Tsp Black Pepper\", \"Apple\"";

//echo $str;

$ingredients = str_getcsv( $str, ",", '"' );

//                                print "<pre>";
//                             print_r( $ingredients );
//                             print "</pre>";

$measurements = array('tbsp', 'tsp', 'cup', 'cups', 'oz', 'ounce', 'ounces', 'pint', 'pints', 'quart', 'quarts', 'gal', 'gallon', 'mil', 'milliliter', 'lt', 'liter', 'lb', 'pount', 'lbs', 'pounds', 'large', 'lg', 'medium', 'med', 'small', 'sm', 'extra large', 'xl', 'dozen', 'doz', 'unit', 'units', 'fl ounce', 'fl oz', 'gram', 'g', 'kilogram', 'kg');


$pattern2 = '/[-]?[0-9]+[,.]?[0-9]*([\/][0-9]+[,.]?[0-9]*)*/';

$replacement = '$2';

foreach($ingredients as $ing)
{
    //echo "<strong>ing:</strong> {$ing}<br>";
        
    if (preg_match($pattern2, $ing)) 
    {
        $replace1 = preg_replace($pattern2, $replacement, $ing);
  
        preg_match($pattern2, $ing, $matches);
//        echo "<pre>";
//        var_dump($matches);
//        echo "</pre>";
        
        $ingredientCount = $matches[0];
        
        //echo "count: {$count}<br>";
        
        //echo "Found<br>";
        //echo "<br><strong>replace:</strong> " . strtolower($replace1) . "<br>";
        
        foreach ($measurements as $measurement) {
            
            //echo "  measurement val: " . $measurement . "<br>";

            if (stripos(trim($replace1), trim(strtolower($measurement))) !== FALSE) {
                
                //echo "<strong>measurement found</strong><br>"; 
                
                //echo "new: " . str_ireplace(trim($measurement),"",$replace1) . "<br>";
                
                //echo "{$replace1} measurement: {$measurement}<br>"; 
                
                $measurementVal = $measurement;
                
                $val = str_ireplace(trim($measurement),"",$replace1);
                
                break;
            }
            else
            {
                $val = $replace1;
                $measurementVal = "";
            }
        }
        
    } else {
        
        //echo "Not found<br>";
        
        $measurementVal = "";
        
        if(!$ingredientCount)
        {
            $ingredientCount = 1;
        }
        
        $val = $ing;
    }
    
    echo "count: {$ingredientCount}<br>";
    echo "measurement: {$measurementVal}<br>";
    echo "<strong>ingredient:</strong> {$val}<br>";
}

?>