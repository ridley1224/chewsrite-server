// JavaScript Document

var recipeTitles = [];
var recipeSteps = [];
var recipeIngredients = [];
var recipeUnits = [];
var recipeAmounts = [];


$(document).ready(function () {

    //console.log("ready");
    
    //debugFields();

    
    $('#fileField').on('change', function () {        
        
        validateFileType();
    });

    $('#submitBtn').on('click', function () {

        console.log("submit form");
        
                
        var file_data = $('#fileField').prop('files')[0];

        if (file_data == null) {
            
            $('#submitStatus').html("Please select a recipe image");
            return;
        }        
        
        $('#submitStatus').html("");
        
        var ext = $('#fileField').val().split('.').pop().toLowerCase();
        
        var validImageTypes = ["mov", "mp4", "png", "jpg", "jpeg"];
        
        console.log("ext: "+ext);
                
        if ($.inArray(ext, validImageTypes) < 0) {
            
            console.log("invalid file type");

        // invalid file type code goes here.

        //$('#languageTxt').html("Invalid file type. Only audio formats .txt, .doc, .docx and .pdf files are accepted.");
        
        } else {

            submitForm();
        }


        //window.location.replace("recipeExtras.php?recipeid=24");
        //window.location.href = "recipeExtras.php?recipeid=24";        

    });

    $('#ingredientBtn').on('click', function () {

        if ($('#ingredientTxt').val() != "" && $('#amountTxt').val() != "") {
            console.log("add ingredient");

            var ingredientname = $('#ingredientTxt').val();
            var amount = $('#amountTxt').val();
            var unit = $('#units').val();

            var displayString = ingredientname + " " + amount + " " + unit;
            var displayStringHTML = "<div>" + ingredientname + " " + amount + " " + unit + "</div>";

            recipeIngredients.push(ingredientname);
            recipeAmounts.push(amount);
            recipeUnits.push(unit);

            $('#ingredientCell').append(displayStringHTML);

            $('#ingredientTxt').val("");
            $('#amountTxt').val("");
        }
    });

    $('#stepsBtn').on('click', function () {

        if ($('#stepsTxt').val() != "") {
            console.log("add step");

            var step = $('#stepsTxt').val();
            var title = $('#stepTxt').val();

            recipeSteps.push(step);
            recipeTitles.push(title);

            $('#stepsCell').append("<div><strong>Step " + recipeSteps.length + ": </strong>" + title + "<br>" + step + "</div>");

            $('#stepsTxt').val("");
            $('#stepTxt').val("");
            
            $('#submitStatus').html("");
        }
    });
})

function validateFileType () {
    
    var ext = $('#fileField').val().split('.').pop().toLowerCase();
    var validImageTypes = ["mov", "mp4", "png", "jpg", "jpeg"];

    console.log("ext: "+ext);

    if ($.inArray(ext, validImageTypes) < 0) {

        console.log("invalid file type");

        $('#fileStatus').html("Invalid file type");

        $('#fileField').val(''); 

    }
    else
    {
        $('#fileStatus').html("");
    }    
}

function submitForm() {
    
    var recipename = $('#recipename').val();
    var description = $('#description').val();

    var source = $('#source').val();
    var sourceurl = $('#sourceurl').val();
    var servings = $('#servings').val();

    var preptime = $('#preptime').val();
    var cooktime = $('#cooktime').val();

    var prepunits = $('#prepunits').val();
    var cookunits = $('#cookunits').val();

    var cuisinetags = $('#cuisinetags').val();
    var dietaryconcerns = $('#dietaryconcerntags').val();
    var userid = $('#userid').val();

    var isFeatured;

    if ($('#isFeatured').prop("checked") == false) {
        isFeatured = 0;
    } else {
        isFeatured = 1;
    }

    if (recipename == "") {
        console.log("need recipe name");
        return;
    }

    //        if(description == "")
    //        {
    //            console.log("need description name");
    //            return;
    //        }
    //        
    //        if(servings == "")
    //        {
    //            console.log("need servings");
    //            return;
    //        }
    //        
    //        if(preptime == "")
    //        {
    //            console.log("need preptime");
    //            return;
    //        }
    //        
    //        if(cooktime == "")
    //        {
    //            console.log("need cooktime");
    //            return;
    //        }


    console.log("recipename: " + recipename);
    console.log("description: " + description);

    console.log("source: " + source);
    console.log("sourceurl: " + sourceurl);
    console.log("servings: " + servings);

    console.log("preptime: " + preptime);
    console.log("cooktime: " + cooktime);

    console.log("prepunits: " + prepunits);
    console.log("cookunits: " + cookunits);

    console.log("isFeatured: " + isFeatured);
    console.log("cuisinetags: " + cuisinetags);
    console.log("dietaryconcerns: " + dietaryconcerns);

    
    console.log("recipeTitles: " + recipeTitles);
    console.log("recipeSteps: " + recipeSteps);
    console.log("recipeIngredients: " + recipeIngredients);


    //console.log("ext: " + ext);

    //console.log("client fileType: " + fileType);

    var file_data = $('#fileField').prop('files')[0];
    var form_data = new FormData();


    if (file_data != null) {
        form_data.append('file', file_data);
    }

    form_data.append('recipename', recipename);
    form_data.append('description', description);

    form_data.append('source', source);
    form_data.append('sourceurl', sourceurl);
    form_data.append('servings', servings);

    form_data.append('preptime', preptime + " " + prepunits);
    form_data.append('cooktime', cooktime + " " + cookunits);

    form_data.append('isFeatured', isFeatured);
    form_data.append('cuisinetags', cuisinetags);
    form_data.append('dietaryconcerntags', dietaryconcerns);

    
    
    form_data.append('recipeTitles', recipeTitles);
    form_data.append('recipeSteps', recipeSteps);
    form_data.append('recipeIngredients', recipeIngredients);
    form_data.append('recipeAmounts', recipeAmounts);
    form_data.append('recipeUnits', recipeUnits);

    form_data.append('userid', userid);

    //$('#uploadStatus').html("File sent");
    //$('#uploadStatus').html(file["name"] + ": transcription in progress");
    //$('#fileField').val('');

    //alert(form_data);   
    
    $("#submitBtn").attr("disabled", true);

    $.ajax({
        url: 'add-recipe.php', // point to server-side PHP script 
        dataType: 'text', // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        success: function (php_script_response) {

            console.log(php_script_response);

            var response = php_script_response;

            var obj = jQuery.parseJSON(php_script_response);

            //console.log("json: "+obj);
            //console.log("recipeStatus: "+obj.recipeStatus);

            if (obj.recipeStatus.includes("uploaded successfully") || obj.recipeStatus.includes("recipe saved successfully")) {
                //$('#uploadStatus').html(file["name"] + ": Complete");

                //alert("recipe saved");

                window.location.href = "edit-recipeNutritionDetails.php?recipeid=" + encodeURIComponent(obj.recipeid);
            }

            //alert(php_script_response); // display response from the PHP script, if any
        }
    });
}

function clearFields() {}

function debugFields() {

    console.log("debug fields");

    $('#recipename').val("test recipe");
    $('#description').val("test description");

    $('#source').val("RT");
    $('#sourceurl').val("http://rt.com");
    $('#servings').val("1");

    $('#preptime').val("1");
    $('#cooktime').val("30");

    
    $('#amountTxt').val("1");
    
    
//    $('#prepunits').val("min");
//    $('#cookunits').val("min");        
}


//https://stackoverflow.com/questions/29805909/jquery-how-to-check-if-uploaded-file-is-an-image-without-checking-extensions
