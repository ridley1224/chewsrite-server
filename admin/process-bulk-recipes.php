<?php

require("functions.php");

if(isset($_POST["submit"])) {

  $target_file = basename($_FILES["fileToUpload"]["name"]);
  $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

  if($imageFileType == "csv")
  {
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="disambiguation_test.csv"');

    $csvFile = $_FILES["fileToUpload"]["tmp_name"]; //'utterances.csv';

    $utteranceList = readCSV($csvFile);

    $user_CSV[0] = array('','Utterance', 'Intent', 'Disambiguate?', 'Response');

    // very simple to increment with i++ if looping through a database result

    $i = 0;
    $limit = $_POST["limit"];

    $workspace = "https://gateway.watsonplatform.net/conversation/api/v1/workspaces/{$_POST['workspace']}/message?version=2017-05-26";

    foreach ($utteranceList as $utterance)
    {
      if ($i < $limit)
      {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $workspace,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "{\"input\": {\"text\": \"{$utterance}\"},\"alternate_intents\": true}",
          CURLOPT_HTTPHEADER => array(
            "Authorization: Basic Nzc0M2Y5MzYtMDhmYi00ZjFiLTllZjktZjE0ZGE1MjFmOWM5OkoybDdmNkE3QUpDcA==",
            "Content-type: application/json",
            "Postman-Token: f5249426-5d62-4d01-bf7c-dcadfed09684",
            "cache-control: no-cache"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {

          echo "cURL Error #:" . $err;

        } else {

          $decodedData = json_decode($response);

          //check for disambiguation

          if($decodedData->context->first_intent_match)
          {
            $disambiguate = "yes";
            $outputString = "Would you like ".$decodedData->context->first_intent_match." or &".$decodedData->context->second_intent_match."?";
          }
          else
          {
            $disambiguate = "no";
            $outputString = $decodedData->output->text[0];
          }

          //populate row

          $ind = $i + 1;

          $user_CSV[$ind] = array($ind, $utterance, $decodedData->intents[0]->intent, $disambiguate, $outputString);
        }

        $i++; // increment limit counter

      } // end limit condition

    } //end loop

    $fp = fopen('php://output', 'w');

    foreach ($user_CSV as $line) {

        // though CSV stands for "comma separated value"
        // in many countries (including France) separator is ";"

        fputcsv($fp, $line, ',');
    }

    fclose($fp);

    $uploadOk = 1;
  }
  else {

    echo "Invalid file type. Please choose CSV file";

    $uploadOk = 0;
  }
}

?>


<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Untitled Document</title>
</head>

<body>
</body>
</html>