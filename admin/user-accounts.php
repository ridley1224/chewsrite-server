 <?php include("includes/devStatus.php"); 

require_once('../../Connections/chewsrite.php'); ?>
<?php /*?><?php include ("auth.php");?><?php */?>
<?php include ("../en-de.php");?>
<?php include ("../functions.php");?>
<?php

if(isset($_GET['deleteaccount']) && isset($_GET['userid']))
{
	//echo "<p>delete report: " . $_GET['userid'] . "</p>";
	
	$updateSQL = sprintf("UPDATE users SET deleted = %s WHERE userid = %s",
	GetSQLValueString(1, "text"),
	GetSQLValueString(de($_GET['userid']), "text"));
	
	mysql_select_db($database_chewsrite, $chewsrite);
	$Result1 = mysql_query($updateSQL, $chewsrite) or die(mysql_error());
}


$currentPage = $_SERVER["PHP_SELF"];

$maxRows_rsUserAccount = 25;
$pageNum_rsUserAccount = 0;
if (isset($_GET['pageNum_rsUserAccount'])) {
  $pageNum_rsUserAccount = $_GET['pageNum_rsUserAccount'];
}
$startRow_rsUserAccount = $pageNum_rsUserAccount * $maxRows_rsUserAccount;

mysql_select_db($database_chewsrite, $chewsrite);

$query_rsUserAccount = "SELECT * FROM users WHERE accountconfirmed = 1";

$query_limit_rsUserAccount = sprintf("%s LIMIT %d, %d", $query_rsUserAccount, $startRow_rsUserAccount, $maxRows_rsUserAccount);
$rsUserAccount = mysql_query($query_limit_rsUserAccount, $chewsrite) or die(mysql_error());
$row_rsUserAccount = mysql_fetch_assoc($rsUserAccount);

if (isset($_GET['totalRows_rsUserAccount'])) {
  $totalRows_rsUserAccount = $_GET['totalRows_rsUserAccount'];
} else {
  $all_rsUserAccount = mysql_query($query_rsUserAccount);
  $totalRows_rsUserAccount = mysql_num_rows($all_rsUserAccount);
}
$totalPages_rsUserAccount = ceil($totalRows_rsUserAccount/$maxRows_rsUserAccount)-1;

$queryString_rsUserAccount = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_rsUserAccount") == false && 
        stristr($param, "totalRows_rsUserAccount") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_rsUserAccount = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_rsUserAccount = sprintf("&totalRows_rsUserAccount=%d%s", $totalRows_rsUserAccount, $queryString_rsUserAccount);

mysql_select_db($database_chewsrite, $chewsrite);

?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>User Accounts</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>

    <?php include("includes/nav.php"); ?>
    
<h1>User Accounts</h1>
<table width="600" cellspacing="10">
  <tbody>
    
    <tr>
      <td>&nbsp;</td>
      <td><strong>Username</strong></td>
      <td><strong>Account Type</strong></td>
    </tr>
    
    <?php if($totalRows_rsUserAccount) { do { ?>
    
    <tr>
      
        <td width="75">
        <a href="view-account.php?userid=<?php echo urlencode(en($row_rsUserAccount['userid'])); ?>&usertype=<?php echo en($row_rsUserAccount['usertype']); ?>">
        <img src="../userimages/1.jpg" width="75" height="76"/>
        </a>
        </td>
        <td width="689">
        <a href="view-account.php?userid=<?php echo urlencode(en($row_rsUserAccount['userid'])); ?>&usertype=<?php echo en($row_rsUserAccount['usertype']); ?>"><?php echo $row_rsUserAccount['firstname'] . " " . $row_rsUserAccount['lastname']; ?></a></td>
        <td width="689"><?php 
		
		$accountstring;
	  
	if($row_rsUserAccount['usertype'] == 0)
	{
		$accountstring = "Manager";
	}
	else if($row_rsUserAccount['usertype'] == 1)
	{
		$accountstring = "Patron";
	}
	
	echo $accountstring; 
		
		
		?></td>
        
    </tr>
    <?php } while ($row_rsUserAccount = mysql_fetch_assoc($rsUserAccount)); } else { echo "No user accounts"; } ?>
    
    
    
    
  </tbody>
</table>
<p>&nbsp;

<?php if($totalRows_rsUserAccount) { ?>

  <?php if ($pageNum_rsUserAccount > 0) { // Show if not first page ?>
    <a href="<?php printf("%s?pageNum_rsUserAccount=%d%s", $currentPage, 0, $queryString_rsUserAccount); ?>">First</a>
    <?php } // Show if not first page ?>
  <?php if ($pageNum_rsUserAccount > 0) { // Show if not first page ?>
    <a href="<?php printf("%s?pageNum_rsUserAccount=%d%s", $currentPage, max(0, $pageNum_rsUserAccount - 1), $queryString_rsUserAccount); ?>">Previous</a>
    <?php } // Show if not first page ?>
  <?php if ($pageNum_rsUserAccount == 0) { // Show if first page ?>
      <a href="<?php printf("%s?pageNum_rsUserAccount=%d%s", $currentPage, min($totalPages_rsUserAccount, $pageNum_rsUserAccount + 1), $queryString_rsUserAccount); ?>">Next</a>
      <?php } // Show if first page ?>
  <?php if ($pageNum_rsUserAccount == 0) { // Show if first page ?>
    <a href="<?php printf("%s?pageNum_rsUserAccount=%d%s", $currentPage, $totalPages_rsUserAccount, $queryString_rsUserAccount); ?>">Last</a>
    <?php } // Show if first page ?>
    
    
    <?php } ?>
    
</p>
</body>
</html>
<?php
mysql_free_result($rsUserAccount);
?>
