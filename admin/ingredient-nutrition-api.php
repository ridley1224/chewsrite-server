<?php

$amt = $ingredientAmounts[ $ingredientInd ];
$ingredientStr = $amt . " " . $ingredientLower;
$ingredientParam = urlencode( $ingredientStr );

$curl = curl_init();


//"https://api.edamam.com/api/nutrition-data?app_id=8b8d9094&app_key=dee46f90cfca92d1791603df1c4c33fb&ingr=1%25cup%20broccoli"

curl_setopt_array( $curl, array(
    CURLOPT_URL => "https://api.edamam.com/api/nutrition-data?app_id=8b8d9094&app_key=dee46f90cfca92d1791603df1c4c33fb&ingr={$ingredientParam}",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
        "Accept: */*",
        "Accept-Encoding: gzip, deflate",
        "Cache-Control: no-cache",
        "Connection: keep-alive",
        "Host: api.edamam.com",
        "Postman-Token: c9b62d3c-6706-463e-99c1-79f63fe43ee6,650df75a-bc95-4073-a0ba-f6c815616d79",
        "User-Agent: PostmanRuntime/7.15.2",
        "cache-control: no-cache"
    ),
) );

$response = curl_exec( $curl );
$err = curl_error( $curl );

curl_close( $curl );

if ( $err ) {
    echo "cURL Error #:" . $err;
} else {
    //echo $response;

    $decodedData = json_decode( $response );

    $nutrientList = $decodedData->totalNutrients;

    // echo "<pre>";
    //   var_dump($nutrientList);
    //   echo "<pre>";

    $nutrientOb = new stdClass;
    $nutrientOb->ingredient = $ingredientLower;

    foreach ( $nutrientList as $nutrient ) {

        $label = $nutrient->label;
        $qty2 = $nutrient->quantity . " " . $nutrient->unit;

        $nutrientOb->$label = $qty2;
        $nutrientOb->amount = $amt;


        //echo "<strong>Nutrient:</strong> " . $nutrient->label . "<br><strong>Quantity:</strong> " .  $nutrient->quantity ."<br><strong>Unit:</strong> " .  $nutrient->unit . "<br><br>";


        //      [Energy] => 1137.92 kcal
        //            [amount] => 2 cups
        //            [Fat] => 89.6 g
        //            [Saturated] => 33.96288 g
        //            [Trans] => 5.2864 g
        //            [Monounsaturated] => 39.63904 g
        //            [Polyunsaturated] => 2.33408 g
        //            [Protein] => 76.9216 g
        //            [Cholesterol] => 318.08 mg
        //            [Sodium] => 295.68 mg
        //            [Calcium] => 80.64 mg
        //            [Magnesium] => 76.16 mg
        //            [Potassium] => 1209.6 mg
        //            [Iron] => 8.6912 mg
        //            [Zinc] => 18.7264 mg
        //            [Phosphorus] => 707.84 mg
        //            [Vitamin A] => 17.92 µg
        //            [Thiamin (B1)] => 0.19264 mg
        //            [Riboflavin (B2)] => 0.67648 mg
        //            [Niacin (B3)] => 18.93696 mg
        //            [Vitamin B6] => 1.44704 mg
        //            [Folate equivalent (total)] => 31.36 µg
        //            [Folate (food)] => 31.36 µg
        //            [Vitamin B12] => 9.5872 µg
        //            [Vitamin D] => 13.44 IU
        //            [Vitamin E] => 0.7616 mg
        //            [Vitamin K] => 8.064 µg
    }

    $nutrientObs[] = $nutrientOb;

    $Energy = explode( " ", $nutrientOb->Energy );
    //carb missing on some
    $Carbs = explode( " ", $nutrientOb->Carbs );
    $Protein = explode( " ", $nutrientOb->Protein );
    $Fat = explode( " ", $nutrientOb->Fat );
    $Saturated = explode( " ", $nutrientOb->Saturated );
    $Unsaturated = explode( " ", $nutrientOb->Monounsaturated );
    $Trans = explode( " ", $nutrientOb->Trans );
    $Cholesterol = explode( " ", $nutrientOb->Cholesterol );
    $Sodium = explode( " ", $nutrientOb->Sodium );
    $Potassium = explode( " ", $nutrientOb->Potassium );
    //fiber missing on some
    $Fiber = explode( " ", $nutrientOb->Fiber );
    //sugar missing on some
    $Sugars = explode( " ", $nutrientOb->Sugars );
    $VitaminA = explode( " ", $nutrientOb->Vitamin_A );
    $VitaminC = explode( " ", $nutrientOb->Vitamin_C );
    $Calcium = explode( " ", $nutrientOb->Calcium );
    $Iron = explode( " ", $nutrientOb->Iron );

    //echo "cals: {$cals[0]}<br>";

    $nutritionSQL = sprintf( "INSERT INTO recipenutrition  (Calories, Carbohydrates, Protein, Fat, SaturatedFat, UnsaturatedFat, TransFat, Cholestrol, Sodium, Potassium, Fiber, Sugars, VitaminA, VitaminC, Calcium, Iron, recipeid) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
        GetSQLValueString( $Energy[ 0 ], "text" ),
        GetSQLValueString( $Carbs[ 0 ], "text" ),
        GetSQLValueString( $Protein[ 0 ], "text" ),
        GetSQLValueString( $Fat[ 0 ], "text" ),
        GetSQLValueString( $Saturated[ 0 ], "text" ),
        GetSQLValueString( $Unsaturated[ 0 ], "text" ),
        GetSQLValueString( $Trans[ 0 ], "text" ),
        GetSQLValueString( $Cholesterol[ 0 ], "text" ),
        GetSQLValueString( $Sodium[ 0 ], "text" ),
        GetSQLValueString( $Potassium[ 0 ], "text" ),
        GetSQLValueString( $Fiber[ 0 ], "text" ),
        GetSQLValueString( $Sugars[ 0 ], "text" ),
        GetSQLValueString( $VitaminA[ 0 ], "text" ),
        GetSQLValueString( $VitaminC[ 0 ], "text" ),
        GetSQLValueString( $Calcium[ 0 ], "text" ),
        GetSQLValueString( $Iron[ 0 ], "text" ),
        GetSQLValueString( $last_id, "int" ) );

    mysql_select_db( $database_chewsrite, $chewsrite );
    //$ResultNutrition = mysql_query( $nutritionSQL, $chewsrite )or die( mysql_error() );
}

?>