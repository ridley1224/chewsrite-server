<?php 

//https://www.regextester.com/94462
//https://www.freeformatter.com/regex-tester.html

$str = "\"3/4 cup Farro\", \"1 Lemon\", \"1 Red Onion\", \"1 1/2 Cup Sugar Snap Peas\", \"3/4 Cup Mint\", \"2 Tbsp Red Wine Vinegar\", \"2 Tbsp Sugar\", \"1/2 Cup Kalamata Olives\", \"1/4 Cup Parmesan Cheese (grated)\", \"2 Tbsp Olive Oil\", \"1 Tsp Salt\", \"1 Tsp Black Pepper\"";

//echo $str;


$ingredients = str_getcsv( $str, ",", '"' );

//                                print "<pre>";
//                             print_r( $ingredients );
//                             print "</pre>";

$measurements = array('tbsp', 'tsp', 'cup', 'cups');


$pattern = '/\d{1,}\s/';
$pattern2 = '/[-]?[0-9]+[,.]?[0-9]*([\/][0-9]+[,.]?[0-9]*)*/';

$replacement = '$2';

foreach($ingredients as $ing)
{
    //echo "{$ing}<br>";
        
    if (preg_match($pattern2, $ing)) 
    {
        $replace1 = preg_replace($pattern2, $replacement, $ing);
        //echo "Found<br>";
        echo "<br><strong>replace:</strong> " . strtolower($replace1) . "<br>";
        
        foreach ($measurements as $measurement) {
            
            //echo "  measurement val: " . $measurement . "<br>";

            if (stripos(trim($replace1), trim(strtolower($measurement))) !== FALSE) {
                
                echo "<strong>measurement found</strong><br>"; 
                
                
                echo "new: " . str_replace(trim(strtolower($measurement)),"",strtolower($replace1)) . "<br>";
                
                
            }
        }
        
        
        
        
        
        
    } else {
        echo "Not found<br>";
    }
    
    
    
    //print_r($matches);
}



?>