 <?php include("includes/devStatus.php"); 

require_once('../../Connections/chewsrite.php'); 
include("functions.php");
include("auth.php");

$colname_rsRecipeDetails = "-1";
if (isset($_GET['reviewid'])) {
  $colname_rsRecipeDetails = de($_GET['reviewid']);
}

//echo "rev: {$_GET['reviewid']}<br>";
//echo "col: {$colname_rsRecipeDetails}<br>";

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
    
    
  $updateSQL = sprintf("UPDATE reviews SET `review`=%s WHERE reviewid=%s",
                       GetSQLValueString($_POST['review'], "text"),
                      GetSQLValueString(de($_POST['reviewid']), "int"));
    
    //echo "updateSQL: {$updateSQL}";

  mysql_select_db($database_chewsrite, $chewsrite);
  $Result1 = mysql_query($updateSQL, $chewsrite) or die(mysql_error());

  $updateGoTo = "edit-reviewDetails.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}


mysql_select_db($database_chewsrite, $chewsrite);
$query_rsRecipeDetails = sprintf("SELECT a.*, b.*, c.* FROM (SELECT * FROM reviews WHERE reviewid = %s) as a INNER JOIN (SELECT fullname,userid from users) as b on a.userid = b.userid INNER JOIN (SELECT imagename,recipename, recipeid from recipes) as c on a.recipeid = c.recipeid", GetSQLValueString($colname_rsRecipeDetails, "int"));


//echo $query_rsRecipeDetails;

$rsRecipeDetails = mysql_query($query_rsRecipeDetails, $chewsrite) or die(mysql_error());
$row_rsRecipeDetails = mysql_fetch_assoc($rsRecipeDetails);
$totalRows_rsRecipeDetails = mysql_num_rows($rsRecipeDetails);

$selectedcuisines = explode(",",$row_rsRecipeDetails['cuisinetags']);
$selecteddietaryConcerns = explode(",",$row_rsRecipeDetails['dietaryconcerntags']);

//mysql_select_db($database_chewsrite, $chewsrite);
//$query_rsSelectedCuisines = "SELECT * FROM cuisines WHERE cuisineid IN ({$row_rsRecipeDetails['cuisinetags']}) ";
//$rsSelectedCuisines = mysql_query($query_rsSelectedCuisines, $chewsrite) or die(mysql_error());
//$row_rsSelectedCuisines = mysql_fetch_assoc($rsSelectedCuisines);
//$totalRows_rsSelectedCuisines = mysql_num_rows($rsSelectedCuisines);

mysql_select_db($database_chewsrite, $chewsrite);
$query_rsSelectedCuisines = "SELECT * FROM cuisines";
$rsSelectedCuisines = mysql_query($query_rsSelectedCuisines, $chewsrite) or die(mysql_error());
$row_rsSelectedCuisines = mysql_fetch_assoc($rsSelectedCuisines);
$totalRows_rsSelectedCuisines = mysql_num_rows($rsSelectedCuisines);

$query_rsDietaryConcerns = "SELECT * FROM dietaryconcerns";
$rsDietaryConcerns = mysql_query($query_rsDietaryConcerns, $chewsrite) or die(mysql_error());
$row_rsDietaryConcerns = mysql_fetch_assoc($rsDietaryConcerns);
$totalRows_rsDietaryConcerns = mysql_num_rows($rsDietaryConcerns);
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Edit <?php echo $row_rsRecipeDetails['recipename']; ?></title>
<link href="admin.css" rel="stylesheet" type="text/css">
</head>

<body>
    
<?php include("includes/nav.php"); ?>

<h1>Edit Review</h1>
    
<form action="<?php echo $editFormAction; ?>" id="form1" name="form1" method="POST">

<p>&nbsp;</p>
<table width="600" cellspacing="5" class="table">
  <tbody>
    <tr>
      <td width="140"><strong><img src="https://s3-us-west-2.amazonaws.com/chewsrite/images/<?php echo $row_rsRecipeDetails['imagename']; ?>" alt="" width="80" height="80"/></strong></td>
      <td width="439">&nbsp;</td>
    </tr>
    <tr>
      <td><strong>Recipe</strong></td>
      <td class="cellTxt"><?php echo $row_rsRecipeDetails['recipename']; ?></td>
    </tr>
    <tr>
      <td><strong>Author</strong></td>
      <td><?php echo $row_rsRecipeDetails['fullname']; ?></td>
    </tr>
    <tr>
      <td><strong>Description</strong></td>
      <td><textarea name="review" class="description" id="review"><?php echo $row_rsRecipeDetails['review']; ?></textarea></td>
    </tr>
    <tr>
      <td><strong>Date Added</strong></td>
      <td><?php echo date("m-d-Y",strtotime($row_rsRecipeDetails['reviewdate'])); ?></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input type="submit" name="submit" id="submit" value="Submit">
        <input name="reviewid" type="hidden" id="reviewid" value="<?php echo en($colname_rsRecipeDetails); ?>"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><strong><a href="viewReviews.php">Back</a></strong></td>
      <td>&nbsp;</td>
    </tr>
  </tbody>
</table>
<input type="hidden" name="MM_update" value="form1">
    
    </form>
</body>
</html>
<?php
mysql_free_result($rsRecipeDetails);

mysql_free_result($rsSelectedCuisines);
?>
  