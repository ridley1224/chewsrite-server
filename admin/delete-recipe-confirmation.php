<?php

include("includes/devStatus.php"); 

require_once('../../Connections/chewsrite.php'); 
include("functions.php");
include("auth.php");

$colname_rsRecipeDetails = "-1";
if (isset($_GET['recipeid'])) {
  $colname_rsRecipeDetails = de($_GET['recipeid']);
}

mysql_select_db($database_chewsrite, $chewsrite);
$query_rsRecipeDetails = sprintf("SELECT a.*, b.* FROM (SELECT * FROM recipes WHERE recipeid = %s) as a INNER JOIN (SELECT fullname,userid from users) as b on a.userid = b.userid", GetSQLValueString($colname_rsRecipeDetails, "int"));

$rsRecipeDetails = mysql_query($query_rsRecipeDetails, $chewsrite) or die(mysql_error());
$row_rsRecipeDetails = mysql_fetch_assoc($rsRecipeDetails);
$totalRows_rsRecipeDetails = mysql_num_rows($rsRecipeDetails);

?>

<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Untitled Document</title>
</head>

<body>
<p>Delete recipe <strong><?php echo $row_rsRecipeDetails['recipename']?></strong>?</p>
<p><a href="delete-recipe.php?recipeid=<?php echo urlencode(en($colname_rsRecipeDetails)); ?>">Yes</a> <a href="edit-recipeDetails.php?recipeid=<?php echo urlencode(en($colname_rsRecipeDetails)); ?>">No</a></p>
</body>
</html>