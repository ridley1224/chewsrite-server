 <?php include("includes/devStatus.php"); 

require_once('../../Connections/chewsrite.php'); ?>
<?php /*?><?php include ("auth.php");?><?php */?>
<?php include ("../en-de.php");?>
<?php include ("../functions.php");?>

<?php

$colname_rsUserInfo = "-1";
if (isset($_GET['userid'])) {
  $colname_rsUserInfo = de($_GET['userid']);
}
mysql_select_db($database_chewsrite, $chewsrite);
$query_rsUserInfo = sprintf("SELECT * FROM users WHERE userid = %s", GetSQLValueString($colname_rsUserInfo, "int"));
$rsUserInfo = mysql_query($query_rsUserInfo, $chewsrite) or die(mysql_error());
$row_rsUserInfo = mysql_fetch_assoc($rsUserInfo);
$totalRows_rsUserInfo = mysql_num_rows($rsUserInfo);
?>

<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>View Post</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>

    <?php include("includes/nav.php"); ?>
    
<h1>Account Info</h1>
<table cellspacing="10">
  <tbody>
    <tr>
      <td>Name</td>
      <td><?php echo $row_rsUserInfo['firstname']; ?> <?php echo $row_rsUserInfo['lastname']; ?></td>
    </tr>
    <tr>
      <td>Email</td>
      <td><?php echo de($row_rsUserInfo['email']); ?></td>
    </tr>
    <tr>
      <td>Location</td>
      <td><?php echo $row_rsUserInfo['city']; ?> <?php echo $row_rsUserInfo['state']; ?> <?php echo $row_rsUserInfo['zip']; ?> <?php echo $row_rsUserInfo['country']; ?></td>
    </tr>
    
    <tr>
      <td>Account Type</td>
      <td><?php 
	  
	  $accountstring;
	  
	if($row_rsUserInfo['usertype'] == 0)
	{
		$accountstring = "Manager";
	}
	else if($row_rsUserInfo['usertype'] == 1)
	{
		$accountstring = "Patron";
	}
	
	echo $accountstring; ?></td>
    </tr>
    <tr>
      <td>Membership</td>
      <td><?php 
	  
	  /*$membershipString;
	  
	if($row_rsUserInfo['membership'] == 0)
	{
		$membershipString = "Free";
	}
	else if($row_rsUserInfo['membership'] == 1)
	{
		$membershipString = "Monthly";
	}
	else if($row_rsUserInfo['membership'] == 2)
	{
		$membershipString = "Annual";
	}*/
	  
		$membershipString = "Paid";  
	  
	  echo $membershipString; ?></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><a href="user-accounts.php">Back</a></td>
      <td>&nbsp;</td>
    </tr>
  </tbody>
</table>
<?php /*?><p><a href="user-accounts.php?deleteaccount=true&userid=<?php echo $_GET['userid']; ?>">Delete account</a></p><?php */?>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($rsUserInfo);
?>
