<?php 

include("includes/devStatus.php");

require_once('../../Connections/chewsrite.php'); 

include("functions.php");
include("auth.php");

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_rsRecipes = 20;
$pageNum_rsRecipes = 0;
if (isset($_GET['pageNum_rsRecipes'])) {
  $pageNum_rsRecipes = $_GET['pageNum_rsRecipes'];
}
$startRow_rsRecipes = $pageNum_rsRecipes * $maxRows_rsRecipes;

mysql_select_db($database_chewsrite, $chewsrite);
$query_rsRecipes = "SELECT * FROM recipes ORDER BY dateadded DESC";
$query_limit_rsRecipes = sprintf("%s LIMIT %d, %d", $query_rsRecipes, $startRow_rsRecipes, $maxRows_rsRecipes);
$rsRecipes = mysql_query($query_limit_rsRecipes, $chewsrite) or die(mysql_error());
$row_rsRecipes = mysql_fetch_assoc($rsRecipes);

if (isset($_GET['totalRows_rsRecipes'])) {
  $totalRows_rsRecipes = $_GET['totalRows_rsRecipes'];
} else {
  $all_rsRecipes = mysql_query($query_rsRecipes);
  $totalRows_rsRecipes = mysql_num_rows($all_rsRecipes);
}
$totalPages_rsRecipes = ceil($totalRows_rsRecipes/$maxRows_rsRecipes)-1;

$queryString_rsRecipes = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_rsRecipes") == false && 
        stristr($param, "totalRows_rsRecipes") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_rsRecipes = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_rsRecipes = sprintf("&totalRows_rsRecipes=%d%s", $totalRows_rsRecipes, $queryString_rsRecipes);
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>User Recipes</title>
<link href="admin.css" rel="stylesheet" type="text/css">
</head>

<body>



<?php include("includes/nav.php"); ?>


    
<p>&nbsp;</p>
<h1>Recipes</h1>
<p><a href="addRecipe.php">Add New Recipe</a><br>
<a href="bulk-addRecipes.php">Bulk Add Recipes </a><br>
<!--<a href="bulk-addRecipes-directions.php">Bulk Add Recipes Directions</a>--></p>
    
    <?php  if($totalRows_rsRecipes > 0 ) { ?>

<table width="100%" cellspacing="5" class="table">
    <tbody>
      <tr align="left">
        <th width="11%">&nbsp;</th>
        <th width="20%">Recipe</th>
        <th width="16%">Source</th>
        <th width="53%">Date</th>
      </tr>

   <?php do { ?>     
        
        
      <tr>
        <td><strong><img src="https://s3-us-west-2.amazonaws.com/chewsrite/images/<?php echo $row_rsRecipes['imagename']; ?>" alt="" width="80" height="80"/></strong></td>
        <td><a href="recipeDetails.php?recipeid=<?php echo urlencode(en($row_rsRecipes['recipeid'])); ?>"><?php echo $row_rsRecipes['recipename']; ?></a></td>
        <td><?php echo $row_rsRecipes['source']; ?></td>
        <td><?php echo date("m-d-Y",strtotime($row_rsRecipes['dateadded'])); ?></td>
      </tr>
        
    <?php } while ($row_rsRecipes = mysql_fetch_assoc($rsRecipes)); ?>

    </tbody>
</table>
  <p>&nbsp;
    <?php if ($pageNum_rsRecipes > 0) { // Show if not first page ?>
      <a href="<?php printf("%s?pageNum_rsRecipes=%d%s", $currentPage, 0, $queryString_rsRecipes); ?>">First</a> 
      <?php } // Show if not first page ?>
    <?php if ($pageNum_rsRecipes > 0) { // Show if not first page ?>
      <a href="<?php printf("%s?pageNum_rsRecipes=%d%s", $currentPage, max(0, $pageNum_rsRecipes - 1), $queryString_rsRecipes); ?>">Previous</a> 
      <?php } // Show if not first page ?>
    <?php if ($pageNum_rsRecipes < $totalPages_rsRecipes) { // Show if not last page ?>
  <a href="<?php printf("%s?pageNum_rsRecipes=%d%s", $currentPage, min($totalPages_rsRecipes, $pageNum_rsRecipes + 1), $queryString_rsRecipes); ?>">Next</a> 
  <?php } // Show if not last page ?>
    <?php if ($pageNum_rsRecipes < $totalPages_rsRecipes) { // Show if not last page ?>
      <a href="<?php printf("%s?pageNum_rsRecipes=%d%s", $currentPage, $totalPages_rsRecipes, $queryString_rsRecipes); ?>">Last</a> 
      <?php } // Show if not last page ?>
  </p>
    
    <?php } else {echo "No recipes";} // if reviews > 0 ?>
</body>
</html>
<?php
mysql_free_result($rsRecipes);
?>
        