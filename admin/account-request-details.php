 <?php include("includes/devStatus.php"); 

require_once('../../Connections/chewsrite.php'); ?>
<?php /*?><?php include ("auth.php");?><?php */?>
<?php include ("../en-de.php");?>
<?php include ("../functions.php");?>
<?php

$colname_rsPostInfo = "-1";
if (isset($_GET['userid'])) {
  $colname_rsPostInfo = de($_GET['userid']);
}
mysql_select_db($database_chewsrite, $chewsrite);
$query_rsPostInfo = sprintf("SELECT * FROM users WHERE userid = %s", GetSQLValueString($colname_rsPostInfo, "int"));
$rsPostInfo = mysql_query($query_rsPostInfo, $chewsrite) or die(mysql_error());
$row_rsPostInfo = mysql_fetch_assoc($rsPostInfo);
$totalRows_rsPostInfo = mysql_num_rows($rsPostInfo);
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Account Request Details</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>

    <?php include("includes/nav.php"); ?>
    
<table width="600" cellspacing="10">
  <tbody>
    <tr>
      <td><img src="../userimages/<?php echo $row_rsPostInfo['image']; ?>" alt=""/></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>Account Type:</td>
      <td><?php $accountstring;
	  
	if($row_rsPostInfo['usertype'] == "0")
	{
		$accountstring = "Manager";
	}
	else if($row_rsPostInfo['usertype'] == "1")
	{
		$accountstring = "Patron";
	}
	
	echo $accountstring; ?></td>
    </tr>
    <tr>
      <td>Username:</td>
      <td><?php echo $row_rsPostInfo['username']; ?></td>
    </tr>
    <tr>
      <td>First Name:</td>
      <td><?php echo $row_rsPostInfo['firstname']; ?></td>
    </tr>
    <tr>
      <td>Last Name:</td>
      <td><?php echo $row_rsPostInfo['lastname']; ?></td>
    </tr>
    <tr>
      <td>Email:</td>
      <td><?php echo $row_rsPostInfo['email']; ?></td>
    </tr>
    <tr>
      <td>Address:</td>
      <td><?php echo $row_rsPostInfo['address']; ?></td>
    </tr>
    <tr>
      <td>City:</td>
      <td><?php echo $row_rsPostInfo['city']; ?></td>
    </tr>
    <tr>
      <td>State:</td>
      <td><?php echo $row_rsPostInfo['state']; ?></td>
    </tr>
    <tr>
      <td>Zip:</td>
      <td><?php echo $row_rsPostInfo['zip']; ?></td>
    </tr>
    <tr>
      <td>Phone:</td>
      <td><?php echo $row_rsPostInfo['phone']; ?></td>
    </tr>
    <tr>
      <td>Occupation:</td>
      <td><?php echo $row_rsPostInfo['occupation']; ?></td>
    </tr>
    <tr>
      <td>College:</td>
      <td><?php echo $row_rsPostInfo['college']; ?></td>
    </tr>
    <tr>
      <td>Personality:</td>
      <td><?php echo $row_rsPostInfo['personality']; ?></td>
    </tr>
    <tr>
      <td>Social:</td>
      <td><?php echo $row_rsPostInfo['social']; ?></td>
    </tr>
    <tr>
      <td>Interests:</td>
      <td><?php echo $row_rsPostInfo['interests']; ?></td>
    </tr>
    <tr>
      <td>Art Interests:</td>
      <td><?php echo $row_rsPostInfo['artinterests']; ?></td>
    </tr>
    <tr>
      <td>Story:</td>
      <td><?php echo $row_rsPostInfo['story']; ?></td>
    </tr>
    <tr>
      <td>Submission Date:</td>
      <td><?php echo $row_rsPostInfo['datecreated']; ?></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><a href="accept-account.php?userid=<?php echo en($colname_rsPostInfo); ?>">Accept</a></td>
      <td>&nbsp;</td>
    </tr>
  </tbody>
</table>
<p><a href="account-requests.php">Back</a></p>
</body>
</html>
<?php
mysql_free_result($rsPostInfo);
?>
