<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "index.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php require_once('../../../Connections/prospectdugout.php'); ?>
<?php include ("../en-de.php");?>
<?php include ("../functions.php");?>
<?php

if(isset($_GET['deletereport']))
{
	//echo "<p>delete report: " . $_GET['postid'] . "</p>";
	
	$updateSQL = sprintf("UPDATE reportpost SET deleted = %s WHERE postid = %s",
	GetSQLValueString(1, "text"),
	GetSQLValueString(de($_GET['postid']), "text"));
	
	mysql_select_db($database_prospectdugout, $prospectdugout);
	$Result1 = mysql_query($updateSQL, $prospectdugout) or die(mysql_error());
}
else if(isset($_GET['remove']))
{
	//echo "<p>remove post: " . $_GET['postid'] . "</p>";	
	
	$updateSQL = sprintf("UPDATE posts SET deleted = %s WHERE postid = %s",
	GetSQLValueString(1, "text"),
	GetSQLValueString(de($_GET['postid']), "text"));
	
	mysql_select_db($database_prospectdugout, $prospectdugout);
	$Result1 = mysql_query($updateSQL, $prospectdugout) or die(mysql_error());
	
	$updateSQL = sprintf("UPDATE reportpost SET confirmed = %s WHERE postid = %s",
	GetSQLValueString(1, "text"),
	GetSQLValueString(de($_GET['postid']), "text"));
	
	mysql_select_db($database_prospectdugout, $prospectdugout);
	$Result1 = mysql_query($updateSQL, $prospectdugout) or die(mysql_error());
	
	// update posts set deleted = 1 where postid = 
	// update reports set confirmed = 1
	//send email to poster with delete notification
}


$currentPage = $_SERVER["PHP_SELF"];

$maxRows_rsReportPost = 25;
$pageNum_rsReportPost = 0;
if (isset($_GET['pageNum_rsReportPost'])) {
  $pageNum_rsReportPost = $_GET['pageNum_rsReportPost'];
}
$startRow_rsReportPost = $pageNum_rsReportPost * $maxRows_rsReportPost;

mysql_select_db($database_prospectdugout, $prospectdugout);

$query_rsReportPost = "SELECT a1.*,a2.* FROM (SELECT * FROM reportpost WHERE confirmed IS NULL AND deleted IS NULL 
ORDER BY reportdate DESC) as a1 
INNER JOIN (SELECT caption, image, userid, postid FROM posts WHERE deleted IS NULL) as a2 
ON a1.postid = a2.postid";

$query_limit_rsReportPost = sprintf("%s LIMIT %d, %d", $query_rsReportPost, $startRow_rsReportPost, $maxRows_rsReportPost);
$rsReportPost = mysql_query($query_limit_rsReportPost, $prospectdugout) or die(mysql_error());
$row_rsReportPost = mysql_fetch_assoc($rsReportPost);

if (isset($_GET['totalRows_rsReportPost'])) {
  $totalRows_rsReportPost = $_GET['totalRows_rsReportPost'];
} else {
  $all_rsReportPost = mysql_query($query_rsReportPost);
  $totalRows_rsReportPost = mysql_num_rows($all_rsReportPost);
}
$totalPages_rsReportPost = ceil($totalRows_rsReportPost/$maxRows_rsReportPost)-1;

$queryString_rsReportPost = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_rsReportPost") == false && 
        stristr($param, "totalRows_rsReportPost") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_rsReportPost = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_rsReportPost = sprintf("&totalRows_rsReportPost=%d%s", $totalRows_rsReportPost, $queryString_rsReportPost);

mysql_select_db($database_prospectdugout, $prospectdugout);

//$query_rsReportedPosts = "SELECT a1.*,a2.* FROM (SELECT * FROM reportpost ORDER BY reportdate DESC) as a1 INNER JOIN (SELECT caption, image, userid, postid FROM posts) as a2 ON a1.postid = a2.postid";



/*$rsReportedPosts = mysql_query($query_rsReportedPosts, $prospectdugout) or die(mysql_error());
$row_rsReportedPosts = mysql_fetch_assoc($rsReportedPosts);
$totalRows_rsReportedPosts = mysql_num_rows($rsReportedPosts);*/
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Reported Posts</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
    
    <?php include("includes/nav.php"); ?>
    
<h1>Reported Posts</h1>
<table width="600" cellspacing="10">
  <tbody>
    
    <?php if($totalRows_rsReportPost) { do { ?>
    <tr>
      
        <td width="75">
        <a href="viewPost.php?postid=<?php echo urlencode(en($row_rsReportPost['postid'])); ?>">
        <img src="../userimages/1.jpg" width="75" height="76"/>
        </a>
        </td>
        <td width="689">
        <a href="viewPost.php?postid=<?php echo urlencode(en($row_rsReportPost['postid'])); ?>"><?php echo $row_rsReportPost['caption']; ?></a></td>
        
    </tr>
    <?php } while ($row_rsReportPost = mysql_fetch_assoc($rsReportPost)); } else { echo "No reported posts"; } ?>
    
    
    
    
  </tbody>
</table>
<p>&nbsp;

<?php if($totalRows_rsReportPost) { ?>

  <?php if ($pageNum_rsReportPost > 0) { // Show if not first page ?>
    <a href="<?php printf("%s?pageNum_rsReportPost=%d%s", $currentPage, 0, $queryString_rsReportPost); ?>">First</a>
    <?php } // Show if not first page ?>
  <?php if ($pageNum_rsReportPost > 0) { // Show if not first page ?>
    <a href="<?php printf("%s?pageNum_rsReportPost=%d%s", $currentPage, max(0, $pageNum_rsReportPost - 1), $queryString_rsReportPost); ?>">Previous</a>
    <?php } // Show if not first page ?>
    <?php if ($pageNum_rsReportPost == 0) { // Show if first page ?>
      <a href="<?php printf("%s?pageNum_rsReportPost=%d%s", $currentPage, min($totalPages_rsReportPost, $pageNum_rsReportPost + 1), $queryString_rsReportPost); ?>">Next</a>
      <?php } // Show if first page ?>
    <?php if ($pageNum_rsReportPost == 0) { // Show if first page ?>
    <a href="<?php printf("%s?pageNum_rsReportPost=%d%s", $currentPage, $totalPages_rsReportPost, $queryString_rsReportPost); ?>">Last</a>
    <?php } // Show if first page ?>
    
    
    <?php } ?>
    
</p>
</body>
</html>
<?php
mysql_free_result($rsReportPost);

mysql_free_result($rsReportPost);

mysql_free_result($rsReportedPosts);
?>
