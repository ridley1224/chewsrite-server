 <?php include("includes/devStatus.php"); 

require_once('../../Connections/chewsrite.php'); 
include("functions.php");
include("auth.php");

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

$colname_rsRecipeDetails = "-1";
if (isset($_GET['recipeid'])) {
  $colname_rsRecipeDetails = de($_GET['recipeid']);
}

//echo "rev: {$_GET['recipeid']}<br>";
//echo "col: {$colname_rsRecipeDetails}<br>";

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
    
  $updateSQL = sprintf("UPDATE recipes SET recipename=%s, `description`=%s, source=%s, sourceurl=%s, servings=%s, preptime=%s, cooktime=%s, calories=%s, isFeatured=%s, videoname=%s, cuisinetags=%s, dietaryconcerntags=%s, dateadded=%s WHERE recipeid=%s",
                       GetSQLValueString($_POST['recipename'], "text"),
                       GetSQLValueString($_POST['description'], "text"),
                       GetSQLValueString($_POST['source'], "text"),
                       GetSQLValueString($_POST['sourceurl'], "text"),
                       GetSQLValueString($_POST['servings'], "text"),
                       GetSQLValueString($_POST['preptime'], "text"),
                       GetSQLValueString($_POST['cooktime'], "text"),
                       GetSQLValueString($_POST['calories'], "text"),
                       GetSQLValueString(isset($_POST['isFeatured']) ? "true" : "", "defined","1","0"),
                       GetSQLValueString($_POST['videoname'], "text"),
                       GetSQLValueString(implode(",",$_POST['cuisinetags']), "text"),
                       GetSQLValueString(implode(",",$_POST['dietaryconcerntags']), "text"),
                       GetSQLValueString($_POST['dateadded'], "date"),
                       GetSQLValueString(de($_POST['recipeid']), "int"));
    
    //echo "updateSQL: {$updateSQL}";

  mysql_select_db($database_chewsrite, $chewsrite);
  $Result1 = mysql_query($updateSQL, $chewsrite) or die(mysql_error());

  $updateGoTo = "edit-recipeDetails.php?recipeid={$colname_rsRecipeDetails}";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}


mysql_select_db($database_chewsrite, $chewsrite);
$query_rsRecipeDetails = sprintf("SELECT a.*, b.* FROM (SELECT * FROM recipes WHERE recipeid = %s) as a INNER JOIN (SELECT fullname,userid from users) as b on a.userid = b.userid", GetSQLValueString($colname_rsRecipeDetails, "int"));


//echo $query_rsRecipeDetails;

$rsRecipeDetails = mysql_query($query_rsRecipeDetails, $chewsrite) or die(mysql_error());
$row_rsRecipeDetails = mysql_fetch_assoc($rsRecipeDetails);
$totalRows_rsRecipeDetails = mysql_num_rows($rsRecipeDetails);


$query_rsNutritionDetails = sprintf("SELECT Calories FROM recipenutrition WHERE recipeid = %s", GetSQLValueString($colname_rsRecipeDetails, "int"));


$rsNutritionDetails = mysql_query($query_rsNutritionDetails, $chewsrite) or die(mysql_error());
$row_rsNutritionDetails = mysql_fetch_assoc($rsNutritionDetails);
$totalRows_rsNutritionDetails = mysql_num_rows($rsNutritionDetails);

$selectedcuisines = explode(",",$row_rsRecipeDetails['cuisinetags']);
$selecteddietaryConcerns = explode(",",$row_rsRecipeDetails['dietaryconcerntags']);

//mysql_select_db($database_chewsrite, $chewsrite);
//$query_rsSelectedCuisines = "SELECT * FROM cuisines WHERE cuisineid IN ({$row_rsRecipeDetails['cuisinetags']}) ";
//$rsSelectedCuisines = mysql_query($query_rsSelectedCuisines, $chewsrite) or die(mysql_error());
//$row_rsSelectedCuisines = mysql_fetch_assoc($rsSelectedCuisines);
//$totalRows_rsSelectedCuisines = mysql_num_rows($rsSelectedCuisines);

mysql_select_db($database_chewsrite, $chewsrite);
$query_rsSelectedCuisines = "SELECT * FROM cuisines WHERE active = 1";
$rsSelectedCuisines = mysql_query($query_rsSelectedCuisines, $chewsrite) or die(mysql_error());
$row_rsSelectedCuisines = mysql_fetch_assoc($rsSelectedCuisines);
$totalRows_rsSelectedCuisines = mysql_num_rows($rsSelectedCuisines);

//echo $query_rsSelectedCuisines . "<br>";

$query_rsDietaryConcerns = "SELECT * FROM dietaryconcerns WHERE active = 1";
$rsDietaryConcerns = mysql_query($query_rsDietaryConcerns, $chewsrite) or die(mysql_error());
$row_rsDietaryConcerns = mysql_fetch_assoc($rsDietaryConcerns);
$totalRows_rsDietaryConcerns = mysql_num_rows($rsDietaryConcerns);

//echo $query_rsDietaryConcerns;

?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Edit <?php echo $row_rsRecipeDetails['recipename']; ?></title>
<style type="text/css">

    
</style>
<link href="admin.css" rel="stylesheet" type="text/css">
</head>

<body>
    
<?php include("includes/nav.php"); ?>
    
<form action="<?php echo $editFormAction; ?>" id="form1" name="form1" method="POST">

<p>&nbsp;</p>

<h1>Edit Recipe</h1>

<table width="600" cellspacing="5" class="table">
  <tbody>
    <tr>
      <td width="140"><strong><img src="https://s3-us-west-2.amazonaws.com/chewsrite/images/<?php echo $row_rsRecipeDetails['imagename']; ?>" alt="" width="80" height="80"/></strong></td>
      <td width="439">&nbsp;</td>
    </tr>
    <tr>
      <td><strong>Recipe</strong></td>
      <td><input name="recipename" type="text" class="input" id="recipename" value="<?php echo $row_rsRecipeDetails['recipename']; ?>"></td>
    </tr>
    <tr>
      <td><strong>Author</strong></td>
      <td><?php echo $row_rsRecipeDetails['fullname']; ?></td>
    </tr>
    <tr>
      <td><strong>Description</strong></td>
      <td><textarea name="description" class="description" id="description"><?php echo $row_rsRecipeDetails['description']; ?></textarea></td>
    </tr>
    <tr>
      <td><strong>Source</strong></td>
      <td><input name="source" type="text" class="input" id="source" value="<?php echo $row_rsRecipeDetails['source']; ?>"></td>
    </tr>
    <tr>
      <td><strong>Source URL</strong></td>
      <td><input name="sourceurl" type="text" class="input" id="sourceurl" value="<?php echo $row_rsRecipeDetails['sourceurl']; ?>"></td>
    </tr>
    <tr>
      <td><strong>Servings</strong></td>
      <td><input name="servings" type="text" class="input" id="servings" value="<?php echo $row_rsRecipeDetails['servings']; ?>"></td>
    </tr>
    <tr>
      <td><strong>Prep Time</strong></td>
      <td><input name="preptime" type="text" class="input" id="preptime" value="<?php echo $row_rsRecipeDetails['preptime']; ?>"></td>
    </tr>
    <tr>
      <td><strong>Cook Time</strong></td>
      <td><input name="cooktime" type="text" class="input" id="cooktime" value="<?php echo $row_rsRecipeDetails['cooktime']; ?>"></td>
    </tr>
    <tr>
      <td><strong>Calories</strong></td>
      <td><?php echo $row_rsNutritionDetails['Calories']; ?></td>
    </tr>
    <tr>
      <td><strong>Nutrition Info</strong></td>
      <td><a href="edit-recipeNutritionDetails.php?recipeid=<?php echo en($colname_rsRecipeDetails); ?>">View nutrition info</a></td>
    </tr>
    <tr>
      <td><strong>Featured Recipe?</strong></td>
      <td><?php 
          
            if ($row_rsRecipeDetails['isFeatured'] == 1)
            {
                $checked = " checked";
            }
            else
            {
                $checked = "";
            }
          
        ?>
        <input name="isFeatured" type="checkbox" id="isFeatured" <?php echo $checked ?>></td>
    </tr>
    <tr>
      <td><strong>Has Video?</strong></td>
      <td><?php echo $row_rsRecipeDetails['videoname']; ?></td>
    </tr>
    <tr>
      <td><strong>Cuisines</strong></td>
      <td><select name="cuisinetags[]" multiple="MULTIPLE" class="list" id="cuisinetags">

          <?php do { 
          
            if (in_array($row_rsSelectedCuisines['cuisineid'],$selectedcuisines))
            {
              $selection = " selected";
            }
            else
            {
                $selection = "";
            }
          
          ?>        
          
        <option value="<?php echo $row_rsSelectedCuisines['cuisineid']; ?>" <?php echo $selection ?>><?php echo $row_rsSelectedCuisines['cuisinename'];?></option>
          
        <?php } while ($row_rsSelectedCuisines = mysql_fetch_assoc($rsSelectedCuisines)); ?>
          
      </select></td>
    </tr>
    <tr>
      <td><strong>Dietary Concerns</strong></td>
      <td><select name="dietaryconcerntags[]" multiple="MULTIPLE" class="list" id="dietaryconcerntags">
          
          <?php do { 
          
            if (in_array($row_rsDietaryConcerns['concernid'],$selecteddietaryConcerns))
            {
              $selection = " selected";
            }
            else
            {
                $selection = "";
            }
          
          ?>          
          
        <option value="<?php echo $row_rsDietaryConcerns['concernid']; ?>" <?php echo $selection ?>><?php echo $row_rsDietaryConcerns['concernname'];?></option>
          
        <?php } while ($row_rsDietaryConcerns = mysql_fetch_assoc($rsDietaryConcerns)); ?>
          
      </select></td>
    </tr>
    <tr>
      <td><strong>Date Added</strong></td>
      <td><input name="dateadded" type="text" class="input" id="dateadded" value="<?php echo $row_rsRecipeDetails['dateadded']; ?>"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input type="submit" name="submit" id="submit" value="Submit">
        <input name="recipeid" type="hidden" id="recipeid" value="<?php echo en($colname_rsRecipeDetails); ?>"></td>
    </tr>
    <tr>
      <td><a href="delete-recipe-confirmation.php?recipeid=<?php echo urlencode(en($colname_rsRecipeDetails)); ?>">Delete recipe</a></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><strong><a href="recipeDetails.php?recipeid=<?php echo urlencode(en($colname_rsRecipeDetails)); ?>">Back</a></strong></td>
      <td>&nbsp;</td>
    </tr>
  </tbody>
</table>
<input type="hidden" name="MM_update" value="form1">
    
    </form>
</body>
</html>
<?php
mysql_free_result($rsRecipeDetails);

mysql_free_result($rsSelectedCuisines);
?>
  