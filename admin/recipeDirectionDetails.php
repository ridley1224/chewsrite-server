 <?php include("includes/devStatus.php"); 

require_once('../../Connections/chewsrite.php'); 
include("functions.php");
include("auth.php");

//$colname_rsNutritionDetails = "7";

if (isset($_GET['recipeid'])) {
  $colname_rsNutritionDetails = de($_GET['recipeid']);
}

mysql_select_db($database_chewsrite, $chewsrite);

$query_rsRecipeDetails = sprintf("SELECT recipename FROM recipes WHERE recipeid = %s", GetSQLValueString($colname_rsNutritionDetails, "int"));
$rsRecipeDetails = mysql_query($query_rsRecipeDetails, $chewsrite) or die(mysql_error());
$row_rsRecipeDetails = mysql_fetch_assoc($rsRecipeDetails);
$totalRows_rsRecipeDetails = mysql_num_rows($rsRecipeDetails);

//echo $query_rsRecipeDetails;

$query_rsNutritionDetails = sprintf("SELECT * FROM recipedirections WHERE recipeid = %s", GetSQLValueString($colname_rsNutritionDetails, "int"));
$rsNutritionDetails = mysql_query($query_rsNutritionDetails, $chewsrite) or die(mysql_error());
$row_rsNutritionDetails = mysql_fetch_assoc($rsNutritionDetails);
$totalRows_rsNutritionDetails = mysql_num_rows($rsNutritionDetails);
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Nutrition Details</title>
<link href="admin.css" rel="stylesheet" type="text/css">
</head>

<body>

<?php include("includes/nav.php"); ?>
    
<p>&nbsp;</p>

<h1>Recipe Directions</h1>

<table width="600" cellspacing="5" class="table">
  <tbody>
      
    <?php 
      
      if($totalRows_rsNutritionDetails > 0) {
      
      $i = 1; do { ?> 

    <tr>
      <td width="134"><strong>Step <?php echo $i; ?></strong></td>
      <td width="445"><?php echo $row_rsNutritionDetails['title']; ?><br><?php echo $row_rsNutritionDetails['directions']; ?></td>
    </tr>
      
    <?php 

$i++;

} while ($row_rsNutritionDetails = mysql_fetch_assoc($rsNutritionDetails)); } ?>
      
    <?php /*?><tr>
      <td><a href="edit-recipeNutritionDetails.php?recipeid=<?php echo en($colname_rsNutritionDetails); ?>">Edit</a></td>
      <td>&nbsp;</td>
    </tr><?php */?>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><a href="edit-recipeDirectionDetails.php?recipeid=<?php echo urlencode(en($colname_rsNutritionDetails)); ?>">Edit</a></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><a href="recipeDetails.php?recipeid=<?php echo urlencode(en($colname_rsNutritionDetails)); ?>">Back</a></td>
      <td>&nbsp;</td>
    </tr>
  </tbody>
</table>
</body>
</html>
<?php
mysql_free_result($rsNutritionDetails);
?>
  