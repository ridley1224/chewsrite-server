<?php

header('Content-type: application/json');

require_once('../../Connections/chewsrite.php');
include("functions.php");

mysql_select_db($database_chewsrite, $chewsrite);

//$response = ["URLOfTheSecondWebsite" => $request['websites'][1]['URL']];

$string;
$date = date("Y-m-d H:i:s");

//$_POST['userid'] = "1";
//$_POST['recipeid'] = "1";
//$_POST['mealid'] = "0";
//$_POST['mealdate'] = "2018-08-04";
//$_POST['originalrecipeid'] = "2";
//$_POST['replace'] = true;

$object = new stdClass();
$object->status = "meal plan item not saved";


if(isset($_POST['replace']) && isset($_POST['originalrecipeid']))
{
	$date2 = $_POST['mealdate'] . " 00:00:00";
	
	$insertSQL = sprintf("UPDATE mealplans SET recipeid = %s WHERE mealid = %s AND userid = %s AND mealplandate = %s AND recipeid = %s",
						GetSQLValueString(mysql_real_escape_string($_POST['recipeid']), "int"),
						GetSQLValueString(mysql_real_escape_string($_POST['mealid']), "int"),
						GetSQLValueString(mysql_real_escape_string($_POST['userid']), "int"),
						GetSQLValueString(mysql_real_escape_string($date2), "date"),
						GetSQLValueString(mysql_real_escape_string($_POST['originalrecipeid']), "int"));
	
	mysql_select_db($database_chewsrite, $chewsrite);
	$Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());
	
	$object->status = "meal plan recipe replaced";
}
else if(isset($_POST['userid']) && isset($_POST['recipeid']) && isset($_POST['mealid']) && isset($_POST['insert']))
{
	$insertSQL = sprintf("INSERT INTO mealplans (recipeid, mealid, userid, mealplandate, datecreated) VALUES (%s, %s, %s, %s, %s)",
					GetSQLValueString(mysql_real_escape_string($_POST['recipeid']), "int"),
					GetSQLValueString(mysql_real_escape_string($_POST['mealid']), "int"),
					GetSQLValueString(mysql_real_escape_string($_POST['userid']), "int"),
					GetSQLValueString(mysql_real_escape_string($_POST['mealdate']), "date"),
					GetSQLValueString(mysql_real_escape_string($date), "date"));

	mysql_select_db($database_chewsrite, $chewsrite);
	$Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());	

	$last_id = mysql_insert_id();	
	$object->recordid = $last_id;
	$object->status = "meal plan item saved";	
}
else if(isset($_POST['mealplanid']) && isset($_POST['delete']))
{
	$insertSQL = sprintf("DELETE FROM mealplans WHERE mealplanid = %s",
				GetSQLValueString(mysql_real_escape_string($_POST['mealplanid']), "text"));

	mysql_select_db($database_chewsrite, $chewsrite);
	$Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());	

	$object->status = "meal plan recipe deleted";
}
else if(isset($_POST['mealplanid']) && isset($_POST['update']))
{
	$insertSQL = sprintf("UPDATE mealplans SET mealplandate = %s, mealid = %s WHERE mealplanid = %s",
					GetSQLValueString(mysql_real_escape_string($_POST['mealdate']), "date"),
					GetSQLValueString(mysql_real_escape_string($_POST['mealid']), "int"),
					GetSQLValueString(mysql_real_escape_string($_POST['mealplanid']), "int"));

	mysql_select_db($database_chewsrite, $chewsrite);
	$Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());	

	$last_id = mysql_insert_id();	
	$object->recordid = $last_id;
	$object->status = "meal plan item updated";
}


if (isset($_POST['family']) && ($_POST['family'] == "1" || $_POST['family'] == "true"))
{
    //send notification to family members
    
    $query_rsSender = "SELECT firstname FROM users WHERE userid = {$_POST['userid']}";

	$rsSender = mysql_query($query_rsSender, $chewsrite) or die(mysql_error());
	$row_rsSender = mysql_fetch_assoc($rsSender);    
    
    
    $query_rsInvites = "SELECT a.userid,b.deviceid FROM (SELECT userid FROM familymembers WHERE userid = {$_POST['userid']}) as a INNER JOIN (SELECT * FROM users WHERE deviceid IS NOT NULL) as b ON a.userid = b.userid";

	$rsInvites = mysql_query($query_rsInvites, $chewsrite) or die(mysql_error());
	$row_rsInvites = mysql_fetch_assoc($rsInvites);
	$totalRows_rsInvites = mysql_num_rows($rsInvites);
	
		
	if($totalRows_rsInvites > 0)
	{
		do {
            
            $message = "{$row_rsInvites['firstname']}, you've been invited to view {$row_rsSender['firstname']}% Meal Plan.";
            $url = "http://chewsrite.com";
            //$body = $_POST['groupid'];
						
			$deviceToken = $row_rsInvites['deviceid'];
			$_POST['token'] = $row_rsInvites['deviceid'];

			$body['aps'] = array(
					  'alert' => $message,
					  'sound' => 'default',
					  'link_url' => $url,
					  'category' => "MEAL_PLAN_INVITE",
					  'body' => $_POST['mealplanid'],
					  'badge' => 1,
					  );

			include("send-push-notification.php");

		}  while ($row_rsInvites = mysql_fetch_assoc($rsInvites));
	}
}


echo "{\"data\":";
echo "{\"mealPlanData\":";
echo json_encode( $object );
echo "}";
echo "}";

?>
