<?php

require_once('../../Connections/chewsrite.php');
include("functions.php");

mysql_select_db($database_chewsrite, $chewsrite);

$date = date("Y-m-d H:i:s");

//$_POST['groupname'] = "2";
//$_POST['groupdescription'] = "2";
//$_POST['userid'] = "2";
//$_POST['imagename'] = "2";

$object = new stdClass();
$object->status = "group not saved";

if(isset($_POST['groupname']) && isset($_POST['userid']) && isset($_POST['insert']))
{
    if(isset($_POST['isFamilyMember']) && $_POST['isFamilyMember'] == "true")
    {
        $field = "familyid";
    }
    else
    {
        $field = "userid";
    }
    
	$insertSQL = sprintf("INSERT INTO groups(groupname, groupdescription, {$field}, groupimage, datecreated) VALUES (%s, %s, %s, %s, %s)",
				GetSQLValueString(mysql_real_escape_string($_POST['groupname']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['groupdescription']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['userid']), "int"),
				GetSQLValueString(mysql_real_escape_string($_POST['imagename']), "text"),
				GetSQLValueString(mysql_real_escape_string($date), "date"));

	mysql_select_db($database_chewsrite, $chewsrite);
	$Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());	

	$last_id = mysql_insert_id();	
	$object->groupid = $last_id;
    $object->query = json_encode($insertSQL);
	$object->status = "group saved";
    
	
	$insertSQL2 = sprintf("INSERT INTO groupmembers(userid, groupid, dateadded) VALUES (%s, %s, %s)",
				GetSQLValueString(mysql_real_escape_string($_POST['userid']), "text"),
				GetSQLValueString(mysql_real_escape_string($last_id), "text"),
				GetSQLValueString(mysql_real_escape_string($date), "date"));

	mysql_select_db($database_chewsrite, $chewsrite);
	$Result2 = mysql_query($insertSQL2, $chewsrite) or die(mysql_error());	
    
    $insertSQL2 = sprintf("INSERT INTO groupnotificationsettings(userid, groupid, selection, datecreated) VALUES (%s, %s, %s, %s)",
				GetSQLValueString(mysql_real_escape_string($_POST['userid']), "int"),
				GetSQLValueString(mysql_real_escape_string($last_id), "int"),
                GetSQLValueString(0, "int"),
				GetSQLValueString(mysql_real_escape_string($date), "date"));

	mysql_select_db($database_chewsrite, $chewsrite);
	$Result2 = mysql_query($insertSQL2, $chewsrite) or die(mysql_error());	

	$last_id = mysql_insert_id();	
}
else if(isset($_POST['groupname']) && isset($_POST['groupid']) && isset($_POST['update']))
{
    $updateSQL = sprintf("UPDATE groups SET groupname=%s, groupdescription=%s WHERE groupid = %s ",
                        GetSQLValueString(mysql_real_escape_string($_POST['groupname']), "text"),
				        GetSQLValueString(mysql_real_escape_string($_POST['groupdescription']), "text"),
                        GetSQLValueString(mysql_real_escape_string($_POST['groupid']), "int"));

    mysql_select_db($database_chewsrite, $chewsrite);
    $Result1 = mysql_query($updateSQL, $chewsrite) or die(mysql_error());	
    $object->status = "group updated";
}
else if(isset($_POST['groupname']) && isset($_POST['groupid']) && isset($_POST['delete']))
{
    $updateSQL = sprintf("DELETE FROM groups WHERE groupid = %s ",
                        GetSQLValueString(mysql_real_escape_string($_POST['groupid']), "int"));

    mysql_select_db($database_chewsrite, $chewsrite);
    $Result1 = mysql_query($updateSQL, $chewsrite) or die(mysql_error());	
    $object->status = "group deleted";
    
    //send push
    
    //$query_rsInvites = "SELECT userid FROM groupmembers WHERE groupid = {$_POST['groupid']}";
    
    //randall to do debug this later for familymembers when they have deviceids
    
    $query_rsInvites = "SELECT a.userid,b.deviceid FROM (SELECT userid FROM groupmembers WHERE groupid = {$_POST['groupid']}) as a INNER JOIN (SELECT * FROM users WHERE deviceid IS NOT NULL) as b ON a.userid = b.userid";

	$rsInvites = mysql_query($query_rsInvites, $chewsrite) or die(mysql_error());
	$row_rsInvites = mysql_fetch_assoc($rsInvites);
	$totalRows_rsInvites = mysql_num_rows($rsInvites);
	
	$message = "Uh oh..it looks like one of the groups " . strtoupper($_POST['groupname']) . " you are part of has been deleted.";
	$url = "http://chewsrite.com";
	//$body = $_POST['groupid'];
	
	
	if($totalRows_rsInvites)
	{
		do {
						
			$deviceToken = $row_rsInvites['deviceid'];
			$_POST['token'] = $row_rsInvites['deviceid'];

			$body['aps'] = array(
					  'alert' => $message,
					  'sound' => 'default',
					  'link_url' => $url,
					  'category' => "GROUP_INVITE",
					  'body' => $_POST['groupid'],
					  'badge' => 1,
					  );

			include("send-push-notification.php");

		}  while ($row_rsInvites = mysql_fetch_assoc($rsInvites));
	}
}

//to do randall

//send invites

if(isset($_POST['invitelist']))
{
    //send push
}

echo "{\"data\":";
echo "{\"groupData\":";
echo json_encode( $object );
echo "}";
echo "}";

?>
        