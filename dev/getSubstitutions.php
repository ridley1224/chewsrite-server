<?php require_once('../../Connections/chewsrite.php'); ?>
<?php

include("functions.php");
include("en-de.php");

$date = date("Y-m-d H:i:s");

mysql_select_db($database_chewsrite, $chewsrite);


//$_POST['ingredientname'] = "Quinoa";

if(isset($_POST['ingredientname']))
{
    //$_POST['ingredientname'] = "Black Pepper";

    $ingredientname = strtolower($_POST['ingredientname']);


    $query_rsIngredientInfo = "SELECT * from ingredients WHERE LOWER(ingredient) = '" . $ingredientname . "'";

    //echo $query_rsIngredientInfo;

    $rsIngredientInfo = mysql_query($query_rsIngredientInfo, $chewsrite) or die(mysql_error());
    $row_rsIngredientInfo = mysql_fetch_assoc($rsIngredientInfo);
    $totalRows_rsIngredientInfo = mysql_num_rows($rsIngredientInfo);

    if($totalRows_rsIngredientInfo > 0)
    {
        $ingredientid = $row_rsIngredientInfo['ingredientid'];

        $object = new stdClass();
        $object->ingredientid = $ingredientid;

        //echo "ingredientid: {$ingredientid }<br>";

        $query_rsSubstitutionInfo = "SELECT * FROM substitutions WHERE ingredientid = {$ingredientid}";

        $rsSubstitutionInfo = mysql_query($query_rsSubstitutionInfo, $chewsrite) or die(mysql_error());
        $row_rsSubstitutionInfo = mysql_fetch_assoc($rsSubstitutionInfo);
        $totalRows_rsSubstitutionInfo = mysql_num_rows($rsSubstitutionInfo);

        if($totalRows_rsSubstitutionInfo > 0)
        {
            $object->source = "DB";
            // we have substitution data

            //echo "has substitutions<br>";

            //$object = new stdClass();

            do {		

                //$object->substitution = $row_rsSubstitutionInfo['substitution'];
        //		$object->eventname = $row_rsSubstitutionInfo['eventname'];
        //		$object->city = $row_rsSubstitutionInfo['city'];
        //		$object->state = $row_rsSubstitutionInfo['state'];
        //		$object->zip = $row_rsSubstitutionInfo['zip'];
        //		$object->openseats = $row_rsSubstitutionInfo['availableseats'];
        //		$object->totalseats = $row_rsSubstitutionInfo['totalseats'];
        //		$object->eventcost = $row_rsSubstitutionInfo['eventcost'];
        //		$object->eventdate = $row_rsSubstitutionInfo['eventdate'];
        //		$object->venueid = $row_rsSubstitutionInfo['venueid'];


                if($row_rsSubstitutionInfo['substitution'] != "Could not find any substitutes for that ingredient.")
                {
                    $object1 = new stdClass();
                    $object1->ingredientname = $row_rsSubstitutionInfo['substitution'];
                    $object1->subid = $row_rsSubstitutionInfo['subid'];


                    //$object->substitutions[] = $row_rsSubstitutionInfo['substitution'];
                    $object->substitutions[] = $object1;
                }

            }  while ($row_rsSubstitutionInfo = mysql_fetch_assoc($rsSubstitutionInfo));
        }
        else
        {
            //substitutions not currently in DB, query API

            //echo "query API<br>";

            $object->source = "API";

            if(0 == 0)
            {
                $curl = curl_init();

                curl_setopt_array($curl, array(
                  CURLOPT_URL => "https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/food/ingredients/substitutes?ingredientName={$ingredientname}",
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => "",
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 30,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_CUSTOMREQUEST => "GET",
                  CURLOPT_POSTFIELDS => "",
                  CURLOPT_HTTPHEADER => array(
                    "Postman-Token: a4211257-4db8-4115-80bb-3a3f63ea21c6",
                    "X-Mashape-Host: spoonacular-recipe-food-nutrition-v1.p.mashape.com",
                    "X-Mashape-Key: A6jIcNophWmshQEMCrJucq0gQW1xp1oyts4jsnqkBJhn4r3Iu3",
                    "cache-control: no-cache"
                  ),
                ));

                $response = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);

                if ($err) {

                  echo "cURL Error #:" . $err;

                } else {

                  //echo "<p>response: {$response}</p>";

                    $json = json_decode($response);

                    //$string = "{\"status\":\"success\",\"ingredient\":\"butter\",\"substitutes\":[\"1 cup = 7/8 cup shortening and 1/2 tsp salt\",\"1 cup = 7/8 cup vegetable oil + 1/2 tsp salt\",\"1/2 cup = 1/4 cup buttermilk + 1/4 cup unsweetened applesauce\",\"1 cup = 1 cup margarine\"],\"message\":\"Found 4 substitutes for the ingredient.\"}";


                    $insertSQL = sprintf("INSERT INTO substitutionAPI (ingredientname, response, createddate) VALUES (%s, %s, %s)",
                            GetSQLValueString(mysql_real_escape_string($_POST['ingredientname']), "text"),
                            GetSQLValueString(mysql_real_escape_string($response), "text"),
                            GetSQLValueString(mysql_real_escape_string($date), "date"));

                    mysql_select_db($database_chewsrite, $chewsrite);
                    $Result = mysql_query($insertSQL, $chewsrite) or die(mysql_error());


                    if(!$json->message)
                    {
                        //echo "no response<br>";

                        $insertSQL2 = sprintf("INSERT INTO substitutions (substitution, ingredientid, createddate) VALUES (%s, %s, %s)",
                                GetSQLValueString(mysql_real_escape_string("Could not find any substitutes for that ingredient."), "text"),
                                GetSQLValueString(mysql_real_escape_string($object->ingredientid), "text"),
                                GetSQLValueString(mysql_real_escape_string($date), "date"));

                                mysql_select_db($database_chewsrite, $chewsrite);
                                $Result2 = mysql_query($insertSQL2, $chewsrite) or die(mysql_error());
                    }
                    else
                    {
                        if($json->message != "Could not find any substitutes for that ingredient.")
                        {
                            //echo "has subs from API<br>";
                            $substitutes = $json->substitutes;

                            foreach($substitutes as $sub)
                            {       
                                //echo "sub: {$sub}<br>";

                                $insertSQL2 = sprintf("INSERT INTO substitutions (substitution, ingredientid, createddate) VALUES (%s, %s, %s)",
                                GetSQLValueString(mysql_real_escape_string($sub), "text"),
                                GetSQLValueString(mysql_real_escape_string($ingredientid), "text"),
                                GetSQLValueString(mysql_real_escape_string($date), "date"));

                                mysql_select_db($database_chewsrite, $chewsrite);
                                $Result2 = mysql_query($insertSQL2, $chewsrite) or die(mysql_error());

                                $object->substitutions[] = $sub;
                            }   
                        }
                        else
                        {
                            $insertSQL2 = sprintf("INSERT INTO substitutions (substitution, ingredientid, createddate) VALUES (%s, %s, %s)",
                                GetSQLValueString(mysql_real_escape_string($json->message), "text"),
                                GetSQLValueString(mysql_real_escape_string($object->ingredientid), "text"),
                                GetSQLValueString(mysql_real_escape_string($date), "date"));

                                mysql_select_db($database_chewsrite, $chewsrite);
                                $Result2 = mysql_query($insertSQL2, $chewsrite) or die(mysql_error());
                        }
                    }   
                }
            }    
        }
    } 
    else
    {
        //insert ingredient?

        //for now just return none

        $insertSQL2 = sprintf("INSERT INTO substitutions (substitution, ingredientid, createddate) VALUES (%s, %s, %s)",
                            GetSQLValueString(mysql_real_escape_string("Could not find any substitutes for that ingredient."), "text"),
                            GetSQLValueString(mysql_real_escape_string($object->ingredientid), "text"),
                            GetSQLValueString(mysql_real_escape_string($date), "date"));

        mysql_select_db($database_chewsrite, $chewsrite);
        $Result2 = mysql_query($insertSQL2, $chewsrite) or die(mysql_error());
    }
}


	
echo "{\"data\":";
echo "{\"substitutesData\":";
echo json_encode( $object );
echo "}";
echo "}";


?>