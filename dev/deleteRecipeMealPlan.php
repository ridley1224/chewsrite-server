<?php

header('Content-type: application/json');

require_once('../../Connections/chewsrite.php');
include("functions.php");

mysql_select_db($database_chewsrite, $chewsrite);

$string;
$object = new stdClass();
$object->status = "meal plan not deleted";

//$_POST['mealplanid'] = "11";

if(isset($_POST['mealplanid']))
{
	$insertSQL = sprintf("DELETE FROM mealplans WHERE mealplanid = %s",
				GetSQLValueString(mysql_real_escape_string($_POST['mealplanid']), "text"));

	mysql_select_db($database_chewsrite, $chewsrite);
	$Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());	

	$object->status = "meal plan recipe deleted";
}

echo "{\"data\":";
echo "{\"deleteData\":";
echo json_encode( $object );
echo "}";
echo "}";

?>
        