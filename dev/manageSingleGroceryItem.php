<?php

header('Content-type: application/json');

//$_POST['recipeid'] = 76;
//$_POST['devStatus'] = "dev";
//$_POST['active'] = 1;
//$_POST['ingredientid'] = 173;
//$_POST['userid'] = 53;


//301
    

require_once('../../Connections/chewsrite.php');
include("functions.php");

mysql_select_db($database_chewsrite, $chewsrite);


//recipeid=76&userid=53&devStatus=dev&active=1&ingredientid=173
    


//$response = ["URLOfTheSecondWebsite" => $request['websites'][1]['URL']];

$date = date("Y-m-d H:i:s");

//From RecipeDetails.swift or EditGroceryIngredient.swift


if(isset($_POST['recipeid']) && isset($_POST['userid']) && (isset($_POST['ingredientid']) || isset($_POST['subid'])))
{
    //check if substitute
    
    if(isset($_POST['isSubstitute']))
    {
        $query_rsGroceryItems = "SELECT * from groceryitems WHERE recipeid = {$_POST['recipeid']} AND userid = {$_POST['userid']}";
				
        $rsGroceryItems = mysql_query($query_rsGroceryItems, $chewsrite) or die(mysql_error());
        $row_rsGroceryItems = mysql_fetch_assoc($rsGroceryItems);
        $totalRows_rsGroceryItems = mysql_num_rows($rsGroceryItems);


        $object = new stdClass();

        $object->status = "grocery items not updated";

        if($totalRows_rsGroceryItems)
        {
            //$ids = explode(",",$_POST['ingredientids']);    

            $ids = explode(",",$row_rsGroceryItems['substitutionids']);

            if(isset($_POST['active']) && ($_POST['active'] == false || $_POST['active'] == "false"))
            {
                //remove current ingredient

               foreach($ids as $id)
                {
                    if($id != $_POST['subid'])
                    {
                        $newIDs[] = $id;
                    }
                } 
            }
            else
            {
                //add current ingredient

                foreach($ids as $id)
                {
                     $newIDs[] = $id;
                }

                $newIDs[] = $_POST['subid'];
            }

            $newIDString = implode(",",$newIDs);

            $updateSQL = sprintf("UPDATE groceryitems SET substitutionids = %s WHERE recipeid = %s AND userid = %s",
                            GetSQLValueString(mysql_real_escape_string($newIDString), "text"),
                            GetSQLValueString(mysql_real_escape_string($_POST['recipeid']), "int"),
                            GetSQLValueString(mysql_real_escape_string($_POST['userid']), "int"));

            mysql_select_db($database_chewsrite, $chewsrite);
            $Result1 = mysql_query($updateSQL, $chewsrite) or die(mysql_error());

            $object->status = "grocery items updated";
            $object->newIDs = $newIDString;
            $object->query = json_encode($updateSQL);
        }
        else
        {
            //recipe currently doesn't have grocery items
            
            $insertSQL = sprintf("INSERT INTO groceryitems (substitutionids, recipeid, userid, datecreated) VALUES (%s, %s, %s, %s)",
				GetSQLValueString(mysql_real_escape_string($_POST['subid']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['recipeid']), "int"),
				GetSQLValueString(mysql_real_escape_string($_POST['userid']), "int"),
				GetSQLValueString(mysql_real_escape_string($date), "date"));

            mysql_select_db($database_chewsrite, $chewsrite);
            $Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());
            
            $object->status = "grocery item added";
            $object->query = json_encode($insertSQL);
        }
    }
    else
    {
        //check if regular ingredient item
        
        $query_rsRecipes = "SELECT * from groceryitems WHERE recipeid = {$_POST['recipeid']} AND userid = {$_POST['userid']}";
				
        $rsRecipes = mysql_query($query_rsRecipes, $chewsrite) or die(mysql_error());
        $row_rsRecipes = mysql_fetch_assoc($rsRecipes);
        $totalRows_rsRecipes = mysql_num_rows($rsRecipes);


        $object = new stdClass();

        $object->status = "grocery items not updated";

        if($totalRows_rsRecipes)
        {
            //$ids = explode(",",$_POST['ingredientids']);    

            //$ids = explode(",",$row_rsRecipes['ingredientids']);
            
            
            if (strpos($row_rsRecipes['ingredientids'], ',') !== false) {

                $ids = explode(",",$row_rsRecipes['ingredientids']);
            }
            else
            {
                
                $ids[] = $row_rsRecipes['ingredientids'];
                //$ids = $row_rsRecipes['ingredientids'];
            }
                        

            if(isset($_POST['active']) && ($_POST['active'] == false || $_POST['active'] == "false"))
            {
                //remove current ingredient

               foreach($ids as $id)
                {
                    if($id != $_POST['ingredientid'])
                    {
                        $newIDs[] = $id;
                    }
                } 
            }
            else
            {
                //add current ingredient
                
                //173

                foreach($ids as $id)
                {
                     $newIDs[] = $id;
                }

                $newIDs[] = $_POST['ingredientid'];
            }
            
            //var_dump($newIDs);
            
            if(count($newIDs) > 1)
            {
                $newIDString = implode(",",$newIDs);
            }
            else
            {
                $newIDString = $newIDs[0];
            }            

            $updateSQL = sprintf("UPDATE groceryitems SET ingredientids = %s WHERE recipeid = %s AND userid = %s",
                            GetSQLValueString(mysql_real_escape_string($newIDString), "text"),
                            GetSQLValueString(mysql_real_escape_string($_POST['recipeid']), "int"),
                            GetSQLValueString(mysql_real_escape_string($_POST['userid']), "int"));

            mysql_select_db($database_chewsrite, $chewsrite);
            $Result1 = mysql_query($updateSQL, $chewsrite) or die(mysql_error());

            $object->status = "grocery items updated";
            $object->newIDs = $newIDString;
        }
        else
        {
            //recipe currently doesn't have grocery items
            
            $insertSQL = sprintf("INSERT INTO groceryitems (ingredientids, recipeid, userid, datecreated) VALUES (%s, %s, %s, %s)",
				GetSQLValueString(mysql_real_escape_string($_POST['ingredientid']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['recipeid']), "int"),
				GetSQLValueString(mysql_real_escape_string($_POST['userid']), "int"),
				GetSQLValueString(mysql_real_escape_string($date), "date"));

            mysql_select_db($database_chewsrite, $chewsrite);
            $Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());
            
            $object->status = "grocery item added";
            $object->newIDs = $newIDString;
        }
    }    
}


echo "{\"data\":";
echo "{\"groceryItemsData\":";
echo json_encode( $object );
echo "}";
echo "}";

?>
