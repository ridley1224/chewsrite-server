<?php

header('Content-type: application/json');

require_once('../../Connections/chewsrite.php');
include("functions.php");

mysql_select_db($database_chewsrite, $chewsrite);

//$response = ["URLOfTheSecondWebsite" => $request['websites'][1]['URL']];

$string;
$date = date("Y-m-d H:i:s");


$object = new stdClass();
$object->status = "venue not saved";

if($_POST['venuename'])
{
	if(isset($_POST['updating']))
	{
		$insertSQL = sprintf("UPDATE venues SET venuename = %s,venueinfo = %s,venuephone = %s, address = %s, city = %s, state = %s, zip = %s WHERE venueid = %s",
					GetSQLValueString(mysql_real_escape_string($_POST['venuename']), "text"),
					GetSQLValueString(mysql_real_escape_string($_POST['venueinfo']), "text"),
					GetSQLValueString(mysql_real_escape_string($_POST['venuephone']), "text"),
					GetSQLValueString(mysql_real_escape_string($_POST['address']), "text"),
					GetSQLValueString(mysql_real_escape_string($_POST['city']), "text"),
					GetSQLValueString(mysql_real_escape_string($_POST['state']), "text"),
					GetSQLValueString(mysql_real_escape_string($_POST['zip']), "text"),
					GetSQLValueString(mysql_real_escape_string($_POST['venueid']), "int"));
			
		mysql_select_db($database_chewsrite, $chewsrite);
		$Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());
		
		if(isset($_POST['venueimage']))
		{
			$insertSQL = sprintf("UPDATE venues SET venueimage = %s WHERE venueid = %s",
						GetSQLValueString(mysql_real_escape_string($_POST['venueimage']), "text"),
						GetSQLValueString(mysql_real_escape_string($_POST['venueid']), "int"));

			mysql_select_db($database_chewsrite, $chewsrite);
			$Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());
		}
		
		$object->status = "venue updated";
	}
	else
	{
		$insertSQL = sprintf("INSERT INTO venues (venuename, userid, address, city, state, zip, venuephone, venueinfo, venueimage, datecreated) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
					GetSQLValueString(mysql_real_escape_string($_POST['venuename']), "text"),
					GetSQLValueString(mysql_real_escape_string($_POST['userid']), "int"),
					GetSQLValueString(mysql_real_escape_string($_POST['address']), "text"),
					GetSQLValueString(mysql_real_escape_string($_POST['city']), "text"),
					GetSQLValueString(mysql_real_escape_string($_POST['state']), "text"),
					GetSQLValueString(mysql_real_escape_string($_POST['zip']), "text"),
					GetSQLValueString(mysql_real_escape_string($_POST['venuephone']), "date"),
					GetSQLValueString(mysql_real_escape_string($_POST['venueinfo']), "text"),
					GetSQLValueString(mysql_real_escape_string($_POST['venueimage']), "text"),
					GetSQLValueString(mysql_real_escape_string($date), "date"));

		mysql_select_db($database_chewsrite, $chewsrite);
		$Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());	

		$last_id = mysql_insert_id();	
		$object->venueid = $last_id;
		$object->status = "venue added";
	}
	
	
	
}

echo "{\"data\":";
echo "{\"venueData\":";
echo json_encode( $object );
echo "}";
echo "}";

?>
