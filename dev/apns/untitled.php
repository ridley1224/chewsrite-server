<?php
 
const AUTH_KEY_PATH = 'AuthKey_42L9WRSHLA.p8';
const AUTH_KEY_ID = '42L9WRSHLA';
const TEAM_ID = '5RS7D6MXZX';
const BUNDLE_ID = 'com.Chewsrite';
 
$payload = [
    'aps' => [
      'alert'    => [
          'title' => 'This is the notification.',
      ],
      'sound'    => 'default',
    ],
];
 
function tokensToReceiveNotification()
{
    return ['1ec490c31d049cb6808cac3df7a04f4f66450700f8d5f1eae671fe0d5ae377f0'];
}
 
function generateAuthenticationHeader()
{
    $header = base64_encode(json_encode(['alg' => 'ES256', 'kid' => AUTH_KEY_ID]));
    $claims = base64_encode(json_encode(['iss' => TEAM_ID, 'iat' => time()]));
 
    $pkey = openssl_pkey_get_private('file://' . AUTH_KEY_PATH);
    openssl_sign("$header.$claims", $signature, $pkey, 'sha256');
 
    $signed = base64_encode($signature);
 
    return "$header.$claims.$signed";
}
 
$ch = curl_init();
curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_2_0);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payload));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, [
    'apns-topic: ' . BUNDLE_ID,
    'Authorization: Bearer ' . generateAuthenticationHeader()
]);
 
foreach (tokensToReceiveNotification() as $token) {
    $url = "https://api.development.push.apple.com/3/device/$token";
    curl_setopt($ch, CURLOPT_URL, "{$url}");
 
    $response = curl_exec($ch);
    if ($response === false) {
        echo("curl_exec failed: " . curl_error($ch));
        continue;
    }
 
    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    if ($code === 400) {
        $json = @json_decode($response);
        if ($json->reason === 'BadDeviceToken') {
            # Remove $token from your list of valid tokens to send to
        }
    }
}
 
curl_close($ch);

?>