<?php require_once('../../Connections/chewsrite.php'); ?>
<?php

include("functions.php");
include("en-de.php");

$object = new stdClass();
$date = date("Y-m-d H:i:s");


if(isset($_POST['groupids']))
{	

    $groupids = explode(',',$_POST['groupids']);

    foreach($groupids as $groupid) {

        //get group info

        $query_rsGroupInfo = "SELECT groupname from groups WHERE groupid = '" . $groupid . "' ";

        $rsGroupInfo = mysql_query($query_rsGroupInfo, $chewsrite) or die(mysql_error());
        $row_rsGroupInfo = mysql_fetch_assoc($rsGroupInfo);
        $totalRows_rsGroupInfo = mysql_num_rows($rsGroupInfo);


        //get users in selected group

        $query_rsInvites = "SELECT a.userid,b.deviceid FROM (SELECT userid FROM groupmembers WHERE groupid = {$groupid}) as a INNER JOIN (SELECT * FROM users WHERE deviceid IS NOT NULL) as b ON a.userid = b.userid";

        $rsInvites = mysql_query($query_rsInvites, $chewsrite) or die(mysql_error());
        $row_rsInvites = mysql_fetch_assoc($rsInvites);
        $totalRows_rsInvites = mysql_num_rows($rsInvites);


        if($totalRows_rsInvites)
        {
            do {

                $message = "{$row_rsInvites['firstname']}, a new recipe has been added to your group " . strtoupper($row_rsGroupInfo['groupname']) . "!";
                $url = "http://chewsrite.com";

                $deviceToken = $row_rsInvites['deviceid'];
                $_POST['token'] = $row_rsInvites['deviceid'];

                $body['aps'] = array(
                          'alert' => $message,
                          'sound' => 'default',
                          'link_url' => $url,
                          'category' => "RECIPE_GROUP",
                          'body' => $_POST['recipeid'],
                          'badge' => 1,
                          );

                include("send-push-notification.php");

            }  while ($row_rsInvites = mysql_fetch_assoc($rsInvites));
        }
        
        $object->status = "invites sent to {$totalRows_rsInvites} users";
    }
}

echo "{\"data\":";
echo "{\"inviteData\":";
echo json_encode( $object );
echo "}";
echo "}";

?>