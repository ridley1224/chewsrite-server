<?php 

header('Content-type: application/json');

require_once('../../Connections/chewsrite.php'); 

include("functions.php");
include("en-de.php");

//$_POST['recipeid'] = "1";

if(isset($_POST['recipeid']))
{
    $recipeid = $_POST['recipeid'];
}
else if(isset($_GET['recipeid']))
{
    $recipeid = $_GET['recipeid'];
}

if(isset($recipeid))
{    
    mysql_select_db($database_chewsrite, $chewsrite);
    
    
    //$query_rsRecipeSubstitutes = "SELECT * FROM recipesubstitutes WHERE recipeid = {$_POST['recipeid']}";
    
     //$query_rsRecipeSubstitutes = "SELECT a.*,b.ingredientid FROM (SELECT * FROM substitutions) as a INNER JOIN (SELECT * FROM recipesubstitutes WHERE recipeid = {$_POST['recipeid']}) as b ON a.subid = b.subid";
    
    
    //include("includes/substitutionsQuery.php");
    
    //var_dump($replacementlist);
    
    
    $query_rsRecipes = "SELECT * from recipeingredients WHERE recipeid = {$recipeid} AND active = 1";

    //echo $query_rsRecipes;

    $rsRecipes = mysql_query($query_rsRecipes, $chewsrite) or die(mysql_error());
    $row_rsRecipes = mysql_fetch_assoc($rsRecipes);
    $totalRows_rsRecipes = mysql_num_rows($rsRecipes);

    if($totalRows_rsRecipes)
    {
        do {

            $ingredientObject = new stdClass();

            $ingredientObject->recipeid = blankNull($row_rsRecipes['recipeid']);
            $ingredientObject->ingredientname = (string)blankNull($row_rsRecipes['ingredientname']);
            $ingredientObject->ingredientid = (string)blankNull($row_rsRecipes['ingredientid']);
            $ingredientObject->quantity = (string)blankNull($row_rsRecipes['quantity']);
            $ingredientObject->unit = (string)blankNull($row_rsRecipes['unit']);
            $ingredientObject->active = (int)$row_rsRecipes['active'];
            $ingredientObject->isSubstitute = false;
            $ingredientObject->imagename = "";
            $list[] = $ingredientObject;
            
            //check replacement list
            
            //include("includes/checkSubstitutionObject.php");
            
            
        }  while ($row_rsRecipes = mysql_fetch_assoc($rsRecipes));
    }
    
    
    
    $query_rsRecipeSubstitutes = "SELECT a.*, b.recipeid, b.active FROM (SELECT * FROM substitutions) as a INNER JOIN (SELECT * FROM recipesubstitutes WHERE recipeid = {$recipeid}) as b ON a.subid = b.subid";

    $rsRecipeSubstitutes = mysql_query($query_rsRecipeSubstitutes, $chewsrite) or die(mysql_error());
    $row_rsRecipeSubstitutes = mysql_fetch_assoc($rsRecipeSubstitutes);
    $totalRows_rsRecipeSubstitutes = mysql_num_rows($rsRecipeSubstitutes);

    if($totalRows_rsRecipeSubstitutes)
    {
        //echo "has substitutions<br>";

        //add substitutes from list
        //populates in checkSubstitutionObject.php

        do {

            $object = new stdClass();

            $object->recipeid = blankNull($row_rsRecipeSubstitutes['recipeid']);
            $object->ingredientname = (string)blankNull($row_rsRecipeSubstitutes['substitution']);
            //$object->subid = (string)blankNull($row_rsRecipeSubstitutes['subid']);
            $object->replacedobjectid = (string)blankNull($row_rsRecipeSubstitutes['ingredientid']);
            $object->ingredientid = (string)blankNull($row_rsRecipeSubstitutes['subid']);
            $object->quantity = (string)blankNull($row_rsRecipeSubstitutes['quantity']);
            $object->unit = (string)blankNull($row_rsRecipeSubstitutes['unit']);
            $object->active = (int)$row_rsRecipeSubstitutes['active'];
            //$object->imagename = blankNull($row_rsRecipeDetails['imagename']);
            $object->isSubstitute = true;

            $list[] = $object;				

        }  while ($row_rsRecipeSubstitutes = mysql_fetch_assoc($rsRecipeSubstitutes));
    }

}



//SELECT 
//    cuisinename 
//FROM
//    cuisines
//WHERE
//    cuisineid IN (SELECT 
//            cuisinetags
//        FROM
//            recipes
//        WHERE
//            recipeid = 1);


	
echo "{\"data\":";
echo "{\"itemsData\":";
echo json_encode( $list );
echo "}";
echo "}";

?>
