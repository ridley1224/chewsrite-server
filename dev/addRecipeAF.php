<?php

header('Content-type: application/json');

error_reporting(E_ERROR | E_PARSE);

require_once('../../Connections/chewsrite.php');
include("functions.php");

mysql_select_db($database_chewsrite, $chewsrite);

$request = json_decode(file_get_contents('php://input'), TRUE);


//$request['recipename'] = "shrimp alfredo";
//$request['servings'] = "4";
//$request= json_decode("{\"ingredients\":[{\"active\": \"0\",\"ingredient\": \"Shrimp\",\"name\": \"\",\"quantity\": \"1\",\"recipeorder\": \"0\",\"unit\": \"oz\"}, {\"active\": \"0\",\"ingredient\": \"alfredo sauce\",\"name\": \"\",\"quantity\": \"1\",\"recipeorder\": \"1\",\"unit\": \"oz\"}, {\"active\": \"0\",\"ingredient\": \"alfredo noodles\",\"name\": \"\",\"quantity\": \"8\",\"recipeorder\": \"1\",\"unit\": \"oz\"}]}");
//
////var_dump($request->ingredients);
//
//foreach ($request->ingredients as $ingredient) 
//        {
//    //echo "ingredient<br>";
//    
//    
//    $ingredientList[] = $ingredient->quantity . " " . $ingredient->unit . " " . $ingredient->ingredient;
//}
//
////var_dump($ingredientList);
//
//$ingredientParams = implode(",",$ingredientList);
//
//var_dump($ingredientParams);
//        
//$fileJSON = "{\"title\": \"{$request['recipename']}\",\"yield\": \"{$request['servings']}\",\"ingr\": [{$ingredientParams}]}";
//
////             print "<pre>";
////             print_r( $ingredients );
////             print "</pre>";
//
// print "<pre>";
// print_r( $fileJSON );
// print "</pre>";

//$response = ["URLOfTheSecondWebsite" => $request['websites'][1]['URL']];

$string;
$date = date("Y-m-d H:i:s");

$status = "data not saved";

//$request['recipename'] = "a";

if($request['recipename'])
{
    if($request['filetype'] == 1)
    {
        $videoname = $request['imagename'].".mov";
    }
    else
    {
        $videoname = "";
    }    
    
	$insertSQL = sprintf("INSERT INTO recipes (recipename, userid, description, source, sourceurl, servings, preptime, cooktime, totaltime, calories, imagename, videoname, cuisinetags, dateadded) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
            GetSQLValueString(mysql_real_escape_string($request['recipename']), "text"),
            GetSQLValueString(mysql_real_escape_string($request['userid']), "int"),
            GetSQLValueString(mysql_real_escape_string($request['description']), "text"),
            GetSQLValueString(mysql_real_escape_string($request['source']), "text"),
            GetSQLValueString(mysql_real_escape_string($request['sourceurl']), "text"),
            GetSQLValueString(mysql_real_escape_string($request['servings']), "text"),
            GetSQLValueString(mysql_real_escape_string($request['preptime']), "text"),
            GetSQLValueString(mysql_real_escape_string($request['cooktime']), "text"),
            GetSQLValueString(mysql_real_escape_string($request['totaltime']), "text"),
            GetSQLValueString(mysql_real_escape_string($request['calories']), "text"),
            GetSQLValueString(mysql_real_escape_string($request['imagename'].".jpg"), "text"),
            GetSQLValueString(mysql_real_escape_string($videoname), "text"),
            GetSQLValueString(mysql_real_escape_string($request['cuisinetags']), "text"),
            GetSQLValueString($date, "date"));
			
	mysql_select_db($database_chewsrite, $chewsrite);
	$Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());	

	$last_id = mysql_insert_id();	
	$status = "recipe saved";
        
    //send push to all users with dietary concerns in cuisine tags
    
    mysql_select_db($database_chewsrite, $chewsrite);
    $query_rsCuisineRecipients = "SELECT a.*,b.firstname,b.lastname,b.deviceid FROM (SELECT userid,selections FROM usercuisines WHERE selections IN ({$request['cuisinetags']})) as a INNER JOIN (SELECT * FROM users WHERE deviceid IS NOT NULL) as b ON a.userid = b.userid";

    $rsCuisineRecipients = mysql_query($query_rsCuisineRecipients, $chewsrite) or die(mysql_error());
    $row_rsCuisineRecipients = mysql_fetch_assoc($rsCuisineRecipients);
    $totalRows_rsCuisineRecipients = mysql_num_rows($rsCuisineRecipients);
    
    if(isset($request['ingredients']))
    {
        foreach ($request['ingredients'] as $ingredient) 
        {	
            //to do randall
            //look into query ingredientid on insert in app
            
            $a = strtolower($ingredient['ingredient']);
            
            $query_rsRecipes = "SELECT ingredientid from ingredients WHERE LOWER(ingredient) LIKE '{$a}'";

            //echo $query_rsRecipes;

            $rsRecipes = mysql_query($query_rsRecipes, $chewsrite) or die(mysql_error());
            $row_rsRecipes = mysql_fetch_assoc($rsRecipes);
            $totalRows_rsRecipes = mysql_num_rows($rsRecipes);

            if($totalRows_rsRecipes > 0)
            {
                //ingredient exists
                
                $ingedientid = $row_rsRecipes['ingredientid'];
            }
            else
            {
                $ingredientSQL = sprintf("INSERT INTO ingredients (ingredient, userid, datecreated) VALUES (%s, %s, %s)",
                GetSQLValueString(mysql_real_escape_string(trim($ingredient['ingredient'])), "text"),
                GetSQLValueString(mysql_real_escape_string($request['userid']), "int"),
                GetSQLValueString($date, "date"));

                mysql_select_db($database_chewsrite, $chewsrite);
                $Result1 = mysql_query($ingredientSQL, $chewsrite) or die(mysql_error());
            }
            
            
            $ingredientList[] = '"' . $ingredient['quantity'] . " " . $ingredient['unit'] . " " . $ingredient['ingredient'] . '"';
                        

            $insertSetSQL = sprintf("INSERT INTO recipeingredients (ingredientname, ingredientid, quantity, unit, recipeorder, recipeid,active,datecreated) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
                GetSQLValueString(mysql_real_escape_string($ingredient['ingredient']), "text"),
                GetSQLValueString(mysql_real_escape_string(blankNull($ingedientid)), "int"),
                GetSQLValueString(mysql_real_escape_string($ingredient['quantity']), "text"),
                GetSQLValueString(mysql_real_escape_string($ingredient['unit']), "text"),
                GetSQLValueString(mysql_real_escape_string($ingredient['recipeorder']), "text"),
                GetSQLValueString(mysql_real_escape_string($last_id), "int"),
                GetSQLValueString(1, "int"),
                GetSQLValueString($date, "date"));

                mysql_select_db($database_chewsrite, $chewsrite);
                $Result2 = mysql_query($insertSetSQL, $chewsrite) or die(mysql_error());
        }
        
        
        $status .= ". ingredients saved";
        
        $ingredientParams = implode(",",$ingredientList);
        
        
        
        
        
        $fileJSON = "{\"title\": \"{$request['recipename']}\",\"yield\": \"{$request['servings']}\",\"ingr\": [{$ingredientParams}]}";
        
        
//        $myfile = fopen("upload.json", "w") or die("Unable to open file!");
//        $txt = $fileJSON;
//        fwrite($myfile, $txt);
//        fclose($myfile);
        
        

//         if ( $debug ) {
//             print "<pre>";
//             print_r( $ingredients );
//             print "</pre>";
//
//             print "<pre>";
//             print_r( $fileJSON );
//             print "</pre>";
//         }        
        
        $_GET['mobile'] = true;
        
        include( "../admin/add-bulk-nutrition.php" );
        
        $status .= ". nutrition api data saved";
    }
        
    if($request['folderids'] && $request['folderids'] != "")
    {
        $insertSetSQL = sprintf("INSERT INTO recipefolders (folderids, recipeid, userid) VALUES (%s, %s, %s)",
        GetSQLValueString(mysql_real_escape_string($request['folderids']), "text"),
        GetSQLValueString(mysql_real_escape_string($last_id), "int"),
        GetSQLValueString(mysql_real_escape_string($request['userid'])));

        mysql_select_db($database_chewsrite, $chewsrite);
        $Result2 = mysql_query($insertSetSQL, $chewsrite) or die(mysql_error());
        
        $status .= ". folders saved";
    }
    
    if(isset($request['nutritionValLabels']))
    {
        $request['nutritionValLabels'] .= ",recipeid";
        $request['nutritionValues'] .= ",{$last_id}";
        
        
//        $insertSQL = "INSERT INTO recipenutrition ({$request['nutritionValLabels']}) VALUES ({$request['nutritionValues']})";
//
//        mysql_select_db($database_chewsrite, $chewsrite);
//        $Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());	
//        $status .= ". nutrition info saved";
    }
    
    if(isset($request['directions']))
    {
        foreach ($request['directions'] as $ingredient) 
        {	
            //to do randall
            //look into query ingredientid on insert in app

            $insertSetSQL = sprintf("INSERT INTO recipedirections (title, directions, directionsorder, recipeid, datecreated) VALUES (%s, %s, %s, %s, %s)",
            GetSQLValueString(mysql_real_escape_string($ingredient['title']), "text"),
            GetSQLValueString(mysql_real_escape_string($ingredient['directions']), "text"),
            GetSQLValueString(mysql_real_escape_string($ingredient['directionsorder']), "text"),
            GetSQLValueString(mysql_real_escape_string($last_id), "int"),
                               GetSQLValueString( $date, "date" ));

            mysql_select_db($database_chewsrite, $chewsrite);
            $Result2 = mysql_query($insertSetSQL, $chewsrite) or die(mysql_error());
        }
        
        $status .= ". directions saved";
    }
    
    if(isset($request['tips']))
    {
        foreach ($request['tips'] as $tip) 
        {
            $insertSetSQL = sprintf("INSERT INTO recipetips (tip, recipeid, userid, dateadded) VALUES (%s, %s, %s, %s)",
                GetSQLValueString(mysql_real_escape_string($tip['tips']), "text"),
                GetSQLValueString(mysql_real_escape_string($last_id), "int"),
                GetSQLValueString(mysql_real_escape_string($request['userid']), "text"),
                GetSQLValueString($date, "date"));

                mysql_select_db($database_chewsrite, $chewsrite);
                $Result2 = mysql_query($insertSetSQL, $chewsrite) or die(mysql_error());            
        }
        
        
        $status .= ". tips saved";
    }
    
    
    $enablePush = false;
    
    if($enablePush == true && $totalRows_rsCuisineRecipients > 0)
    {
        do {
        
            $url = "http://chewsrite.com";
            $deviceToken = $row_rsCuisineRecipients['deviceid'];
            $_POST['token'] = $row_rsCuisineRecipients['deviceid'];

            $message = "{$row_rsCuisineRecipients['firstname']}, New recipes have been added!";

            $body['aps'] = array(
                      'alert' => $message,
                      'sound' => 'default',
                      'link_url' => $url,
                      'category' => "NEW_RECIPE_SIGNAL",
                      'body' => "body test",
                      'badge' => 1,
                      );

            //echo "send to user: ".$deviceToken."<br>";

            include("send-push-notification.php");

        }  while ($row_rsCuisineRecipients = mysql_fetch_assoc($rsCuisineRecipients));

        $pushStatus = "New recipe signal sent at $date EST to {$totalRows_rsCuisineRecipients} users.";
    }
    else
    {
        $pushStatus = "none";
    }
}

$response = ["status" => $status, "recipeid" => $last_id, "pushStatus" => $pushStatus];

echo json_encode($response);

?>
