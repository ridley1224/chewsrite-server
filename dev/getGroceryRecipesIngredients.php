<?php

header( 'Content-type: application/json' );

//$_POST[ 'devStatus' ] = "dev";
//$_POST[ 'userid' ] = "53";


require_once( '../../Connections/chewsrite.php' );

include( "functions.php" );
include( "en-de.php" );


mysql_select_db( $database_chewsrite, $chewsrite );


if ( isset( $_POST[ 'userid' ] ) && $_POST[ 'userid' ] != "" ) {

  mysql_select_db( $database_chewsrite, $chewsrite );

  $query_rsGroceryItems = "SELECT * FROM groceryitems WHERE userid = {$_POST['userid']}";

  $rsGroceryItems = mysql_query( $query_rsGroceryItems, $chewsrite )or die( mysql_error() );
  $row_rsGroceryItems = mysql_fetch_assoc( $rsGroceryItems );
  $totalRows_rsGroceryItems = mysql_num_rows( $rsGroceryItems );

  //echo "<p>{$query_rsGroceryItems}</p>";

  if ( $totalRows_rsGroceryItems > 0 ) {

    do {

      //get basic ingredient types in cart

      $object = new stdClass();
      $object->ingredientids = $row_rsGroceryItems[ 'ingredientids' ];
      $object->recipeid = $row_rsGroceryItems[ 'recipeid' ];

      $ingredientIDsList[] = $object;

      $query_rsRecipeDetails = " SELECT a.*, b.count, b.recipeid FROM (SELECT * from recipes WHERE recipeid = {$row_rsGroceryItems['recipeid']}) as a INNER JOIN (SELECT count(*) as 'count', recipeid FROM recipeingredients WHERE recipeid = {$row_rsGroceryItems['recipeid']} AND active = 1) as b ON a.recipeid = b.recipeid";

      //echo "<p>{$query_rsRecipeDetails}</p>";

      $rsRecipeDetails = mysql_query( $query_rsRecipeDetails, $chewsrite )or die( mysql_error() );
      $row_rsRecipeDetails = mysql_fetch_assoc( $rsRecipeDetails );
      $totalRows_rsRecipeDetails = mysql_num_rows( $rsRecipeDetails );

      do {

        $object2 = new stdClass();
        $object2->recipename = blankNull($row_rsRecipeDetails[ 'recipename' ]);
        $object2->recipeid = blankNull($row_rsRecipeDetails[ 'recipeid' ]);
        $object2->recipeIngredientsCount = (int)$row_rsRecipeDetails[ 'count' ];
        $object2->calories = blankNull($row_rsRecipeDetails[ 'calories' ]);
        $object2->imagename = blankNull($row_rsRecipeDetails[ 'imagename' ]);

        $recipeObs[] = $object2;

      } while ( $row_rsRecipeDetails = mysql_fetch_assoc( $rsRecipeDetails ) );


    } while ( $row_rsGroceryItems = mysql_fetch_assoc( $rsGroceryItems ) );

    //        var_dump($recipeObs);
    //        
    //        return;

    //var_dump($ingredientIDsList);

    //return;

    foreach ( $ingredientIDsList as $idItem ) {

      $ingredientIDs = explode( ",", $idItem->ingredientids );

      //echo "item: {$ingredientIDs}<br>";

      //var_dump($ingredientIDs);

      foreach ( $ingredientIDs as $id ) {
        //echo "id: {$id}<br>";

        $recipeid = $idItem->recipeid;

        $query_rsIngredients = "SELECT * from recipeingredients WHERE ingredientid = {$id} AND active = 1 AND recipeid = {$recipeid}";

        //echo "<p>{$query_rsIngredients}</p>";

        $rsIngredients = mysql_query( $query_rsIngredients, $chewsrite )or die( mysql_error() );
        $row_rsIngredients = mysql_fetch_assoc( $rsIngredients );
        $totalRows_rsIngredients = mysql_num_rows( $rsIngredients );

        if ( $totalRows_rsIngredients > 0 ) {

          do {

            $ingredientObject = new stdClass();
            $ingredientObject->ingredientname = blankNull( $row_rsIngredients[ 'ingredientname' ] );
            $ingredientObject->ingredientid = blankNull( $row_rsIngredients[ 'ingredientid' ] );
            $ingredientObject->ingredientsid = blankNull( $row_rsIngredients[ 'ingredientsid' ] );
            $ingredientObject->recipeid = blankNull( $row_rsIngredients[ 'recipeid' ] );
            $ingredientObject->quantity = ( string )blankNull( $row_rsIngredients[ 'quantity' ] );
            $ingredientObject->unit = ( string )blankNull( $row_rsIngredients[ 'unit' ] );
            $ingredientObject->active = ( int )$row_rsIngredients[ 'active' ];
            $ingredientObject->imagename = "";
            $ingredientObject->isSubstitute = false;

            //$list[] = $ingredientObject;

            foreach ( $recipeObs as $recipeOb ) {
              if ( $recipeOb->recipeid == $recipeid ) {
                $recipeOb->ingredients[] = $ingredientObject;
                break;
              }
            }

          } while ( $row_rsIngredients = mysql_fetch_assoc( $rsIngredients ) );
        }
      }
    }
  }
}

//update cart numbers

foreach ( $recipeObs as $recipeOb ) {
  $count = count( $recipeOb->ingredients );
  $remainingCount = ( int )$recipeOb->recipeIngredientsCount - $count;
  $recipeOb->remainingCount = ( int )$remainingCount;
    $recipeOb->groceryItemsCount = ( int )$count;
}

//echo "<pre>";
//var_dump($recipeObs);
//echo "<pre>";

echo "{\"data\":";
echo "{\"recipeData\":";
echo json_encode( $recipeObs );
echo "}";
echo "}";

?>
