<?php

  $keyfile = 'AuthKey_42L9WRSHLA.p8';               # <- Your AuthKey file
  $keyid = '42L9WRSHLA';                            # <- Your Key ID
  $teamid = '5RS7D6MXZX';                           # <- Your Team ID (see Developer Portal)
  $bundleid = 'com.Chewsrite';                # <- Your Bundle ID
  $url = 'https://api.development.push.apple.com';  # <- development url, or use http://api.push.apple.com for production environment
  $token = '1ec490c31d049cb6808cac3df7a04f4f66450700f8d5f1eae671fe0d5ae377f0';              # <- Device Token

  $message = '{"aps":{"alert":"Hi there!","sound":"default"}}';

  $key = openssl_pkey_get_private('file://'.$keyfile);

  $header = ['alg'=>'ES256','kid'=>$keyid];
  $claims = ['iss'=>$teamid,'iat'=>time()];

  $header_encoded = base64($header);
  $claims_encoded = base64($claims);

  $signature = '';
  openssl_sign($header_encoded . '.' . $claims_encoded, $signature, $key, 'sha256');
  $jwt = $header_encoded . '.' . $claims_encoded . '.' . base64_encode($signature);

  // only needed for PHP prior to 5.5.24
  if (!defined('CURL_HTTP_VERSION_2_0')) {
      define('CURL_HTTP_VERSION_2_0', 3);
  }

echo "1";

  $http2ch = curl_init();
  curl_setopt_array($http2ch, array(
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_2_0,
    CURLOPT_URL => "{$url}/3/device/{$token}",
    CURLOPT_PORT => 443,
    CURLOPT_HTTPHEADER => array(
      "apns-topic: {$bundleid}",
      "authorization: bearer {$jwt}"
    ),
    CURLOPT_POST => TRUE,
    CURLOPT_POSTFIELDS => $message,
    CURLOPT_RETURNTRANSFER => TRUE,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HEADER => 1
  ));

echo "2";

  $result = curl_exec($http2ch);


  if ($result === FALSE) {
      
      echo "3";
      
    throw new Exception("Curl failed: ".curl_error($http2ch));
  }

echo "4";

  $status = curl_getinfo($http2ch, CURLINFO_HTTP_CODE);

  echo "status: {$status}";

  function base64($data) {
      
    return rtrim(strtr(base64_encode(json_encode($data)), '+/', '-_'), '=');
  }

?>