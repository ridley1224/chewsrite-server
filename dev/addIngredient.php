<?php

//header('Content-type: application/json');

require_once('../../Connections/chewsrite.php');
include("functions.php");

mysql_select_db($database_chewsrite, $chewsrite);

//$response = ["URLOfTheSecondWebsite" => $request['websites'][1]['URL']];

$string;
$date = date("Y-m-d H:i:s");


$object = new stdClass();
$object->status = "ingredient not saved";

if(isset($_POST['ingredients']))
{    
    $ingredients = explode(',',$_POST['ingredients']);
    
    foreach($ingredients as $ingredient) {

        $insertSQL = sprintf("INSERT INTO useringredients (ingredient, userid, category, datecreated) VALUES (%s, %s, %s, %s)",
                        GetSQLValueString(mysql_real_escape_string($ingredient), "text"),
                        GetSQLValueString(mysql_real_escape_string($_POST['userid']), "int"),
                        GetSQLValueString(mysql_real_escape_string($_POST['category']), "int"),
                        GetSQLValueString(mysql_real_escape_string($date), "date"));

        mysql_select_db($database_chewsrite, $chewsrite);
        $Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());	

        $last_id = mysql_insert_id();	
        $ids[] = $last_id;
    }
    
    $object->ingredientids = implode(',',$ids);
    $object->status = "ingredient info saved";

}

echo "{\"data\":";
echo "{\"ingredientData\":";
echo json_encode( $object );
echo "}";
echo "}";

?>
