<?php

header('Content-type: application/json');

require_once('../../Connections/chewsrite.php');
include("functions.php");

mysql_select_db($database_chewsrite, $chewsrite);

//$response = ["URLOfTheSecondWebsite" => $request['websites'][1]['URL']];

$date = date("Y-m-d H:i:s");

//$_POST['recipeid'] = "1";
//$_POST['userid'] = "1";


$object = new stdClass();
$object->status = "favorite not saved";


if(isset($_POST['userid']) && isset($_POST['recipeid']))
{
	$query_rsRecipes1 = "SELECT * FROM userfavorites WHERE recipeid = " . $_POST['recipeid'] . " AND userid = " . $_POST['userid'];
	$rsRecipes1 = mysql_query($query_rsRecipes1, $chewsrite) or die(mysql_error());
	$row_rsRecipes1 = mysql_fetch_assoc($rsRecipes1);
	$totalRows_rsRecipes1 = mysql_num_rows($rsRecipes1);

	if($totalRows_rsRecipes1)
	{	
		$insertSQL = sprintf("DELETE FROM userfavorites WHERE recipeid = %s AND userid = %s",
						GetSQLValueString(mysql_real_escape_string($_POST['recipeid']), "int"),
						GetSQLValueString(mysql_real_escape_string($_POST['userid']), "int"));

		mysql_select_db($database_chewsrite, $chewsrite);
		$Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());

		$object->status = "favorite deleted";
	}
	else
	{
		$insertSQL = sprintf("INSERT INTO userfavorites (recipeid, userid, datecreated) VALUES (%s, %s, %s)",
						GetSQLValueString(mysql_real_escape_string($_POST['recipeid']), "int"),
						GetSQLValueString(mysql_real_escape_string($_POST['userid']), "int"),
						GetSQLValueString(mysql_real_escape_string($date), "date"));

		mysql_select_db($database_chewsrite, $chewsrite);
		$Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());	

		$last_id = mysql_insert_id();	
		$object->favoriteid = $last_id;
		$object->status = "favorite item saved";	
	}
}

echo "{\"data\":";
echo "{\"favoriteData\":";
echo json_encode( $object );
echo "}";
echo "}";

?>
