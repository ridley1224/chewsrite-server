<?php

header('Content-type: application/json');

require_once('../../Connections/chewsrite.php');
include("functions.php");

mysql_select_db($database_chewsrite, $chewsrite);

//$response = ["URLOfTheSecondWebsite" => $request['websites'][1]['URL']];

$date = date("Y-m-d H:i:s");


//$_POST['recipeid'] = "1";
//$_POST['userid'] = "22";
//$_POST['ingredientids'] = "5";


$query_rsRecipes = "SELECT * from groceryitems WHERE recipeid = {$_POST['recipeid']} AND userid = {$_POST['userid']}";
				
$rsRecipes = mysql_query($query_rsRecipes, $chewsrite) or die(mysql_error());
$row_rsRecipes = mysql_fetch_assoc($rsRecipes);
$totalRows_rsRecipes = mysql_num_rows($rsRecipes);


$object = new stdClass();

$object->status = "grocery items not saved";

if($totalRows_rsRecipes)
{
    $updateSQL = sprintf("UPDATE groceryitems SET ingredientids = %s, substitutionids = %s WHERE recipeid = %s AND userid = %s",
					GetSQLValueString(mysql_real_escape_string($_POST['ingredientids']), "text"),
                    GetSQLValueString(mysql_real_escape_string($_POST['subIDs']), "text"),
					GetSQLValueString(mysql_real_escape_string($_POST['recipeid']), "int"),
					GetSQLValueString(mysql_real_escape_string($_POST['userid']), "int"));
			
	mysql_select_db($database_chewsrite, $chewsrite);
	$Result1 = mysql_query($updateSQL, $chewsrite) or die(mysql_error());
    
    
    
//	$updateSQL = sprintf("UPDATE groceryitems SET ingredientids = %s WHERE recipeid = %s AND userid = %s",
//					GetSQLValueString(mysql_real_escape_string($_POST['ingredientids']), "text"),
//					GetSQLValueString(mysql_real_escape_string($_POST['recipeid']), "int"),
//					GetSQLValueString(mysql_real_escape_string($_POST['userid']), "int"));
//			
//	mysql_select_db($database_chewsrite, $chewsrite);
//	$Result1 = mysql_query($updateSQL, $chewsrite) or die(mysql_error());
//
//    
//    //userid=22&ingredientids=122,364&recipeid=1&devStatus=dev&removedSubIDs=6&subIDs=
//    
//    if(isset($_POST['removedSubIDs']) && $_POST['removedSubIDs'] != "")
//    {
//        $updateSQL = sprintf("UPDATE recipesubstitutes SET active = %s WHERE subid IN (%s) AND recipeid = %s",
//            GetSQLValueString(mysql_real_escape_string(0), "int"),
//            GetSQLValueString(mysql_real_escape_string($_POST['removedSubIDs']), "int"),
//            GetSQLValueString(mysql_real_escape_string($_POST['recipeid']), "int"));
//
//        mysql_select_db($database_chewsrite, $chewsrite);
//        $Result1 = mysql_query($updateSQL, $chewsrite) or die(mysql_error());
//        
//        $object->substitutionResult .= " removed substitions.";
//    }
//    
//    if(isset($_POST['subIDs']) && $_POST['subIDs'] != "")
//    {
//        $updateSQL = sprintf("UPDATE recipesubstitutes SET active = %s WHERE subid IN (%s) AND recipeid = %s",
//            GetSQLValueString(mysql_real_escape_string(1), "int"),
//            GetSQLValueString(mysql_real_escape_string($_POST['subIDs']), "int"),
//            GetSQLValueString(mysql_real_escape_string($_POST['recipeid']), "int"));
//
//        mysql_select_db($database_chewsrite, $chewsrite);
//        $Result1 = mysql_query($updateSQL, $chewsrite) or die(mysql_error());
//        
//        $object->substitutionResult .= " added substitions.";
//    }
    
    $object->status = "grocery items updated";
}
else
{
    
    
    $insertSQL = sprintf("INSERT INTO groceryitems (ingredientids, substitutionids, recipeid, userid, datecreated) VALUES (%s, %s, %s, %s, %s)",
				GetSQLValueString(mysql_real_escape_string($_POST['ingredientids']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['subIDs']), "text"),
                GetSQLValueString(mysql_real_escape_string($_POST['recipeid']), "int"),
				GetSQLValueString(mysql_real_escape_string($_POST['userid']), "int"),
				GetSQLValueString(mysql_real_escape_string($date), "date"));
    
    mysql_select_db($database_chewsrite, $chewsrite);
    $Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());	
    
    
    //explode ids and create record for each
    
//    $ids = explode(",",$_POST['ingredientids']);
//    
//    foreach($ids as $id)
//    {
//        $insertSQL = sprintf("INSERT INTO groceryitems (ingredientid, recipeid, userid, datecreated) VALUES (%s, %s, %s, %s)",
//				GetSQLValueString(mysql_real_escape_string($id), "text"),
//				GetSQLValueString(mysql_real_escape_string($_POST['recipeid']), "int"),
//				GetSQLValueString(mysql_real_escape_string($_POST['userid']), "text"),
//				GetSQLValueString(mysql_real_escape_string($date), "date"));
//
//        mysql_select_db($database_chewsrite, $chewsrite);
//        $Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());	
//
//        $last_id = mysql_insert_id();	
//        $object->ingredientids[] = $last_id;
//    }
    
//	$insertSQL = sprintf("INSERT INTO groceryitems (ingredientids, recipeid, userid, datecreated) VALUES (%s, %s, %s, %s)",
//				GetSQLValueString(mysql_real_escape_string($_POST['ingredientids']), "text"),
//				GetSQLValueString(mysql_real_escape_string($_POST['recipeid']), "int"),
//				GetSQLValueString(mysql_real_escape_string($_POST['userid']), "text"),
//				GetSQLValueString(mysql_real_escape_string($date), "date"));
//
//	mysql_select_db($database_chewsrite, $chewsrite);
//	$Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());	
//
//	$last_id = mysql_insert_id();	
//	$object->ingredientid = $last_id;
	$object->status = "grocery items saved";
}


echo "{\"data\":";
echo "{\"groceryItemsData\":";
echo json_encode( $object );
echo "}";
echo "}";

?>
