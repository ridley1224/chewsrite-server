<?php

header('Content-type: application/json');

require_once('../../Connections/chewsrite.php');
include("functions.php");

mysql_select_db($database_chewsrite, $chewsrite);

$string;
$date = date("Y-m-d H:i:s");

$object = new stdClass();
$object->status = "feedback not saved";

if(isset($_POST['message']) && isset($_POST['userid']))
{
	$insertSQL = sprintf("INSERT INTO feedback(message, userid, createddate) VALUES (%s, %s, %s)",
				GetSQLValueString(mysql_real_escape_string($_POST['message']), "text"),
				GetSQLValueString(mysql_real_escape_string($_POST['userid']), "int"),
				GetSQLValueString(mysql_real_escape_string($date), "date"));

	mysql_select_db($database_chewsrite, $chewsrite);
	$Result1 = mysql_query($insertSQL, $chewsrite) or die(mysql_error());	

	$last_id = mysql_insert_id();	
	$object->folderid = (string)$last_id;
	$object->status = "feedback saved";
}

echo "{\"data\":";
echo "{\"feedbackData\":";
echo json_encode( $object );
echo "}";
echo "}";

?>
        