<?php require_once('../../Connections/chewsrite.php'); ?>
<?php

include("functions.php");

$object = new stdClass();

mysql_select_db($database_chewsrite, $chewsrite);

$query_rsContacts = sprintf("SELECT * FROM users WHERE email = %s",
			GetSQLValueString($_POST["email"], "text"));
			
$object->contacts = $query_rsContacts;

//echo $query_rsContacts ."<br>";

$rsContacts = mysql_query($query_rsContacts, $chewsrite) or die(mysql_error());
$row_rsContacts = mysql_fetch_assoc($rsContacts);
$totalRows_rsContacts = mysql_num_rows($rsContacts);

if($totalRows_rsContacts > 0)
{	
	$email = $_POST['email'];
    
    $to = $_POST["email"];
    $subject = "ChewsRite Account Registration Request";
    $html = "We’ve emailed a verification link to {$_POST["email"]}. Click here to finish setting up your account.";
    $text = "We’ve emailed a verification link to {$_POST["email"]}. Click here to finish setting up your account.";
    $from = "noreply@chewsrite.com";

    include("send-email.php");
    
    $object->status = "email sent";
}
else
{
	$object->status = "No user exists with that email";
}

//$list[] = $object;

echo "{\"data\":";
echo "{\"emailData\":";
echo json_encode( $object );
echo "}";
echo "}";

?>